package com.partisiablockchain.governance.mpctoken;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Callback;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.RpcType;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.contract.sys.SystemEventCreator;
import com.partisiablockchain.contract.sys.SystemEventManager;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateAccessorAvlLeafNode;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.DataStreamSerializable;
import com.secata.tools.immutable.FixedList;

/** A system contract that handles the MPC tokens used for governance and staking. */
@AutoSysContract(MpcTokenContractState.class)
public final class MpcTokenContract {

  static final long TWO_PHASE_CALLBACK_GAS_COST =
      2500 /* callback CPU cost */ + 2 * 2500 /* COMMIT/ABORT event cost */;

  static final BlockchainAddress SYSTEM_UPDATE_CONTRACT_ADDRESS =
      BlockchainAddress.fromString("04c5f00d7c6d70c3d0919fd7f81c7b9bfe16063620");

  /**
   * Initialize the mpc token contract.
   *
   * @param locked true if transfers of mpc tokens should be blocked
   * @return initial state
   */
  @Init
  public MpcTokenContractState create(boolean locked) {
    return MpcTokenContractState.initial(locked);
  }

  /**
   * Migrate an existing contract to this version.
   *
   * @param oldState accessor for the old state
   * @return the migrated state
   */
  @Upgrade
  public MpcTokenContractState upgrade(StateAccessor oldState) {
    MpcTokenContractState state =
        MpcTokenContractState.initial(oldState.get("locked").booleanValue());

    StateAccessor transfers = oldState.get("transfers");
    for (StateAccessorAvlLeafNode transfer : transfers.getTreeLeaves()) {
      Hash transactionId = transfer.getKey().hashValue();
      StateAccessor transferAccessor = transfer.getValue();
      TransferInformation transferInfo =
          TransferInformation.createFromStateAccessor(transferAccessor);
      state = state.addTransfer(transactionId, transferInfo);
    }

    StateAccessor stakes = oldState.get("stakeDelegations");
    for (StateAccessorAvlLeafNode stake : stakes.getTreeLeaves()) {
      Hash transactionId = stake.getKey().hashValue();
      StakeDelegation stakeDelegation = StakeDelegation.createFromStateAccessor(stake.getValue());
      state = state.addStakeDelegation(transactionId, stakeDelegation);
    }

    StateAccessor iceStakes = oldState.get("icedStakings");
    for (StateAccessorAvlLeafNode iceStake : iceStakes.getTreeLeaves()) {
      Hash transactionId = iceStake.getKey().hashValue();
      IceStaking iceStaking = IceStaking.createFromStateAccessor(iceStake.getValue());
      state = state.addIcedTokens(transactionId, iceStaking);
    }

    return state;
  }

  /**
   * Stakes tokens for sender.
   *
   * @pre
   *     <ul>
   *       <li>The call must be made by an account.
   *       <li>The amount <var>x &gt; 0</var>.
   *       <li>The account exists.
   *       <li>The account has <var>y</var> staked tokens.
   *       <li>The account has <var>z</var> unstaked tokens, such that <var>z &ge; x</var>.
   *     </ul>
   *
   * @post
   *     <ul>
   *       <li>The account has <var>x + y</var> staked tokens.
   *       <li>The account token balance remains unchanged, but the amount of unstaked tokens is
   *           <var>z - x</var>
   *     </ul>
   *
   * @param context execution context
   * @param state current state
   * @param amount number of tokens to stake
   * @return unchanged state
   */
  @Action(Invocations.STAKE_TOKENS)
  public MpcTokenContractState stakeTokens(
      SysContractContext context, MpcTokenContractState state, long amount) {
    ensure(amount > 0, "Amount must be positive");
    BlockchainAddress sender = context.getFrom();
    LocalPluginStateUpdate update =
        LocalPluginStateUpdate.create(sender, AccountPluginRpc.stakeTokens(amount));
    context.getInvocationCreator().updateLocalAccountPluginState(update);
    return state;
  }

  /**
   * Stakes tokens on behalf of another account.
   *
   * <p>See {@link MpcTokenContract#stakeTokens} for pre- and post-conditions.
   *
   * @param context execution context
   * @param state current state
   * @param target address of the account to stake on behalf of
   * @param amount number of tokens to stake
   * @return unchanged state
   */
  @Action(Invocations.STAKE_TOKENS_ON_BEHALF_OF)
  public MpcTokenContractState stakeTokensOnBehalfOf(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress target,
      long amount) {
    ensure(amount > 0, "Amount must be positive");
    BlockchainAddress custodian = context.getFrom();
    LocalPluginStateUpdate update =
        LocalPluginStateUpdate.create(
            target, AccountPluginRpc.stakeTokensOnBehalfOf(custodian, amount));
    context.getInvocationCreator().updateLocalAccountPluginState(update);
    return state;
  }

  /**
   * Unstakes tokens for sender.
   *
   * @pre
   *     <ul>
   *       <li>The call must be made by an account.
   *       <li>The amount <var>x &gt; 0</var>.
   *       <li>The account exists.
   *       <li>The account has <var>y</var> staked tokens, such that <var>y &ge; x</var>.
   *       <li>The account has <var>z</var> unstaked, unassociated tokens.
   *     </ul>
   *
   * @post
   *     <ul>
   *       <li>The account has <var>y - x</var> staked tokens.
   *       <li>The <var>x</var> tokens are added to the pending unstakes of the account.
   *       <li>The account balance remains unchanged.
   *     </ul>
   *
   * @param context execution context
   * @param state current state
   * @param amount number of tokens to unstake
   * @return unchanged state
   */
  @Action(Invocations.UNSTAKE_TOKENS)
  public MpcTokenContractState unstakeTokens(
      SysContractContext context, MpcTokenContractState state, long amount) {
    ensure(amount > 0, "Amount must be positive");
    BlockchainAddress sender = context.getFrom();
    LocalPluginStateUpdate update =
        LocalPluginStateUpdate.create(sender, AccountPluginRpc.unstakeTokens(amount));
    context.getInvocationCreator().updateLocalAccountPluginState(update);
    return state;
  }

  /**
   * Unstakes tokens on behalf of another account.
   *
   * <p>See {@link MpcTokenContract#unstakeTokens} for pre- and post-conditions.
   *
   * @param context execution context
   * @param state current state
   * @param target address of the account to unstake on behalf of
   * @param amount number of tokens to unstake
   * @return unchanged state
   */
  @Action(Invocations.UNSTAKE_TOKENS_ON_BEHALF_OF)
  public MpcTokenContractState unstakeTokensOnBehalfOf(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress target,
      long amount) {
    ensure(amount > 0, "Amount must be positive");
    BlockchainAddress custodian = context.getFrom();
    LocalPluginStateUpdate update =
        LocalPluginStateUpdate.create(
            target, AccountPluginRpc.unstakeTokensOnBehalfOf(custodian, amount));
    context.getInvocationCreator().updateLocalAccountPluginState(update);
    return state;
  }

  /**
   * Disassociates all associated tokens from a removed contract.
   *
   * @pre
   *     <ul>
   *       <li>The account exists.
   *       <li>The account has <var>x</var> tokens associated to contract <var>A</var>.
   *       <li>The contract <var>A</var> does not exist anymore.
   *       <li>The account has <var>y</var> staked tokens, such that <var>y &ge; x</var>.
   *     </ul>
   *
   * @post
   *     <ul>
   *       <li>The account still has <var>x</var> staked tokens.
   *       <li>The account has no tokens associated to contract <var>A</var>.
   *       <li>The account balance remains unchanged.
   *     </ul>
   *
   * @param context execution context
   * @param state current state
   * @param contract the removed contract to disassociate tokens from
   * @return unchanged state
   */
  @Action(MpcTokenContract.Invocations.DISASSOCIATE_TOKENS_FOR_REMOVED_CONTRACT)
  public MpcTokenContractState disassociateTokensForRemovedContract(
      SysContractContext context, MpcTokenContractState state, BlockchainAddress contract) {
    BlockchainAddress sender = context.getFrom();
    SystemEventManager eventManager = context.getRemoteCallsCreator();
    eventManager.registerCallbackWithCostFromRemaining(
        Callbacks.disassociateTokensForRemovedContract(contract, sender));
    eventManager.exists(contract);
    return state;
  }

  /**
   * Disassociates all associated tokens from a removed contract for another account.
   *
   * @pre
   *     <ul>
   *       <li>The account exists.
   *       <li>The account has <var>x</var> tokens associated to contract <var>A</var>.
   *       <li>The contract <var>A</var> does not exist anymore.
   *       <li>The account has <var>y</var> staked tokens, such that <var>y &ge; x</var>.
   *     </ul>
   *
   * @post
   *     <ul>
   *       <li>The account still has <var>x</var> staked tokens.
   *       <li>The account has no tokens associated to contract <var>A</var>.
   *       <li>The account balance remains unchanged.
   *     </ul>
   *
   * @param context execution context
   * @param state current state
   * @param contract the removed contract to disassociate tokens from
   * @param other the account for which to disassociate for
   * @return unchanged state
   */
  @Action(Invocations.DISASSOCIATE_TOKENS_FOR_REMOVED_CONTRACT_FOR_OTHER)
  public MpcTokenContractState disassociateTokensForRemovedContractForOther(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress contract,
      BlockchainAddress other) {
    SystemEventManager eventManager = context.getRemoteCallsCreator();
    eventManager.registerCallbackWithCostFromRemaining(
        Callbacks.disassociateTokensForRemovedContract(contract, other));
    eventManager.exists(contract);
    return state;
  }

  /**
   * Transfer tokens from sender to receiver.
   *
   * @pre
   *     <ul>
   *       <li>Sender and receiver are different accounts.
   *       <li>The amount <var>x &gt; 0</var>.
   *       <li>Sender has <var>y</var> MPC tokens.
   *       <li>A transfer costs <var>g</var> gas.
   *       <li>Sender has BYOC corresponding to <var>h</var> gas and sends <var>f</var> gas with the
   *           transaction, such that <var>h &ge; f &ge; g</var>.
   *       <li>Callbacks of transfer event cost <var>i</var> gas.
   *       <li>Receiver has <var>z</var> MPC tokens.
   *     </ul>
   *
   * @post
   *     <ul>
   *       <li>A transfer event is added to state.
   *       <li>Sender now has BYOC corresponding to <var>h - f</var> gas.
   *       <li>Transaction now has <var>f := f - g</var> gas left.
   *       <li>If <var>y &ge; x &amp; f &ge; i</var> (Commit):
   *           <ul>
   *             <li>Receiver balance is <var>z + x</var> MPC tokens.
   *             <li>Sender balance is <var>y> - x</var> MPC tokens.
   *             <li>Transfer event is removed from state.
   *           </ul>
   *       <li>Else if <var>y &lt; x &amp; f &ge; i</var> (Abort):
   *           <ul>
   *             <li>Receiver balance remains <var>z</var> MPC tokens.
   *             <li>Sender balance remains <var>y</var> MPC tokens.
   *             <li>Transfer event is removed from state.
   *           </ul>
   *       <li>Else (<var>f &lt; i</var>):
   *           <ul>
   *             <li>Receiver balance remains <var>z</var> MPC tokens.
   *             <li>Sender balance is <var>y - x</var> MPC tokens.
   *             <li>Transfer event remains in state.
   *           </ul>
   *     </ul>
   *
   * @param context execution context
   * @param state current state
   * @param recipient receiver of tokens
   * @param amount number of tokens to transfer
   * @return updated state
   */
  @Action(MpcTokenContract.Invocations.TRANSFER)
  public MpcTokenContractState transfer(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress recipient,
      long amount) {
    ensure(!state.isLocked(), "Transfers are locked");
    return executeTransfer(
        context, state, recipient, Unsigned256.create(amount), null, context.getFrom());
  }

  /**
   * Transfer tokens on behalf of another account.
   *
   * <p>See {@link MpcTokenContract#transfer} for pre- and post-conditions.
   *
   * @param context execution context
   * @param state current state
   * @param target address of the account to transfer on behalf of
   * @param recipient receiver of tokens
   * @param amount number of tokens to transfer
   * @return updated state
   */
  @Action(Invocations.TRANSFER_ON_BEHALF_OF)
  public MpcTokenContractState transferOnBehalfOf(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress target,
      BlockchainAddress recipient,
      long amount) {
    ensure(!state.isLocked(), "Transfers are locked");
    return executeTransfer(context, state, recipient, Unsigned256.create(amount), null, target);
  }

  /**
   * Set staking goal for sender. The staking goal is how many total staked tokens the account
   * wants. A non-zero staking goal will automate the account for delegated stakes by automatically
   * either accepting new delegated stakes, current pending delegated stakes or reducing the amount
   * of accepted delegated stakes. If the total staked tokens are higher than the staking goal, the
   * account will prioritize stakes to reach the staking goal in the following order: Own staked
   * tokens, non-expiring delegated stakes and thereafter delegated stakes with a later expiration.
   * See {@link MpcTokenContract#delegateStakesWithExpiration} and {@link
   * MpcTokenContract#setExpirationTimestampForDelegatedStakes} for explanations about expiring
   * delegated stakes.
   *
   * @param context execution context
   * @param state current state
   * @param stakingGoal goal amount of staked tokens
   * @return unchanged state
   */
  @Action(Invocations.SET_STAKING_GOAL)
  public MpcTokenContractState setStakingGoal(
      SysContractContext context, MpcTokenContractState state, long stakingGoal) {
    ensure(stakingGoal >= 0, "Staking goal cannot be negative");
    BlockchainAddress sender = context.getFrom();
    LocalPluginStateUpdate update =
        LocalPluginStateUpdate.create(sender, AccountPluginRpc.setStakingGoal(stakingGoal));
    context.getInvocationCreator().updateLocalAccountPluginState(update);
    return state;
  }

  /**
   * Sets expiration for a delegation to another account. Delegated stakes with an expiration cannot
   * be used by the other account to run new jobs if the stakes has expired.
   *
   * @param context execution context
   * @param state current state
   * @param target the target of the delegated stakes
   * @param expirationTimestamp the new expiration timestamp for the delegation
   * @return unchanged state
   */
  @Action(Invocations.SET_EXPIRATION_FOR_DELEGATED_STAKES)
  public MpcTokenContractState setExpirationTimestampForDelegatedStakes(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress target,
      @RpcType(nullable = true) Long expirationTimestamp) {
    ensure(
        expirationTimestamp == null || expirationTimestamp >= 0,
        "Expiration timestamp cannot be negative");
    BlockchainAddress sender = context.getFrom();
    LocalPluginStateUpdate update =
        LocalPluginStateUpdate.create(
            target, AccountPluginRpc.setExpirationTimestamp(sender, expirationTimestamp));
    context.getInvocationCreator().updateLocalAccountPluginState(update);
    return state;
  }

  /**
   * Sets expiration timestamp for a delegation to another account on behalf of another account.
   *
   * @param context execution context
   * @param state current state
   * @param target address of the account to receive the delegation
   * @param delegator address of the delegator of the delegation
   * @param expirationTimestamp number of tokens to delegate
   * @return updated state
   */
  @Action(Invocations.SET_EXPIRATION_TIMESTAMP_FOR_DELEGATED_STAKES_ON_BEHALF_OF)
  public MpcTokenContractState setExpirationTimestampForDelegatedStakesOnBehalfOf(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress delegator,
      BlockchainAddress target,
      @RpcType(nullable = true) Long expirationTimestamp) {
    ensure(
        expirationTimestamp == null || expirationTimestamp >= 0,
        "Expiration timestamp cannot be negative");
    BlockchainAddress custodian = context.getFrom();
    LocalPluginStateUpdate update =
        LocalPluginStateUpdate.create(
            target,
            AccountPluginRpc.setExpirationTimestampOnBehalfOf(
                custodian, delegator, expirationTimestamp));
    context.getInvocationCreator().updateLocalAccountPluginState(update);
    return state;
  }

  /**
   * Set staking goal on behalf of another account.
   *
   * <p>See {@link MpcTokenContract#setStakingGoal} for pre- and post-conditions.
   *
   * @param context execution context
   * @param state current state
   * @param target address of the account to stake on behalf of
   * @param stakingGoal goal amount of staked tokens
   * @return unchanged state
   */
  @Action(Invocations.SET_STAKING_GOAL_ON_BEHALF_OF)
  public MpcTokenContractState setStakingGoalOnBehalfOf(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress target,
      long stakingGoal) {
    ensure(stakingGoal >= 0, "Staking goal cannot be negative");
    BlockchainAddress custodian = context.getFrom();
    LocalPluginStateUpdate update =
        LocalPluginStateUpdate.create(
            target, AccountPluginRpc.setStakingGoalOnBehalfOf(custodian, stakingGoal));
    context.getInvocationCreator().updateLocalAccountPluginState(update);
    return state;
  }

  /**
   * Abort a previously failed transfer.
   *
   * @pre
   *     <ul>
   *       <li>An existing transfer event is provided.
   *       <li>Transfer event tried to transfer <var>x</var> MPC tokens from sender towards
   *           receiver, but failed.
   *       <li>Sender of transfer event has <var>y</var> MPC tokens.
   *       <li>Receiver of transfer event has <var>z</var> MPC tokens.
   *     </ul>
   *
   * @post
   *     <ul>
   *       <li>Sender of transfer event has <var>y + x</var> MPC tokens.
   *       <li>Receiver of transfer event still has <var>z</var> MPC tokens.
   *       <li>Transfer event is removed from state.
   *     </ul>
   *
   * @param context execution context
   * @param state current state
   * @param transactionId the id of the original transaction that is to be aborted
   * @return updated state
   */
  @Action(MpcTokenContract.Invocations.ABORT)
  public MpcTokenContractState abort(
      SysContractContext context, MpcTokenContractState state, Hash transactionId) {

    AvlTree<Hash, TransferInformation> transfers = state.getTransfers();
    if (!transfers.containsKey(transactionId)) {
      throw new IllegalArgumentException("Cannot abort a new transfer");
    }
    BlockchainAddress sender = context.getFrom();
    TransferInformation transfer = transfers.getValue(transactionId);

    if (!sender.equals(transfer.initiator)) {
      throw new IllegalArgumentException(
          "Cannot abort a pending transfer that you are not the original sender of");
    }

    invokeAbort(transactionId, transfer.sender, transfer.recipient, context.getInvocationCreator());
    return state.removeTransfer(transactionId);
  }

  /**
   * Checks if any pending unstakes are ready to be released.
   *
   * @pre
   *     <ul>
   *       <li>Can only be called by an account.
   *       <li>Called at block production time <var>t</var>.
   *       <li>Account <var>A</var> has <var>x</var> pending unstaked tokens and pending retracted
   *           delegated stakes to be released at block production time <var>v</var>.
   *       <li>Account <var>A</var> has <var>y</var> staked tokens.
   *     </ul>
   *
   * @post
   *     <ul>
   *       <li>If <var>v &le; t</var> (Ready for unstake):
   *           <ul>
   *             <li>Account <var>A</var> has <var>y + x</var> staked tokens.
   *           </ul>
   *       <li>Else <var>v &gt; t</var> (Not ready for unstake):
   *           <ul>
   *             <li>Account <var>A</var> has <var>y</var> staked tokens.
   *           </ul>
   *     </ul>
   *
   * @param context execution context
   * @param state current state
   * @return unchanged state
   */
  @Action(MpcTokenContract.Invocations.CHECK_PENDING_UNSTAKES)
  public MpcTokenContractState checkPendingUnstakes(
      SysContractContext context, MpcTokenContractState state) {
    BlockchainAddress sender = context.getFrom();
    LocalPluginStateUpdate update =
        LocalPluginStateUpdate.create(sender, AccountPluginRpc.checkPendingUnstakes());
    context.getInvocationCreator().updateLocalAccountPluginState(update);
    return state;
  }

  /**
   * Checks if any pending unstakes are ready to be released on behalf of another account.
   *
   * <p>See {@link MpcTokenContract#checkPendingUnstakes} for pre- and post-conditions.
   *
   * @param context execution context
   * @param state current state
   * @param target address of the account to check on behalf of
   * @return unchanged state
   */
  @Action(Invocations.CHECK_PENDING_UNSTAKES_ON_BEHALF_OF)
  public MpcTokenContractState checkPendingUnstakesOnBehalfOf(
      SysContractContext context, MpcTokenContractState state, BlockchainAddress target) {
    BlockchainAddress custodian = context.getFrom();
    LocalPluginStateUpdate update =
        LocalPluginStateUpdate.create(
            target, AccountPluginRpc.checkPendingUnstakesOnBehalfOf(custodian));
    context.getInvocationCreator().updateLocalAccountPluginState(update);
    return state;
  }

  /**
   * Checks if any vested tokens are ready to be released.
   *
   * @pre
   *     <ul>
   *       <li>Can only be called by an account.
   *       <li>Called at block production time <var>t</var>.
   *       <li>Account <var>A</var> has <var>x</var> vested tokens to be released at block
   *           production time <var>v</var>.
   *       <li>Account <var>A</var> has <var>y</var> MPC tokens.
   *     </ul>
   *
   * @post
   *     <ul>
   *       <li>If <var>v &le; t</var> (Ready to release tokens):
   *           <ul>
   *             <li><var>A</var> has <var>y + x</var> MPC Tokens.
   *           </ul>
   *       <li>Else <var>v &gt; t</var> (Not ready to release tokens):
   *           <ul>
   *             <li><var>A</var> has <var>y</var> MPC Tokens.
   *           </ul>
   *     </ul>
   *
   * @param context execution context
   * @param state current state
   * @return unchanged state
   */
  @Action(MpcTokenContract.Invocations.CHECK_VESTED_TOKENS)
  public MpcTokenContractState checkVestedTokens(
      SysContractContext context, MpcTokenContractState state) {
    BlockchainAddress sender = context.getFrom();
    LocalPluginStateUpdate update =
        LocalPluginStateUpdate.create(sender, AccountPluginRpc.checkVestedTokens());
    context.getInvocationCreator().updateLocalAccountPluginState(update);
    return state;
  }

  /**
   * Checks if any vested tokens are ready to be released.
   *
   * @pre
   *     <ul>
   *       <li>Can only be called by an account.
   *       <li>Called at block production time <var>t</var>.
   *       <li>Account <var>Other</var> has <var>x</var> vested tokens to be released at block
   *           production time <var>v</var>.
   *       <li>Account <var>Other</var> has <var>y</var> MPC tokens.
   *     </ul>
   *
   * @post
   *     <ul>
   *       <li>If <var>v &le; t</var> (Ready to release tokens):
   *           <ul>
   *             <li><var>Other</var> has <var>y + x</var> MPC Tokens.
   *           </ul>
   *       <li>Else <var>v &gt; t</var> (Not ready to release tokens):
   *           <ul>
   *             <li><var>Other</var> has <var>y</var> MPC Tokens.
   *           </ul>
   *     </ul>
   *
   * @param context execution context
   * @param state current state
   * @param other the account to check vesting for
   * @return unchanged state
   */
  @Action(MpcTokenContract.Invocations.CHECK_VESTED_TOKENS_FOR_OTHER)
  public MpcTokenContractState checkVestedTokensForOther(
      SysContractContext context, MpcTokenContractState state, BlockchainAddress other) {
    LocalPluginStateUpdate update =
        LocalPluginStateUpdate.create(other, AccountPluginRpc.checkVestedTokens());
    context.getInvocationCreator().updateLocalAccountPluginState(update);
    return state;
  }

  /**
   * Checks if any vested tokens are ready to be released on behalf of another account.
   *
   * <p>See {@link MpcTokenContract#checkVestedTokens} for pre- and post-conditions.
   *
   * @param context execution context
   * @param state current state
   * @param target address of the account to check on behalf of
   * @return unchanged state
   */
  @Action(Invocations.CHECK_VESTED_TOKENS_ON_BEHALF_OF)
  public MpcTokenContractState checkVestedTokensOnBehalfOf(
      SysContractContext context, MpcTokenContractState state, BlockchainAddress target) {
    BlockchainAddress custodian = context.getFrom();
    LocalPluginStateUpdate update =
        LocalPluginStateUpdate.create(
            target, AccountPluginRpc.checkVestedTokensOnBehalfOf(custodian));
    context.getInvocationCreator().updateLocalAccountPluginState(update);
    return state;
  }

  /**
   * Transfer tokens from sender to receiver.
   *
   * @param context execution context
   * @param state current state
   * @param recipient receiver of tokens
   * @param amount number of tokens to transfer
   * @param memo a memo identifying the transfer
   * @return updated state
   * @see #transfer(SysContractContext, MpcTokenContractState, BlockchainAddress, long)
   */
  @Action(MpcTokenContract.Invocations.TRANSFER_SMALL_MEMO)
  public MpcTokenContractState transferWithSmallMemo(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress recipient,
      long amount,
      @SuppressWarnings("unused") long memo) {
    ensure(!state.isLocked(), "Transfers are locked");
    return executeTransfer(
        context, state, recipient, Unsigned256.create(amount), null, context.getFrom());
  }

  /**
   * Transfer tokens and a {@code long} memo to a receiver on behalf of another account.
   *
   * <p>See {@link MpcTokenContract#transferWithSmallMemo} for pre- and post-conditions.
   *
   * @param context execution context
   * @param state current state
   * @param target address of the account to transfer on behalf of
   * @param recipient receiver of tokens
   * @param amount number of tokens to transfer
   * @param memo a memo identifying the transfer
   * @return updated state
   * @see #transfer(SysContractContext, MpcTokenContractState, BlockchainAddress, long)
   */
  @Action(Invocations.TRANSFER_SMALL_MEMO_ON_BEHALF_OF)
  public MpcTokenContractState transferWithSmallMemoOnBehalfOf(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress target,
      BlockchainAddress recipient,
      long amount,
      @SuppressWarnings("unused") long memo) {
    ensure(!state.isLocked(), "Transfers are locked");
    return executeTransfer(context, state, recipient, Unsigned256.create(amount), null, target);
  }

  /**
   * Transfer tokens from sender to receiver.
   *
   * @param context execution context
   * @param state current state
   * @param recipient receiver of tokens
   * @param amount number of tokens to transfer
   * @param memo a memo identifying the transfer
   * @return updated state
   * @see #transfer(SysContractContext, MpcTokenContractState, BlockchainAddress, long)
   */
  @Action(MpcTokenContract.Invocations.TRANSFER_LARGE_MEMO)
  public MpcTokenContractState transferWithLargeMemo(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress recipient,
      long amount,
      @SuppressWarnings("unused") String memo) {
    ensure(!state.isLocked(), "Transfers are locked");
    return executeTransfer(
        context, state, recipient, Unsigned256.create(amount), null, context.getFrom());
  }

  /**
   * Transfer tokens and a {@code String} memo to a receiver on behalf of another account.
   *
   * <p>See {@link MpcTokenContract#transferWithLargeMemo} for pre- and post-conditions.
   *
   * @param context execution context
   * @param state current state
   * @param target address of the account to transfer on behalf of
   * @param recipient receiver of tokens
   * @param amount number of tokens to transfer
   * @param memo a memo identifying the transfer
   * @return updated state
   * @see #transfer(SysContractContext, MpcTokenContractState, BlockchainAddress, long)
   */
  @Action(Invocations.TRANSFER_LARGE_MEMO_ON_BEHALF_OF)
  public MpcTokenContractState transferWithLargeMemoOnBehalfOf(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress target,
      BlockchainAddress recipient,
      long amount,
      @SuppressWarnings("unused") String memo) {
    ensure(!state.isLocked(), "Transfers are locked");
    return executeTransfer(context, state, recipient, Unsigned256.create(amount), null, target);
  }

  /**
   * Sender delegate stakes to a receiver that can use the delegated stakes to run jobs. The
   * delegates stakes can be taken back, if they are not used to run jobs by the receiver.
   *
   * @pre
   *     <ul>
   *       <li>Sender must be an account.
   *       <li>The amount <var>x &gt; 0</var>.
   *       <li>Receiver has <var>y</var> delegated stakes from sender.
   *       <li>Sender has <var>y</var> delegated stakes towards receiver.
   *       <li>Sender has <var>z</var> unstaked tokens, such that <var>z &ge; x</var>.
   *       <li>A stake delegation event costs <var>g</var> gas.
   *       <li>Sender has BYOC corresponding to <var>h</var> gas and sends <var>f</var> gas with the
   *           transaction, such that <var>h &ge; f &ge; g</var>.
   *       <li>Callbacks of stake delegation event cost <var>i</var> gas.
   *     </ul>
   *
   * @post
   *     <ul>
   *       <li>A stake delegation event is added to state.
   *       <li>Sender now has BYOC corresponding to <var>h - f</var> gas.
   *       <li>Transaction now has <var>f := f - g</var> gas left.
   *       <li>If <var>z &ge; x &amp; f &ge; i</var> (Commit):
   *           <ul>
   *             <li>Receiver has <var>y + x</var> delegated stakes from sender.
   *             <li>Sender has <var>y + x</var> delegated stakes towards receiver.
   *             <li>Sender balance is <var>z - x</var> MPC tokens.
   *             <li>Sender has <var>f - i</var> gas.
   *             <li>Stake delegation event is removed from state.
   *           </ul>
   *       <li>Else if <var>z &lt; x &amp; f &ge; i</var> (Abort):
   *           <ul>
   *             <li>Receiver continues to have <var>y</var> delegated stakes from sender.
   *             <li>Sender continues to have <var>y</var> delegated stakes towards receiver.
   *             <li>Sender balance remains <var>z</var> MPC tokens.
   *             <li>Sender has <var>f - i</var> gas.
   *             <li>Stake delegation event is removed from state.
   *           </ul>
   *       <li>Else (<var>f &lt; i</var>):
   *           <ul>
   *             <li>Receiver continues to have <var>y</var> delegated stakes from sender.
   *             <li>Sender continues to have <var>y</var> delegated stakes towards receiver.
   *             <li>Sender balance is <var>z - x</var> MPC tokens.
   *             <li>Stake delegation event remains in state.
   *           </ul>
   *     </ul>
   *
   * @param context execution context
   * @param state current state
   * @param recipient receiver of delegation
   * @param amount number of tokens to delegate
   * @return updated state
   */
  @Action(MpcTokenContract.Invocations.DELEGATE_STAKES)
  public MpcTokenContractState delegateStakes(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress recipient,
      long amount) {
    return executeDelegateStakes(context, state, recipient, amount, context.getFrom(), null);
  }

  /**
   * Sender delegate stakes with expiration to a receiver that can use the delegated stakes to run
   * new jobs if the stakes has not expired. The delegates stakes can be taken back, if they are not
   * used to run jobs by the receiver.
   *
   * @param context execution context
   * @param state current state
   * @param recipient receiver of delegation
   * @param amount number of tokens to delegate
   * @param expirationTimestamp the timestamp the delegation expires
   * @return updated state
   */
  @Action(Invocations.DELEGATE_STAKES_WITH_EXPIRATION)
  public MpcTokenContractState delegateStakesWithExpiration(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress recipient,
      long amount,
      long expirationTimestamp) {
    return executeDelegateStakes(
        context, state, recipient, amount, context.getFrom(), expirationTimestamp);
  }

  /**
   * Delegate stakes to a receiver on behalf of another account.
   *
   * <p>See {@link MpcTokenContract#delegateStakes} for pre- and post-conditions.
   *
   * @param context execution context
   * @param state current state
   * @param target address of the account to delegate on behalf of
   * @param recipient receiver of delegation
   * @param amount number of tokens to delegate
   * @return updated state
   */
  @Action(Invocations.DELEGATE_STAKES_ON_BEHALF_OF)
  public MpcTokenContractState delegateStakesOnBehalfOf(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress target,
      BlockchainAddress recipient,
      long amount) {
    return executeDelegateStakes(context, state, recipient, amount, target, null);
  }

  /**
   * Delegate stakes with an expiration to a receiver on behalf of another account.
   *
   * <p>See {@link MpcTokenContract#delegateStakesWithExpiration} for full description.
   *
   * @param context execution context
   * @param state current state
   * @param target address of the account to delegate on behalf of
   * @param recipient receiver of delegation
   * @param amount number of tokens to delegate
   * @param expirationTimestamp the timestamp the delegation expires
   * @return updated state
   */
  @Action(Invocations.DELEGATE_STAKES_WITH_EXPIRATION_ON_BEHALF_OF)
  public MpcTokenContractState delegateStakesWithExpirationOnBehalfOf(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress target,
      BlockchainAddress recipient,
      long amount,
      long expirationTimestamp) {
    return executeDelegateStakes(context, state, recipient, amount, target, expirationTimestamp);
  }

  /**
   * Sender retracts delegated stakes from receiver.
   *
   * @pre
   *     <ul>
   *       <li>Sender must be an account.
   *       <li>The amount <var>x &gt; 0</var>.
   *       <li>Receiver has <var>y</var> delegated stakes from sender.
   *       <li>Sender has <var>y</var> delegated stakes towards receiver.
   *       <li>A stake delegation event costs <var>g</var> gas.
   *       <li>Sender has BYOC corresponding to <var>h</var> gas and sends <var>f</var> gas with the
   *           transaction, such that <var>h &ge; f &ge; g</var>.
   *       <li>Callbacks of stake delegation event cost <var>i</var> gas.
   *     </ul>
   *
   * @post
   *     <ul>
   *       <li>A stake delegation event is added to state.
   *       <li>Sender now has BYOC corresponding to <var>h - f</var> gas.
   *       <li>Transaction now has <var>f := f - g</var> gas left.
   *       <li>If <var>y &ge; x &amp; f &ge; i</var> (Commit):
   *           <ul>
   *             <li>Receiver has <var>y - x</var> delegated stakes from sender.
   *             <li>Sender has <var>y - x</var> delegated stakes towards receiver.
   *             <li>The <var>x</var> retracted delegated stakes are added to the pending retracted
   *                 delegated stakes of the sender.
   *             <li>Stake delegation event is removed from state.
   *           </ul>
   *       <li>Else if <var>y &lt; x &amp; f &ge; i</var> (Abort):
   *           <ul>
   *             <li>Receiver continues to have <var>y</var> delegated stakes from sender.
   *             <li>Sender continues to have <var>y</var> delegated stakes towards receiver.
   *             <li>Stake delegation event is removed from state.
   *           </ul>
   *       <li>Else (<var>f &lt; i</var>):
   *           <ul>
   *             <li>Receiver has <var>y - x</var> delegated stakes from sender.
   *             <li>Sender has <var>y - x</var> delegated stakes towards receiver.
   *             <li>Stake delegation event remains in state.
   *           </ul>
   *     </ul>
   *
   * @param context execution context
   * @param state current state
   * @param recipient receiver of the delegation
   * @param amount number of tokens to retract
   * @return updated state
   */
  @Action(MpcTokenContract.Invocations.RETRACT_DELEGATED_STAKES)
  public MpcTokenContractState retractDelegatedStakes(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress recipient,
      long amount) {
    return executeRetractDelegatedStakes(context, state, recipient, amount, context.getFrom());
  }

  /**
   * Retract delegated stakes from receiver on behalf of another account.
   *
   * <p>See {@link MpcTokenContract#retractDelegatedStakes} for pre- and post-conditions.
   *
   * @param context execution context
   * @param state current state
   * @param target address of the account to retract on behalf of
   * @param recipient receiver of the delegation
   * @param amount number of tokens to retract
   * @return updated state
   */
  @Action(Invocations.RETRACT_DELEGATED_STAKES_ON_BEHALF_OF)
  public MpcTokenContractState retractDelegatedStakesOnBehalfOf(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress target,
      BlockchainAddress recipient,
      long amount) {
    return executeRetractDelegatedStakes(context, state, recipient, amount, target);
  }

  /**
   * Aborts a pending stake delegation event.
   *
   * @pre
   *     <ul>
   *       <li>Delegate stakes event.
   *           <ul>
   *             <li>Can only be called by an account.
   *             <li>An existing stake delegation event is provided.
   *             <li>Account is sender of the stake delegation event.
   *             <li>Stake delegation event tried to delegate <var>x</var> from sender towards
   *                 receiver, but failed.
   *             <li>Sender has <var>y</var> delegated stakes towards receiver.
   *             <li>Receiver has <var>y</var> delegated stakes from sender.
   *             <li>Sender has <var>z</var> MPC tokens.
   *           </ul>
   *       <li>Retract delegated stakes event.
   *           <ul>
   *             <li>Can only be called by an account.
   *             <li>An existing stake delegation event is provided.
   *             <li>Account is sender of the stake delegation event.
   *             <li>Stake delegation event tried to retract <var>x</var> delegated stakes from
   *                 receiver, but failed.
   *             <li>Sender has <var>y</var> delegated stakes towards receiver.
   *             <li>Receiver has <var>y</var> delegated stakes from sender.
   *           </ul>
   *     </ul>
   *
   * @post
   *     <ul>
   *       <li>Delegate stakes event.
   *           <ul>
   *             <li>Receiver still has <var>y</var> delegated stakes from sender.
   *             <li>Sender still has <var>y</var> delegated stakes towards receiver.
   *             <li>Sender has <var>z + x</var> MPC tokens.
   *             <li>Stake delegation event is removed from state.
   *           </ul>
   *       <li>Retract delegated stakes event.
   *           <ul>
   *             <li>Sender has <var>y + x</var> delegated stakes towards receiver.
   *             <li>Receiver has <var>y + x</var> delegated stakes from sender.
   *             <li>Stake delegation event is removed from state.
   *           </ul>
   *     </ul>
   *
   * @param context execution context
   * @param state current state
   * @param transactionId id of the original delegation transaction to abort
   * @return updated state
   */
  @Action(MpcTokenContract.Invocations.ABORT_STAKE_DELEGATION_EVENT)
  public MpcTokenContractState abortStakeDelegationEvent(
      SysContractContext context, MpcTokenContractState state, Hash transactionId) {
    AvlTree<Hash, StakeDelegation> stakeDelegations = state.getStakeDelegations();
    if (!stakeDelegations.containsKey(transactionId)) {
      throw new IllegalArgumentException("Cannot abort action for id that does not exist");
    }
    BlockchainAddress sender = context.getFrom();
    StakeDelegation stakeDelegation = stakeDelegations.getValue(transactionId);

    if (!sender.equals(stakeDelegation.initiator)) {
      throw new IllegalArgumentException(
          "Cannot abort a stake delegation event that you are not the original sender of");
    }

    invokeAbortDelegateStakes(
        transactionId,
        stakeDelegation.sender,
        stakeDelegation.recipient,
        context.getInvocationCreator());

    return state.removeStakeDelegation(transactionId);
  }

  /**
   * Accept pending delegated stakes from another account.
   *
   * @pre
   *     <ul>
   *       <li>The call must be made by an account.
   *       <li>The amount <var>x &gt; 0</var>.
   *       <li>The account exists.
   *       <li>The account has <var>y</var> accepted delegated stakes from sender.
   *       <li>The account has <var>z</var> pending delegated stakes from sender, such that <var>z
   *           &ge; x</var>.
   *     </ul>
   *
   * @post
   *     <ul>
   *       <li>The account has <var>y + x</var> accepted delegated stakes.
   *       <li>The account has <var>z - x</var> pending delegated stakes.
   *       <li>The account balance remains unchanged.
   *     </ul>
   *
   * @param context execution context
   * @param state current state
   * @param sender account delegating tokens to the sender of this transaction
   * @param amount number of tokens to accept
   * @return unchanged state
   */
  @Action(MpcTokenContract.Invocations.ACCEPT_DELEGATED_STAKES)
  public MpcTokenContractState acceptDelegatedStakes(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress sender,
      long amount) {
    ensure(amount > 0, "Amount must be positive");
    LocalPluginStateUpdate update =
        LocalPluginStateUpdate.create(
            context.getFrom(), AccountPluginRpc.acceptDelegatedStakes(sender, amount));
    context.getInvocationCreator().updateLocalAccountPluginState(update);
    return state;
  }

  /**
   * Accept pending delegated stakes on behalf of another account.
   *
   * <p>See {@link MpcTokenContract#acceptDelegatedStakes} for pre- and post-conditions.
   *
   * @param context execution context
   * @param state current state
   * @param target address of the account to accept on behalf of
   * @param sender account delegating tokens to the sender of this transaction
   * @param amount number of tokens to accept
   * @return unchanged state
   */
  @Action(Invocations.ACCEPT_DELEGATED_STAKES_ON_BEHALF_OF)
  public MpcTokenContractState acceptDelegatedStakesOnBehalfOf(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress target,
      BlockchainAddress sender,
      long amount) {
    BlockchainAddress custodian = context.getFrom();
    ensure(amount > 0, "Amount must be positive");
    LocalPluginStateUpdate update =
        LocalPluginStateUpdate.create(
            target, AccountPluginRpc.acceptDelegatedStakesOnBehalfOf(custodian, sender, amount));
    context.getInvocationCreator().updateLocalAccountPluginState(update);
    return state;
  }

  /**
   * Reduce accepted delegated stakes from an account.
   *
   * @pre
   *     <ul>
   *       <li>The call must be made by an account.
   *       <li>The amount <var>x &gt; 0</var>.
   *       <li>The account exists.
   *       <li>The account has <var>y</var> accepted delegated stakes from {@code delegator}, such
   *           that <var>y &ge; x</var>.
   *       <li>The account has <var>z</var> pending delegated stakes from {@code delegator}.
   *     </ul>
   *
   * @post
   *     <ul>
   *       <li>The account has <var>y - x</var> accepted delegated stakes.
   *       <li>The account has <var>z + x</var> pending delegated stakes.
   *       <li>The account balance remains unchanged.
   *     </ul>
   *
   * @param context execution context
   * @param state current state
   * @param delegator the account to reduce the number of accepted delegated stakes from
   * @param amount number of tokens to reduce
   * @return unchanged state
   */
  @Action(MpcTokenContract.Invocations.REDUCE_DELEGATED_STAKES)
  public MpcTokenContractState reduceDelegatedStakes(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress delegator,
      long amount) {
    ensure(amount > 0, "Amount must be positive");
    LocalPluginStateUpdate update =
        LocalPluginStateUpdate.create(
            context.getFrom(), AccountPluginRpc.reduceDelegatedStakes(delegator, amount));
    context.getInvocationCreator().updateLocalAccountPluginState(update);
    return state;
  }

  /**
   * Reduce accepted delegated stakes on behalf of another account.
   *
   * <p>See {@link MpcTokenContract#reduceDelegatedStakes} for pre- and post-conditions.
   *
   * @param context execution context
   * @param state current state
   * @param target address of the account to reduce delegated stakes on behalf of
   * @param delegator the account to reduce the number of accepted delegated stakes from
   * @param amount number of tokens to reduce
   * @return unchanged state
   */
  @Action(Invocations.REDUCE_DELEGATED_STAKES_ON_BEHALF_OF)
  public MpcTokenContractState reduceDelegatedStakesOnBehalfOf(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress target,
      BlockchainAddress delegator,
      long amount) {
    ensure(amount > 0, "Amount must be positive");
    BlockchainAddress custodian = context.getFrom();

    LocalPluginStateUpdate update =
        LocalPluginStateUpdate.create(
            target, AccountPluginRpc.reduceDelegatedStakesOnBehalfOf(custodian, delegator, amount));

    context.getInvocationCreator().updateLocalAccountPluginState(update);
    return state;
  }

  /**
   * Transfer BYOC from sender to a receiver. The action is deprecated, use {@link
   * #transferByoc(SysContractContext, MpcTokenContractState, BlockchainAddress, Unsigned256,
   * String)} instead.
   *
   * @param context execution context
   * @param state current state
   * @param recipient receiver of tokens
   * @param amount number of tokens to transfer
   * @param symbol the BYOC symbol to transfer
   * @return updated state
   */
  @Action(MpcTokenContract.Invocations.BYOC_TRANSFER_OLD)
  public MpcTokenContractState transferByocOld(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress recipient,
      long amount,
      String symbol) {
    return executeTransfer(
        context, state, recipient, Unsigned256.create(amount), symbol, context.getFrom());
  }

  /**
   * Transfer BYOC from sender to a receiver.
   *
   * @pre
   *     <ul>
   *       <li>Sender and receiver are different accounts.
   *       <li>The amount <var>x &gt; 0</var>.
   *       <li>A transfer costs gas corresponding to <var>g</var> BYOC.
   *       <li>Sender has <var>y</var> BYOC and sends gas corresponding to <var>f</var> BYOC with
   *           the transaction, such that <var>y &ge; f &ge; g+x</var>.
   *       <li>Receiver has <var>z</var> BYOC.
   *       <li>Callbacks of transfer event cost gas corresponding to <var>i</var> BYOC.
   *     </ul>
   *
   * @post
   *     <ul>
   *       <li>A transfer event is added to state.
   *       <li>Sender now has <var>y :=y - g</var> BYOC.
   *       <li>Transaction now has <var>f := f - g</var> gas left.
   *       <li>If <var>f &ge; x + i</var> (Commit):
   *           <ul>
   *             <li>Receiver balance is <var>z + x</var> BYOC.
   *             <li>Sender balance is <var>y - x</var> BYOC.
   *             <li>Transfer event is removed from state.
   *           </ul>
   *       <li>Else if <var>f &lt; x &amp; f &ge; i</var> (Abort):
   *           <ul>
   *             <li>Receiver balance remains <var>z</var> BYOC.
   *             <li>Sender balance remains <var>y</var> BYOC.
   *             <li>Transfer event is removed from state.
   *           </ul>
   *       <li>Else (<var>f &lt; i</var>):
   *           <ul>
   *             <li>Receiver balance remains <var>z</var> BYOC.
   *             <li>Sender balance is <var>y - x</var> BYOC.
   *             <li>Transfer event remains in state.
   *           </ul>
   *     </ul>
   *
   * @param context execution context
   * @param state current state
   * @param recipient receiver of tokens
   * @param amount number of tokens to transfer
   * @param symbol the BYOC symbol to transfer
   * @return updated state
   */
  @Action(MpcTokenContract.Invocations.BYOC_TRANSFER)
  public MpcTokenContractState transferByoc(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress recipient,
      Unsigned256 amount,
      String symbol) {
    return executeTransfer(context, state, recipient, amount, symbol, context.getFrom());
  }

  /**
   * Transfer BYOC on behalf of another account.
   *
   * <p>See {@link MpcTokenContract#transferByoc} for pre- and post-conditions.
   *
   * @param context execution context
   * @param state current state
   * @param target address of the account to transfer on behalf of
   * @param recipient receiver of tokens
   * @param amount number of tokens to transfer
   * @param symbol the BYOC symbol to transfer
   * @return updated state
   */
  @Action(Invocations.BYOC_TRANSFER_ON_BEHALF_OF)
  public MpcTokenContractState transferByocOnBehalfOf(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress target,
      BlockchainAddress recipient,
      Unsigned256 amount,
      String symbol) {
    return executeTransfer(context, state, recipient, amount, symbol, target);
  }

  /**
   * Ice-associate tokens to a contract for the sake of keeping it alive should it run out of gas.
   *
   * @param context execution context
   * @param state current state
   * @param contract address of the contract to ice-associate tokens to
   * @param amount amount of tokens to ice-associate
   * @return updated state
   */
  @Action(Invocations.ASSOCIATE_ICE)
  public MpcTokenContractState associateIce(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress contract,
      long amount) {
    ensure(
        contract.getType() == BlockchainAddress.Type.CONTRACT_PUBLIC,
        "Can only ice stake against contracts");
    ensure(amount > 0, "Amount must be positive");
    BlockchainAddress sender = context.getFrom();
    ensure(sender.getType() == BlockchainAddress.Type.ACCOUNT, "Can only ice stake from accounts");
    Hash transactionId = context.getCurrentTransactionHash();

    SystemEventManager eventManager = context.getRemoteCallsCreator();

    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(
            sender, AccountPluginRpc.initiateIceAssociate(transactionId, amount, contract)));

    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(
            contract,
            AccountPluginRpc.initiateIceAssociateContract(transactionId, amount, sender)));

    eventManager.registerCallback(
        Callbacks.iceTokensStaking(transactionId, sender, contract), TWO_PHASE_CALLBACK_GAS_COST);

    IceStaking iceStaking = new IceStaking(sender, contract, amount, sender);
    return state.addIcedTokens(transactionId, iceStaking);
  }

  /**
   * Ice-associate tokens to a contract on behalf of another account.
   *
   * <p>See {@link MpcTokenContract#associateIce} for pre- and post-conditions.
   *
   * @param context execution context
   * @param state current state
   * @param target address of the account to ice-associate on behalf of
   * @param contract address of the contract to ice-associate tokens to
   * @param amount amount of tokens to ice-associate
   * @return updated state
   */
  @Action(Invocations.ASSOCIATE_ICE_ON_BEHALF_OF)
  public MpcTokenContractState associateIceOnBehalfOf(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress target,
      BlockchainAddress contract,
      long amount) {
    ensure(
        contract.getType() == BlockchainAddress.Type.CONTRACT_PUBLIC,
        "Can only ice stake against contracts");
    ensure(amount > 0, "Amount must be positive");
    BlockchainAddress custodian = context.getFrom();
    ensure(target.getType() == BlockchainAddress.Type.ACCOUNT, "Can only ice stake from accounts");
    Hash transactionId = context.getCurrentTransactionHash();

    SystemEventManager eventManager = context.getRemoteCallsCreator();

    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(
            target,
            AccountPluginRpc.initiateIceAssociateOnBehalfOf(
                custodian, transactionId, amount, contract)));

    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(
            contract,
            AccountPluginRpc.initiateIceAssociateContract(transactionId, amount, target)));

    eventManager.registerCallback(
        Callbacks.iceTokensStaking(transactionId, target, contract), TWO_PHASE_CALLBACK_GAS_COST);

    IceStaking iceStaking = new IceStaking(target, contract, amount, custodian);
    return state.addIcedTokens(transactionId, iceStaking);
  }

  /**
   * Disassociate ice-associated tokens from contract.
   *
   * @param context execution context
   * @param state current state
   * @param contract address of contract to disassociate ice-associated tokens from
   * @param amount amount of tokens to disassociate
   * @return updated state
   */
  @Action(Invocations.DISASSOCIATE_ICE)
  public MpcTokenContractState disassociateIce(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress contract,
      long amount) {
    ensure(amount > 0, "Can only disassociate a positive amount");
    ensure(
        contract.getType() == BlockchainAddress.Type.CONTRACT_PUBLIC,
        "Can only disassociate ice stakes from contracts");
    BlockchainAddress sender = context.getFrom();
    ensure(
        sender.getType() == BlockchainAddress.Type.ACCOUNT,
        "Only accounts can disassociate ice stakes");
    Hash transactionId = context.getCurrentTransactionHash();

    SystemEventManager eventManager = context.getRemoteCallsCreator();

    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(
            sender, AccountPluginRpc.initiateIceDisassociate(transactionId, amount, contract)));

    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(
            contract,
            AccountPluginRpc.initiateIceDisassociateContract(transactionId, amount, sender)));

    eventManager.registerCallback(
        Callbacks.iceTokensStaking(transactionId, sender, contract), TWO_PHASE_CALLBACK_GAS_COST);

    IceStaking iceStaking = new IceStaking(sender, contract, amount, sender);
    return state.addIcedTokens(transactionId, iceStaking);
  }

  /**
   * Dissociate ice-associated tokens from a contract on behalf of another account.
   *
   * <p>See {@link MpcTokenContract#associateIce} for pre- and post-conditions.
   *
   * @param context execution context
   * @param state current state
   * @param target address of the account to disassociate on behalf of
   * @param contract address of contract to disassociate ice-associated tokens from
   * @param amount amount of tokens to disassociate
   * @return updated state
   */
  @Action(Invocations.DISASSOCIATE_ICE_ON_BEHALF_OF)
  public MpcTokenContractState disassociateIceOnBehalfOf(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress target,
      BlockchainAddress contract,
      long amount) {
    ensure(amount > 0, "Can only disassociate a positive amount");
    ensure(
        contract.getType() == BlockchainAddress.Type.CONTRACT_PUBLIC,
        "Can only disassociate ice stakes from contracts");
    BlockchainAddress custodian = context.getFrom();
    ensure(
        target.getType() == BlockchainAddress.Type.ACCOUNT,
        "Only accounts can disassociate ice stakes");
    Hash transactionId = context.getCurrentTransactionHash();
    SystemEventManager eventManager = context.getRemoteCallsCreator();

    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(
            target,
            AccountPluginRpc.initiateIceDisassociateOnBehalfOf(
                custodian, transactionId, amount, contract)));

    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(
            contract,
            AccountPluginRpc.initiateIceDisassociateContract(transactionId, amount, target)));

    eventManager.registerCallback(
        Callbacks.iceTokensStaking(transactionId, target, contract), TWO_PHASE_CALLBACK_GAS_COST);

    IceStaking iceStaking = new IceStaking(target, contract, amount, custodian);
    return state.addIcedTokens(transactionId, iceStaking);
  }

  /**
   * Aborts a pending ice-associate transaction.
   *
   * @pre
   *     <ul>
   *       <li>Can only be called by an account.
   *       <li>An existing ice staking event is provided.
   *       <li>Account is sender of the ice staking event.
   *       <li>Ice staking event tried to associate <var>x</var> tokens from sender to contract but
   *           failed.
   *       <li>Sender has <var>y</var> tokens staked to contract.
   *       <li>Contract has <var>y</var> tokens staked by sender.
   *     </ul>
   *
   * @post
   *     <ul>
   *       <li>Sender still has <var>y</var> tokens staked to contract.
   *       <li>Contract still has <var>y</var> tokens staked by sender.
   *       <li>The failed ice staking event is removed from the state
   *     </ul>
   *
   * @param context execution context
   * @param state current state
   * @param transactionId ID of the ice-associate event to abort
   * @return updated state
   */
  @Action(Invocations.ABORT_ICE_ASSOCIATE)
  public MpcTokenContractState abortIceAssociate(
      SysContractContext context, MpcTokenContractState state, Hash transactionId) {

    AvlTree<Hash, IceStaking> iceStakings = state.getIcedStakings();
    if (!iceStakings.containsKey(transactionId)) {
      throw new IllegalArgumentException("Cannot abort action for id that does not exist");
    }

    BlockchainAddress sender = context.getFrom();
    IceStaking iceStake = iceStakings.getValue(transactionId);
    if (!sender.equals(iceStake.initiator())) {
      throw new IllegalArgumentException(
          "Cannot abort an ice association event that you are not the original sender of");
    }

    invokeAbortIceAssociation(
        transactionId, iceStake.sender(), iceStake.contract(), context.getInvocationCreator());
    return state.removeIcedTokens(transactionId);
  }

  /**
   * Appoint a new custodian for an account. The initial custodian has to be appointed by the owner
   * of the account. Appointing a new custodian for an account has to be done by the current
   * custodian.
   *
   * @param context execution context
   * @param state current state
   * @param target address of the account to appoint a new custodian on behalf of. If appointing the
   *     initial custodian this will be the same address as the sender
   * @param appointedCustodian address of the new custodian to appoint
   * @return unchanged state
   */
  @Action(Invocations.APPOINT_CUSTODIAN)
  public MpcTokenContractState appointCustodian(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress target,
      BlockchainAddress appointedCustodian) {

    BlockchainAddress sender = context.getFrom();

    byte[] rpc;
    if (isAccountOwner(target, sender)) {
      rpc = AccountPluginRpc.appointCustodian(appointedCustodian);
    } else {
      rpc = AccountPluginRpc.appointCustodianOnBehalfOf(sender, appointedCustodian);
    }

    context
        .getInvocationCreator()
        .updateLocalAccountPluginState(LocalPluginStateUpdate.create(target, rpc));
    return state;
  }

  /**
   * Cancel an appointed custodian for an account. Cancelling the appointment of the initial
   * custodian can only be done by the owner of the account. Cancelling the appointment of a new
   * custodian for an account has to be done by the current custodian.
   *
   * @param context execution context
   * @param state current state
   * @param target the address of the account to cancel the appointed custodian on behalf of. If
   *     cancelling the initial custodian this will be the same address as the sender
   * @return unchanged state
   */
  @Action(Invocations.CANCEL_APPOINTED_CUSTODIAN)
  public MpcTokenContractState cancelAppointedCustodian(
      SysContractContext context, MpcTokenContractState state, BlockchainAddress target) {

    BlockchainAddress sender = context.getFrom();

    byte[] rpc;
    if (isAccountOwner(target, sender)) {
      rpc = AccountPluginRpc.cancelAppointedCustodian();
    } else {
      rpc = AccountPluginRpc.cancelAppointedCustodianOnBehalfOf(sender);
    }

    context
        .getInvocationCreator()
        .updateLocalAccountPluginState(LocalPluginStateUpdate.create(target, rpc));
    return state;
  }

  /**
   * Reset the custodian of an account. Can only be done by the custodian of an account. If a
   * custodian is currently appointed for the account, this appointed custodian is cancelled.
   *
   * @param context execution context
   * @param state current state
   * @param target the address of the account to reset the custodian on
   * @return unchanged state
   */
  @Action(Invocations.RESET_CUSTODIAN)
  public MpcTokenContractState resetCustodian(
      SysContractContext context, MpcTokenContractState state, BlockchainAddress target) {

    BlockchainAddress sender = context.getFrom();

    context
        .getInvocationCreator()
        .updateLocalAccountPluginState(
            LocalPluginStateUpdate.create(
                target, AccountPluginRpc.resetCustodianOnBehalfOf(sender)));
    return state;
  }

  /**
   * Accept the role as custodian for an account. The sender has to be appointed as custodian on the
   * target account to be able to accept custodianship.
   *
   * @param context execution context
   * @param state current state
   * @param target address of account to accept role as custodian for
   * @return unchanged state
   */
  @Action(Invocations.ACCEPT_CUSTODIANSHIP)
  public MpcTokenContractState acceptCustodianship(
      SysContractContext context, MpcTokenContractState state, BlockchainAddress target) {

    BlockchainAddress sender = context.getFrom();
    context
        .getInvocationCreator()
        .updateLocalAccountPluginState(
            LocalPluginStateUpdate.create(target, AccountPluginRpc.acceptCustodianship(sender)));
    return state;
  }

  /**
   * Lock or unlock MPC token transfers. When locked, MPC transfers are disabled. Locking or
   * unlocking MPC transfers can only be done through a system update.
   *
   * @param context execution context
   * @param state current state
   * @param locked {@code true} if MPC transfers should be locked, otherwise {@code false}
   * @return updated state
   */
  @Action(Invocations.LOCK_MPC_TRANSFERS)
  public MpcTokenContractState lockMpcTransfers(
      SysContractContext context, MpcTokenContractState state, boolean locked) {

    BlockchainAddress sender = context.getFrom();
    ensure(
        sender.equals(SYSTEM_UPDATE_CONTRACT_ADDRESS),
        "Locking or unlocking MPC transfers can only be done through a system update");

    if (locked) {
      return state.lockTransfers();
    } else {
      return state.unlockTransfers();
    }
  }

  MpcTokenContractState executeTransfer(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress recipient,
      Unsigned256 amount,
      String symbol,
      BlockchainAddress target) {
    assertAccount(
        recipient, "Only blockchain addresses of type ACCOUNT can receive tokens or coins");
    ensure(amount.compareTo(Unsigned256.ZERO) > 0, "Amount must be positive");

    BlockchainAddress initiator = context.getFrom();

    if (target.equals(recipient)) {
      throw new IllegalArgumentException("Cannot transfer between same account");
    }

    TransferInformation transfer =
        TransferInformation.create(target, recipient, amount, symbol, initiator);

    Hash transactionId = context.getCurrentTransactionHash();
    if (transfer.symbol == null) {
      initiateTokensTransfer(context, transactionId, transfer);
    } else {
      initiateByocTransfer(context, transactionId, transfer);
    }
    return state.addTransfer(transactionId, transfer);
  }

  MpcTokenContractState executeDelegateStakes(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress recipient,
      long amount,
      BlockchainAddress target,
      Long expirationTimestamp) {
    assertAccount(
        recipient,
        "Only blockchain addresses of type ACCOUNT can receive delegated stakes"
            + " from another account");
    ensure(amount > 0, "Amount must be positive");

    BlockchainAddress initiator = context.getFrom();

    if (target.equals(recipient)) {
      throw new IllegalArgumentException(
          "Cannot delegate stakes between the same account using this operation");
    }

    StakeDelegation stake =
        StakeDelegation.create(
            target,
            recipient,
            amount,
            StakeDelegation.DelegationType.DELEGATE_STAKES,
            initiator,
            expirationTimestamp);

    Hash transactionId = context.getCurrentTransactionHash();
    initiateDelegateStakes(context, transactionId, stake);
    return state.addStakeDelegation(transactionId, stake);
  }

  MpcTokenContractState executeRetractDelegatedStakes(
      SysContractContext context,
      MpcTokenContractState state,
      BlockchainAddress recipient,
      long amount,
      BlockchainAddress target) {
    assertAccount(
        recipient,
        "Only blockchain addresses of type ACCOUNT can get their delegated stakes retracted"
            + " from another account");
    ensure(amount > 0, "Amount must be positive");

    BlockchainAddress initiator = context.getFrom();

    if (target.equals(recipient)) {
      throw new IllegalArgumentException(
          "Cannot retract delegated stakes between the same account using this operation");
    }

    StakeDelegation stake =
        StakeDelegation.create(
            target,
            recipient,
            amount,
            StakeDelegation.DelegationType.RETRACT_DELEGATED_STAKES,
            initiator,
            null);

    Hash transactionId = context.getCurrentTransactionHash();
    initiateRetractDelegatedStakes(context, transactionId, stake);
    return state.addStakeDelegation(transactionId, stake);
  }

  void initiateTokensTransfer(
      SysContractContext context, Hash transactionId, TransferInformation transferInformation) {
    BlockchainAddress sender = transferInformation.sender;
    BlockchainAddress recipient = transferInformation.recipient;
    BlockchainAddress initiator = transferInformation.initiator;
    long amount = transferInformation.amount.longValueExact();

    SystemEventManager eventManager = context.getRemoteCallsCreator();
    eventManager.registerCallback(
        Callbacks.tokenTransfer(transactionId, sender, recipient), TWO_PHASE_CALLBACK_GAS_COST);

    byte[] rpc;
    if (transferInformation.isOnBehalfOfOther()) {
      rpc = AccountPluginRpc.initiateWithdrawOnBehalfOf(initiator, transactionId, amount);
    } else {
      rpc = AccountPluginRpc.initiateWithdraw(transactionId, amount);
    }

    eventManager.updateLocalAccountPluginState(LocalPluginStateUpdate.create(sender, rpc));
    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(
            recipient, AccountPluginRpc.initiateDeposit(transactionId, amount)));
  }

  void initiateByocTransfer(
      SysContractContext context, Hash transactionId, TransferInformation transferInformation) {
    BlockchainAddress sender = transferInformation.sender;
    BlockchainAddress recipient = transferInformation.recipient;
    BlockchainAddress initiator = transferInformation.initiator;
    Unsigned256 amount = transferInformation.amount;
    String symbol = transferInformation.symbol;

    SystemEventManager eventManager = context.getRemoteCallsCreator();
    eventManager.registerCallback(
        Callbacks.tokenTransfer(transactionId, sender, recipient), TWO_PHASE_CALLBACK_GAS_COST);

    byte[] rpc;
    if (transferInformation.isOnBehalfOfOther()) {
      rpc =
          AccountPluginRpc.initByocTransferSenderOnBehalfOf(
              initiator, transactionId, amount, symbol);
    } else {
      rpc = AccountPluginRpc.initiateByocTransferSender(transactionId, amount, symbol);
    }

    eventManager.updateLocalAccountPluginState(LocalPluginStateUpdate.create(sender, rpc));
    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(
            recipient,
            AccountPluginRpc.initiateByocTransferRecipient(transactionId, amount, symbol)));
  }

  void initiateDelegateStakes(
      SysContractContext context, Hash transactionId, StakeDelegation stakeDelegation) {
    BlockchainAddress sender = stakeDelegation.sender;
    BlockchainAddress recipient = stakeDelegation.recipient;
    BlockchainAddress initiator = stakeDelegation.initiator;
    long amount = stakeDelegation.amount;

    SystemEventManager eventManager = context.getRemoteCallsCreator();
    eventManager.registerCallback(
        Callbacks.stakeDelegation(transactionId, sender, recipient), TWO_PHASE_CALLBACK_GAS_COST);

    byte[] rpc;
    if (stakeDelegation.isOnBehalfOfOther()) {
      rpc =
          AccountPluginRpc.initiateDelegateStakesOnBehalfOf(
              initiator, transactionId, amount, recipient);
    } else {
      rpc = AccountPluginRpc.initiateDelegateStakes(transactionId, amount, recipient);
    }

    Long expirationTimestamp = stakeDelegation.expirationTimestamp;
    eventManager.updateLocalAccountPluginState(LocalPluginStateUpdate.create(sender, rpc));
    if (expirationTimestamp == null) {
      eventManager.updateLocalAccountPluginState(
          LocalPluginStateUpdate.create(
              recipient,
              AccountPluginRpc.initiateReceiveDelegatedStakes(transactionId, amount, sender)));
    } else {
      eventManager.updateLocalAccountPluginState(
          LocalPluginStateUpdate.create(
              recipient,
              AccountPluginRpc.initiateReceiveDelegatedStakesWithExpiration(
                  transactionId, amount, sender, expirationTimestamp)));
    }
  }

  void initiateRetractDelegatedStakes(
      SysContractContext context, Hash transactionId, StakeDelegation stakeDelegation) {
    SystemEventManager eventManager = context.getRemoteCallsCreator();
    BlockchainAddress sender = stakeDelegation.sender;
    BlockchainAddress recipient = stakeDelegation.recipient;
    BlockchainAddress initiator = stakeDelegation.initiator;
    long amount = stakeDelegation.amount;

    byte[] rpc;
    if (stakeDelegation.isOnBehalfOfOther()) {
      rpc =
          AccountPluginRpc.initiateRetractDelegatedStakesOnBehalfOf(
              initiator, transactionId, amount, recipient);
    } else {
      rpc = AccountPluginRpc.initiateRetractDelegatedStakes(transactionId, amount, recipient);
    }

    eventManager.updateLocalAccountPluginState(LocalPluginStateUpdate.create(sender, rpc));
    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(
            recipient,
            AccountPluginRpc.initiateReturnDelegatedStakes(transactionId, amount, sender)));

    eventManager.registerCallback(
        Callbacks.stakeDelegation(transactionId, sender, recipient), TWO_PHASE_CALLBACK_GAS_COST);
  }

  private static void assertAccount(BlockchainAddress account, String message) {
    ensure(account.getType() == BlockchainAddress.Type.ACCOUNT, message);
  }

  private static boolean isAccountOwner(BlockchainAddress target, BlockchainAddress sender) {
    return target.equals(sender);
  }

  private static void ensure(boolean predicate, String errorString) {
    if (!predicate) {
      throw new RuntimeException(errorString);
    }
  }

  /**
   * Callback for disassociating all tokens for a removed contract.
   *
   * @param context execution context
   * @param state current state
   * @param callbackContext information about callbacks
   * @param contract address of the removed contract to disassociate all tokens from
   * @param sender address of the account having their tokens disassociated
   * @return updated state
   */
  @Callback(Callbacks.DISASSOCIATE_TOKENS_FOR_REMOVED_CONTRACT_CALLBACK)
  public MpcTokenContractState disassociateTokensForRemovedContractCallback(
      SysContractContext context,
      MpcTokenContractState state,
      CallbackContext callbackContext,
      BlockchainAddress contract,
      BlockchainAddress sender) {

    // Expects single result with a single boolean return value
    FixedList<CallbackContext.ExecutionResult> results = callbackContext.results();

    if (results.get(0).isSucceeded() && !results.get(0).returnValue().readBoolean()) {
      context
          .getInvocationCreator()
          .updateLocalAccountPluginState(
              LocalPluginStateUpdate.create(
                  sender, AccountPluginRpc.disassociateAllTokensForContract(contract)));
    }
    return state;
  }

  /**
   * Callback for transfer of tokens. Will commit or abort the initiated 2-phase transfer depending
   * on the execution status of the events.
   *
   * @param context execution context
   * @param state current state
   * @param callbackContext information about callbacks
   * @param transactionId id of the transaction initiating this transfer
   * @param sender the sender of tokens
   * @param recipient the receiver of tokens
   * @return updated state
   */
  @Callback(Callbacks.TRANSFER_CALLBACK)
  public MpcTokenContractState transferCallbackOnBehalfOf(
      SysContractContext context,
      MpcTokenContractState state,
      CallbackContext callbackContext,
      Hash transactionId,
      BlockchainAddress sender,
      BlockchainAddress recipient) {
    AvlTree<Hash, TransferInformation> transfers = state.getTransfers();
    if (!transfers.containsKey(transactionId)) {
      throw new IllegalArgumentException("Two phase already handled");
    }

    FixedList<CallbackContext.ExecutionResult> results = callbackContext.results();

    SystemEventCreator eventManager = context.getInvocationCreator();
    if (twoPhasePrepareSucceeded(results)) {
      invokeCommit(transactionId, sender, recipient, eventManager);
    } else {
      invokeAbort(transactionId, sender, recipient, eventManager);
    }
    return state.removeTransfer(transactionId);
  }

  /**
   * Callback for stake delegation events. Will commit or abort the initiated 2-phase transfer
   * depending on the execution status of the events.
   *
   * @param context execution context
   * @param state current state
   * @param callbackContext information about callbacks
   * @param transactionId id of the transaction initiating this event
   * @param sender the sender of the event
   * @param recipient the counterpart of the event
   * @return updated state
   */
  @Callback(Callbacks.STAKE_DELEGATION_CALLBACK)
  public MpcTokenContractState stakeDelegationCallbackOnBehalfOf(
      SysContractContext context,
      MpcTokenContractState state,
      CallbackContext callbackContext,
      Hash transactionId,
      BlockchainAddress sender,
      BlockchainAddress recipient) {

    FixedList<CallbackContext.ExecutionResult> results = callbackContext.results();

    AvlTree<Hash, StakeDelegation> transfers = state.getStakeDelegations();
    if (!transfers.containsKey(transactionId)) {
      throw new IllegalArgumentException("Two phase already handled");
    }

    SystemEventCreator eventManager = context.getInvocationCreator();
    if (twoPhasePrepareSucceeded(results)) {
      invokeCommitDelegateStakes(transactionId, sender, recipient, eventManager);
    } else {
      invokeAbortDelegateStakes(transactionId, sender, recipient, eventManager);
    }
    return state.removeStakeDelegation(transactionId);
  }

  /**
   * Callback for ice association events. Will commit or abort the initiated 2-phase association
   * depending on the execution status of the event.
   *
   * @param context execution context
   * @param state current state
   * @param callbackContext information about callbacks
   * @param transactionId id of the transaction initiating this event
   * @param sender the sender of the event
   * @param contract the contract of the event
   * @return updated state
   */
  @Callback(Callbacks.ICE_STAKES)
  public MpcTokenContractState iceAssociationCallbackOnBehalfOf(
      SysContractContext context,
      MpcTokenContractState state,
      CallbackContext callbackContext,
      Hash transactionId,
      BlockchainAddress sender,
      BlockchainAddress contract) {
    FixedList<CallbackContext.ExecutionResult> results = callbackContext.results();

    AvlTree<Hash, IceStaking> transfers = state.getIcedStakings();
    if (!transfers.containsKey(transactionId)) {
      throw new IllegalArgumentException("Two phase already handled");
    }

    SystemEventCreator eventManager = context.getInvocationCreator();
    if (twoPhasePrepareSucceeded(results)) {
      invokeCommitIceAssociation(transactionId, sender, contract, eventManager);
    } else {
      invokeAbortIceAssociation(transactionId, sender, contract, eventManager);
    }
    return state.removeIcedTokens(transactionId);
  }

  /**
   * Check if both part of the initial phase of a two-phase commit succeeded.
   *
   * @param results two execution results with empty return value
   * @return true if both of the executions succeeded, false otherwise
   */
  private static boolean twoPhasePrepareSucceeded(
      FixedList<CallbackContext.ExecutionResult> results) {
    return results.get(0).isSucceeded() && results.get(1).isSucceeded();
  }

  private static void invokeCommit(
      Hash transactionId,
      BlockchainAddress sender,
      BlockchainAddress recipient,
      SystemEventCreator eventManager) {

    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(sender, AccountPluginRpc.commitTransfer(transactionId)));

    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(recipient, AccountPluginRpc.commitTransfer(transactionId)));
  }

  private static void invokeAbort(
      Hash transactionId,
      BlockchainAddress sender,
      BlockchainAddress recipient,
      SystemEventCreator eventManager) {

    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(sender, AccountPluginRpc.abortTransfer(transactionId)));

    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(recipient, AccountPluginRpc.abortTransfer(transactionId)));
  }

  private static void invokeCommitDelegateStakes(
      Hash transactionId,
      BlockchainAddress sender,
      BlockchainAddress recipient,
      SystemEventCreator eventManager) {

    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(
            sender, AccountPluginRpc.commitDelegateStakes(transactionId)));

    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(
            recipient, AccountPluginRpc.commitDelegateStakes(transactionId)));
  }

  private static void invokeAbortDelegateStakes(
      Hash transactionId,
      BlockchainAddress sender,
      BlockchainAddress recipient,
      SystemEventCreator eventManager) {

    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(
            sender, AccountPluginRpc.abortDelegatedStakes(transactionId)));

    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(
            recipient, AccountPluginRpc.abortDelegatedStakes(transactionId)));
  }

  private static void invokeCommitIceAssociation(
      Hash transactionId,
      BlockchainAddress sender,
      BlockchainAddress contract,
      SystemEventCreator eventManager) {

    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(sender, AccountPluginRpc.commitIceAssociate(transactionId)));

    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(
            contract, AccountPluginRpc.commitIceAssociate(transactionId)));
  }

  private static void invokeAbortIceAssociation(
      Hash transactionId,
      BlockchainAddress sender,
      BlockchainAddress contract,
      SystemEventCreator eventManager) {

    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(sender, AccountPluginRpc.abortIceAssociate(transactionId)));

    eventManager.updateLocalAccountPluginState(
        LocalPluginStateUpdate.create(contract, AccountPluginRpc.abortIceAssociate(transactionId)));
  }

  /** Holder for invocation shortnames. */
  static final class Invocations {

    static final int STAKE_TOKENS = 0;
    static final int UNSTAKE_TOKENS = 1;
    static final int DISASSOCIATE_TOKENS_FOR_REMOVED_CONTRACT = 2;
    static final int TRANSFER = 3;
    static final int ABORT = 4;
    static final int CHECK_PENDING_UNSTAKES = 5;
    static final int CHECK_VESTED_TOKENS = 6;
    static final int TRANSFER_SMALL_MEMO = 13;
    static final int TRANSFER_LARGE_MEMO = 23;
    static final int DELEGATE_STAKES = 24;
    static final int RETRACT_DELEGATED_STAKES = 25;
    static final int ABORT_STAKE_DELEGATION_EVENT = 26;
    static final int ACCEPT_DELEGATED_STAKES = 27;
    static final int REDUCE_DELEGATED_STAKES = 28;
    static final int BYOC_TRANSFER_OLD = 29;
    static final int BYOC_TRANSFER = 30;
    static final int ASSOCIATE_ICE = 31;
    static final int DISASSOCIATE_ICE = 32;
    static final int APPOINT_CUSTODIAN = 33;
    static final int CANCEL_APPOINTED_CUSTODIAN = 34;
    static final int RESET_CUSTODIAN = 35;
    static final int ACCEPT_CUSTODIANSHIP = 36;
    static final int ABORT_ICE_ASSOCIATE = 37;
    static final int LOCK_MPC_TRANSFERS = 38;
    static final int TRANSFER_ON_BEHALF_OF = 40;
    static final int CHECK_PENDING_UNSTAKES_ON_BEHALF_OF = 42;
    static final int CHECK_VESTED_TOKENS_ON_BEHALF_OF = 43;
    static final int TRANSFER_SMALL_MEMO_ON_BEHALF_OF = 44;
    static final int TRANSFER_LARGE_MEMO_ON_BEHALF_OF = 45;
    static final int DELEGATE_STAKES_ON_BEHALF_OF = 46;
    static final int RETRACT_DELEGATED_STAKES_ON_BEHALF_OF = 47;
    static final int ACCEPT_DELEGATED_STAKES_ON_BEHALF_OF = 49;
    static final int REDUCE_DELEGATED_STAKES_ON_BEHALF_OF = 50;
    static final int BYOC_TRANSFER_ON_BEHALF_OF = 52;
    static final int ASSOCIATE_ICE_ON_BEHALF_OF = 53;
    static final int DISASSOCIATE_ICE_ON_BEHALF_OF = 54;
    static final int STAKE_TOKENS_ON_BEHALF_OF = 55;
    static final int UNSTAKE_TOKENS_ON_BEHALF_OF = 57;
    static final int CHECK_VESTED_TOKENS_FOR_OTHER = 58;
    static final int DISASSOCIATE_TOKENS_FOR_REMOVED_CONTRACT_FOR_OTHER = 59;
    static final int SET_STAKING_GOAL = 70;
    static final int SET_STAKING_GOAL_ON_BEHALF_OF = 71;
    static final int SET_EXPIRATION_FOR_DELEGATED_STAKES = 72;
    static final int SET_EXPIRATION_TIMESTAMP_FOR_DELEGATED_STAKES_ON_BEHALF_OF = 73;
    static final int DELEGATE_STAKES_WITH_EXPIRATION = 77;
    static final int DELEGATE_STAKES_WITH_EXPIRATION_ON_BEHALF_OF = 79;

    private Invocations() {}
  }

  /** Holder for callback shortnames. */
  static final class Callbacks {

    static final int DISASSOCIATE_TOKENS_FOR_REMOVED_CONTRACT_CALLBACK = 0;
    static final int TRANSFER_CALLBACK = 1;
    static final int STAKE_DELEGATION_CALLBACK = 2;
    static final int ICE_STAKES = 3;

    private Callbacks() {}

    static DataStreamSerializable stakeDelegation(
        Hash transactionId, BlockchainAddress sender, BlockchainAddress recipient) {
      return stream -> {
        stream.writeByte(STAKE_DELEGATION_CALLBACK);
        transactionId.write(stream);
        sender.write(stream);
        recipient.write(stream);
      };
    }

    static DataStreamSerializable iceTokensStaking(
        Hash transactionId, BlockchainAddress sender, BlockchainAddress contract) {
      return stream -> {
        stream.writeByte(ICE_STAKES);
        transactionId.write(stream);
        sender.write(stream);
        contract.write(stream);
      };
    }

    static DataStreamSerializable tokenTransfer(
        Hash transactionId, BlockchainAddress sender, BlockchainAddress recipient) {
      return stream -> {
        stream.writeByte(TRANSFER_CALLBACK);
        transactionId.write(stream);
        sender.write(stream);
        recipient.write(stream);
      };
    }

    static DataStreamSerializable disassociateTokensForRemovedContract(
        BlockchainAddress contract, BlockchainAddress sender) {
      return stream -> {
        stream.writeByte(DISASSOCIATE_TOKENS_FOR_REMOVED_CONTRACT_CALLBACK);
        contract.write(stream);
        sender.write(stream);
      };
    }
  }
}
