package com.partisiablockchain.governance.mpctoken;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.secata.stream.SafeDataOutputStream;

final class AccountPluginRpc {

  static final int DISASSOCIATE_ALL_TOKENS_FOR_CONTRACT_BYTE = 7;
  static final int INIT_DEPOSIT = 12;
  static final int COMMIT_TRANSFER = 13;
  static final int ABORT_TRANSFER = 14;
  static final int INIT_RECEIVE_DELEGATED_STAKES = 22;
  static final int INIT_RETURN_DELEGATED_STAKES = 24;
  static final int COMMIT_DELEGATE_STAKES = 25;
  static final int ABORT_DELEGATE_STAKES = 26;
  static final int INIT_BYOC_TRANSFER_RECIPIENT = 32;
  static final int INITIATE_ICE_ASSOCIATE_CONTRACT = 35;
  static final int INITIATE_ICE_DISASSOCIATE_CONTRACT = 36;
  static final int COMMIT_ICE = 37;
  static final int ABORT_ICE = 38;
  static final int APPOINT_CUSTODIAN = 39;
  static final int CANCEL_APPOINTED_CUSTODIAN = 40;
  static final int RESET_CUSTODIAN = 41;
  static final int ACCEPT_CUSTODIANSHIP = 42;
  static final int STAKE_TOKENS = 46;
  static final int UNSTAKE_TOKENS = 47;
  static final int INIT_WITHDRAW = 49;
  static final int CHECK_VESTED = 53;
  static final int CHECK_PENDING_UNSTAKES = 54;
  static final int INIT_DELEGATE_STAKES = 55;
  static final int INIT_RETRACT_DELEGATED_STAKES = 57;
  static final int ACCEPT_DELEGATED_STAKES = 61;
  static final int REDUCE_DELEGATED_STAKES = 62;
  static final int INIT_BYOC_TRANSFER_SENDER = 63;
  static final int INITIATE_ICE_ASSOCIATE = 65;
  static final int INITIATE_ICE_DISASSOCIATE = 66;
  static final int SET_STAKING_GOAL = 70;
  static final int SET_EXPIRATION_TIMESTAMP_FOR_DELEGATED_STAKES = 72;
  static final int INITIATE_RECEIVE_DELEGATED_STAKES_WITH_EXPIRATION = 87;

  @SuppressWarnings("unused")
  private AccountPluginRpc() {}

  static byte[] stakeTokens(long amount) {
    return stakeTokensOnBehalfOf(null, amount);
  }

  static byte[] stakeTokensOnBehalfOf(BlockchainAddress custodian, long amount) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(STAKE_TOKENS);
          stream.writeOptional(BlockchainAddress::write, custodian);
          stream.writeLong(amount);
        });
  }

  static byte[] setExpirationTimestamp(BlockchainAddress delegator, Long expirationTimestamp) {
    return setExpirationTimestampOnBehalfOf(null, delegator, expirationTimestamp);
  }

  static byte[] setExpirationTimestampOnBehalfOf(
      BlockchainAddress custodian, BlockchainAddress delegator, Long expirationTimestamp) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(SET_EXPIRATION_TIMESTAMP_FOR_DELEGATED_STAKES);
          stream.writeOptional(BlockchainAddress::write, custodian);
          delegator.write(stream);
          stream.writeOptional((value, rpc) -> rpc.writeLong(value), expirationTimestamp);
        });
  }

  static byte[] setStakingGoal(long amount) {
    return setStakingGoalOnBehalfOf(null, amount);
  }

  static byte[] setStakingGoalOnBehalfOf(BlockchainAddress custodian, long amount) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(SET_STAKING_GOAL);
          stream.writeOptional(BlockchainAddress::write, custodian);
          stream.writeLong(amount);
        });
  }

  static byte[] unstakeTokens(long amount) {
    return unstakeTokensOnBehalfOf(null, amount);
  }

  static byte[] unstakeTokensOnBehalfOf(BlockchainAddress custodian, long amount) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(UNSTAKE_TOKENS);
          stream.writeOptional(BlockchainAddress::write, custodian);
          stream.writeLong(amount);
        });
  }

  static byte[] checkPendingUnstakes() {
    return checkPendingUnstakesOnBehalfOf(null);
  }

  static byte[] checkPendingUnstakesOnBehalfOf(BlockchainAddress custodian) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(CHECK_PENDING_UNSTAKES);
          stream.writeOptional(BlockchainAddress::write, custodian);
        });
  }

  static byte[] checkVestedTokens() {
    return checkVestedTokensOnBehalfOf(null);
  }

  static byte[] checkVestedTokensOnBehalfOf(BlockchainAddress custodian) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(CHECK_VESTED);
          stream.writeOptional(BlockchainAddress::write, custodian);
        });
  }

  static byte[] disassociateAllTokensForContract(BlockchainAddress contract) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(DISASSOCIATE_ALL_TOKENS_FOR_CONTRACT_BYTE);
          contract.write(stream);
        });
  }

  static byte[] initiateIceAssociate(
      Hash transactionId, long amount, BlockchainAddress counterPart) {
    return initiateIceAssociateOnBehalfOf(null, transactionId, amount, counterPart);
  }

  static byte[] initiateIceAssociateOnBehalfOf(
      BlockchainAddress custodian, Hash transactionId, long amount, BlockchainAddress counterPart) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(INITIATE_ICE_ASSOCIATE);
          stream.writeOptional(BlockchainAddress::write, custodian);
          transactionId.write(stream);
          stream.writeLong(amount);
          counterPart.write(stream);
        });
  }

  static byte[] initiateIceAssociateContract(
      Hash transactionId, long amount, BlockchainAddress counterPart) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(INITIATE_ICE_ASSOCIATE_CONTRACT);
          transactionId.write(stream);
          stream.writeLong(amount);
          counterPart.write(stream);
        });
  }

  static byte[] initiateIceDisassociate(
      Hash transactionId, long amount, BlockchainAddress counterPart) {
    return initiateIceDisassociateOnBehalfOf(null, transactionId, amount, counterPart);
  }

  static byte[] initiateIceDisassociateOnBehalfOf(
      BlockchainAddress custodian, Hash transactionId, long amount, BlockchainAddress counterPart) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(INITIATE_ICE_DISASSOCIATE);
          stream.writeOptional(BlockchainAddress::write, custodian);
          transactionId.write(stream);
          stream.writeLong(amount);
          counterPart.write(stream);
        });
  }

  static byte[] initiateIceDisassociateContract(
      Hash transactionId, long amount, BlockchainAddress counterPart) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(INITIATE_ICE_DISASSOCIATE_CONTRACT);
          transactionId.write(stream);
          stream.writeLong(amount);
          counterPart.write(stream);
        });
  }

  static byte[] abortIceAssociate(Hash twoPhaseId) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(ABORT_ICE);
          twoPhaseId.write(stream);
        });
  }

  static byte[] commitIceAssociate(Hash twoPhaseId) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(COMMIT_ICE);
          twoPhaseId.write(stream);
        });
  }

  static byte[] initiateWithdraw(Hash transactionId, long amount) {
    return initiateWithdrawOnBehalfOf(null, transactionId, amount);
  }

  static byte[] initiateWithdrawOnBehalfOf(
      BlockchainAddress custodian, Hash transactionId, long amount) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(INIT_WITHDRAW);
          stream.writeOptional(BlockchainAddress::write, custodian);
          transactionId.write(stream);
          stream.writeLong(amount);
        });
  }

  static byte[] initiateDeposit(Hash transactionId, long amount) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(INIT_DEPOSIT);
          transactionId.write(stream);
          stream.writeLong(amount);
        });
  }

  static byte[] acceptDelegatedStakes(BlockchainAddress sender, long amount) {
    return acceptDelegatedStakesOnBehalfOf(null, sender, amount);
  }

  static byte[] acceptDelegatedStakesOnBehalfOf(
      BlockchainAddress custodian, BlockchainAddress sender, long amount) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(ACCEPT_DELEGATED_STAKES);
          stream.writeOptional(BlockchainAddress::write, custodian);
          sender.write(stream);
          stream.writeLong(amount);
        });
  }

  static byte[] reduceDelegatedStakes(BlockchainAddress delegator, long amount) {
    return reduceDelegatedStakesOnBehalfOf(null, delegator, amount);
  }

  static byte[] reduceDelegatedStakesOnBehalfOf(
      BlockchainAddress custodian, BlockchainAddress delegator, long amount) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(REDUCE_DELEGATED_STAKES);
          stream.writeOptional(BlockchainAddress::write, custodian);
          delegator.write(stream);
          stream.writeLong(amount);
        });
  }

  static byte[] initiateDelegateStakesOnBehalfOf(
      BlockchainAddress custodian, Hash transactionId, long amount, BlockchainAddress counterPart) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(INIT_DELEGATE_STAKES);
          stream.writeOptional(BlockchainAddress::write, custodian);
          transactionId.write(stream);
          stream.writeLong(amount);
          counterPart.write(stream);
        });
  }

  static byte[] initiateDelegateStakes(
      Hash transactionId, long amount, BlockchainAddress counterPart) {
    return initiateDelegateStakesOnBehalfOf(null, transactionId, amount, counterPart);
  }

  static byte[] initiateReceiveDelegatedStakes(
      Hash transactionId, long amount, BlockchainAddress counterPart) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(INIT_RECEIVE_DELEGATED_STAKES);
          transactionId.write(stream);
          stream.writeLong(amount);
          counterPart.write(stream);
        });
  }

  static byte[] initiateReceiveDelegatedStakesWithExpiration(
      Hash transactionId, long amount, BlockchainAddress counterPart, long expirationTimestamp) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(INITIATE_RECEIVE_DELEGATED_STAKES_WITH_EXPIRATION);
          transactionId.write(stream);
          stream.writeLong(amount);
          counterPart.write(stream);
          stream.writeLong(expirationTimestamp);
        });
  }

  static byte[] initiateRetractDelegatedStakesOnBehalfOf(
      BlockchainAddress custodian, Hash transactionId, long amount, BlockchainAddress counterPart) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(INIT_RETRACT_DELEGATED_STAKES);
          stream.writeOptional(BlockchainAddress::write, custodian);
          transactionId.write(stream);
          stream.writeLong(amount);
          counterPart.write(stream);
        });
  }

  static byte[] initiateRetractDelegatedStakes(
      Hash transactionId, long amount, BlockchainAddress counterPart) {
    return initiateRetractDelegatedStakesOnBehalfOf(null, transactionId, amount, counterPart);
  }

  static byte[] initiateReturnDelegatedStakes(
      Hash transactionId, long amount, BlockchainAddress counterPart) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(INIT_RETURN_DELEGATED_STAKES);
          transactionId.write(stream);
          stream.writeLong(amount);
          counterPart.write(stream);
        });
  }

  static byte[] initiateByocTransferSender(Hash transactionId, Unsigned256 amount, String symbol) {
    return initByocTransferSenderOnBehalfOf(null, transactionId, amount, symbol);
  }

  static byte[] initByocTransferSenderOnBehalfOf(
      BlockchainAddress custodian, Hash transactionId, Unsigned256 amount, String symbol) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(INIT_BYOC_TRANSFER_SENDER);
          stream.writeOptional(BlockchainAddress::write, custodian);
          transactionId.write(stream);
          amount.write(stream);
          stream.writeString(symbol);
        });
  }

  static byte[] initiateByocTransferRecipient(
      Hash transactionId, Unsigned256 amount, String symbol) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(INIT_BYOC_TRANSFER_RECIPIENT);
          transactionId.write(stream);
          amount.write(stream);
          stream.writeString(symbol);
        });
  }

  static byte[] abortTransfer(Hash twoPhaseId) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(ABORT_TRANSFER);
          twoPhaseId.write(stream);
        });
  }

  static byte[] abortDelegatedStakes(Hash twoPhaseId) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(ABORT_DELEGATE_STAKES);
          twoPhaseId.write(stream);
        });
  }

  static byte[] commitTransfer(Hash twoPhaseId) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(COMMIT_TRANSFER);
          twoPhaseId.write(stream);
        });
  }

  static byte[] commitDelegateStakes(Hash twoPhaseId) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(COMMIT_DELEGATE_STAKES);
          twoPhaseId.write(stream);
        });
  }

  public static byte[] appointCustodian(BlockchainAddress appointedCustodian) {
    return appointCustodianOnBehalfOf(null, appointedCustodian);
  }

  public static byte[] appointCustodianOnBehalfOf(
      BlockchainAddress custodian, BlockchainAddress appointedCustodian) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(APPOINT_CUSTODIAN);
          stream.writeOptional(BlockchainAddress::write, custodian);
          appointedCustodian.write(stream);
        });
  }

  static byte[] cancelAppointedCustodian() {
    return cancelAppointedCustodianOnBehalfOf(null);
  }

  static byte[] cancelAppointedCustodianOnBehalfOf(BlockchainAddress custodian) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(CANCEL_APPOINTED_CUSTODIAN);
          stream.writeOptional(BlockchainAddress::write, custodian);
        });
  }

  static byte[] resetCustodianOnBehalfOf(BlockchainAddress sender) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(RESET_CUSTODIAN);
          sender.write(stream);
        });
  }

  public static byte[] acceptCustodianship(BlockchainAddress sender) {
    return SafeDataOutputStream.serialize(
        stream -> {
          stream.writeByte(ACCEPT_CUSTODIANSHIP);
          sender.write(stream);
        });
  }
}
