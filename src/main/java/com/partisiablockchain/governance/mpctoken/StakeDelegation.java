package com.partisiablockchain.governance.mpctoken;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;

/** Information about a stake delegation event for the two-phase commit protocol. */
@Immutable
final class StakeDelegation implements StateSerializable {
  final BlockchainAddress sender;
  final BlockchainAddress recipient;
  final long amount;
  final DelegationType delegationType;
  final BlockchainAddress initiator;
  final Long expirationTimestamp;

  @SuppressWarnings("unused")
  private StakeDelegation() {
    sender = null;
    recipient = null;
    amount = 0;
    delegationType = null;
    initiator = null;
    expirationTimestamp = null;
  }

  private StakeDelegation(
      BlockchainAddress sender,
      BlockchainAddress receiver,
      long amount,
      DelegationType delegationType,
      BlockchainAddress initiator,
      Long expirationTimestamp) {
    this.sender = sender;
    this.recipient = receiver;
    this.amount = amount;
    this.delegationType = delegationType;
    this.initiator = initiator;
    this.expirationTimestamp = expirationTimestamp;
  }

  public static StakeDelegation create(
      BlockchainAddress sender,
      BlockchainAddress receiver,
      long amount,
      DelegationType delegationType,
      BlockchainAddress initiator,
      Long expirationTimestamp) {
    return new StakeDelegation(
        sender, receiver, amount, delegationType, initiator, expirationTimestamp);
  }

  @SuppressWarnings("EnumOrdinal")
  static StakeDelegation createFromStateAccessor(StateAccessor accessor) {
    Enum<?> enumValue = accessor.get("delegationType").cast(Enum.class);
    BlockchainAddress sender = accessor.get("sender").blockchainAddressValue();
    BlockchainAddress initiator = accessor.get("initiator").blockchainAddressValue();
    Long expirationTimestamp = null;
    if (accessor.hasField("expirationTimestamp")) {
      expirationTimestamp = accessor.get("expirationTimestamp").cast(Long.class);
    }
    return create(
        sender,
        accessor.get("recipient").blockchainAddressValue(),
        accessor.get("amount").longValue(),
        DelegationType.values()[enumValue.ordinal()],
        initiator,
        expirationTimestamp);
  }

  /**
   * Indicates if this stake delegation was made on behalf of another account.
   *
   * @return {@code true} if the stake delegation was made on behalf of other, otherwise {@code
   *     false}.
   */
  public boolean isOnBehalfOfOther() {
    return !initiator.equals(sender);
  }

  /** The different types of delegation of stakes. */
  public enum DelegationType {
    /** Delegate stakes to another account. */
    DELEGATE_STAKES,
    /** Retract delegated stakes from another account. */
    RETRACT_DELEGATED_STAKES
  }
}
