package com.partisiablockchain.governance.mpctoken;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;

/** State for the {@link MpcTokenContract} containing transfers in progress. */
@Immutable
public final class MpcTokenContractState implements StateSerializable {

  private final AvlTree<Hash, TransferInformation> transfers;
  private final boolean locked;
  private final AvlTree<Hash, StakeDelegation> stakeDelegations;

  private final AvlTree<Hash, IceStaking> icedStakings;

  @SuppressWarnings("unused")
  private MpcTokenContractState() {
    this.transfers = null;
    this.locked = false;
    this.stakeDelegations = null;
    this.icedStakings = null;
  }

  MpcTokenContractState(
      AvlTree<Hash, TransferInformation> transfers,
      AvlTree<Hash, StakeDelegation> stakeDelegations,
      AvlTree<Hash, IceStaking> icedStakings,
      boolean locked) {
    this.transfers = transfers;
    this.locked = locked;
    this.stakeDelegations = stakeDelegations;
    this.icedStakings = icedStakings;
  }

  static MpcTokenContractState initial(boolean locked) {
    return new MpcTokenContractState(AvlTree.create(), AvlTree.create(), AvlTree.create(), locked);
  }

  MpcTokenContractState addTransfer(Hash transactionId, TransferInformation transfer) {
    AvlTree<Hash, TransferInformation> updatedTransfers = transfers.set(transactionId, transfer);
    return new MpcTokenContractState(updatedTransfers, stakeDelegations, icedStakings, locked);
  }

  MpcTokenContractState addStakeDelegation(Hash transactionId, StakeDelegation stake) {
    AvlTree<Hash, StakeDelegation> updatedStakes = stakeDelegations.set(transactionId, stake);
    return new MpcTokenContractState(transfers, updatedStakes, icedStakings, locked);
  }

  MpcTokenContractState addIcedTokens(Hash transactionId, IceStaking iceStaking) {
    AvlTree<Hash, IceStaking> updatedIced = icedStakings.set(transactionId, iceStaking);
    return new MpcTokenContractState(transfers, stakeDelegations, updatedIced, locked);
  }

  MpcTokenContractState removeIcedTokens(Hash transactionId) {
    AvlTree<Hash, IceStaking> updatedIced = icedStakings.remove(transactionId);
    return new MpcTokenContractState(transfers, stakeDelegations, updatedIced, locked);
  }

  AvlTree<Hash, TransferInformation> getTransfers() {
    return transfers;
  }

  AvlTree<Hash, StakeDelegation> getStakeDelegations() {
    return stakeDelegations;
  }

  AvlTree<Hash, IceStaking> getIcedStakings() {
    return icedStakings;
  }

  MpcTokenContractState removeTransfer(Hash transactionId) {
    AvlTree<Hash, TransferInformation> updatedTransfer = transfers.remove(transactionId);
    return new MpcTokenContractState(updatedTransfer, stakeDelegations, icedStakings, locked);
  }

  MpcTokenContractState removeStakeDelegation(Hash transactionId) {
    AvlTree<Hash, StakeDelegation> updatedStakes = stakeDelegations.remove(transactionId);
    return new MpcTokenContractState(transfers, updatedStakes, icedStakings, locked);
  }

  MpcTokenContractState lockTransfers() {
    return new MpcTokenContractState(transfers, stakeDelegations, icedStakings, true);
  }

  MpcTokenContractState unlockTransfers() {
    return new MpcTokenContractState(transfers, stakeDelegations, icedStakings, false);
  }

  boolean isLocked() {
    return locked;
  }
}
