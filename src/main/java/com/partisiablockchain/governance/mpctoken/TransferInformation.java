package com.partisiablockchain.governance.mpctoken;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;

/** Information about an action for the two-phase commit protocol. */
@Immutable
final class TransferInformation implements StateSerializable {
  final BlockchainAddress sender;
  final BlockchainAddress recipient;
  final Unsigned256 amount;
  final String symbol;
  final BlockchainAddress initiator;

  @SuppressWarnings("unused")
  private TransferInformation() {
    sender = null;
    recipient = null;
    amount = Unsigned256.ZERO;
    symbol = null;
    initiator = null;
  }

  private TransferInformation(
      BlockchainAddress sender,
      BlockchainAddress recipient,
      Unsigned256 amount,
      String symbol,
      BlockchainAddress initiator) {
    this.sender = sender;
    this.recipient = recipient;
    this.initiator = initiator;
    this.amount = amount;
    this.symbol = symbol;
  }

  public static TransferInformation create(
      BlockchainAddress sender,
      BlockchainAddress recipient,
      Unsigned256 amount,
      String symbol,
      BlockchainAddress initiator) {
    return new TransferInformation(sender, recipient, amount, symbol, initiator);
  }

  static TransferInformation createFromStateAccessor(StateAccessor accessor) {
    BlockchainAddress sender = accessor.get("sender").blockchainAddressValue();
    BlockchainAddress initiator = accessor.get("initiator").blockchainAddressValue();

    return create(
        sender,
        accessor.get("recipient").blockchainAddressValue(),
        accessor.get("amount").cast(Unsigned256.class),
        accessor.get("symbol").stringValue(),
        initiator);
  }

  /**
   * Indicates if this transfer was made on behalf of another account.
   *
   * @return {@code true} if the transfer was made on behalf of other, otherwise {@code false}.
   */
  public boolean isOnBehalfOfOther() {
    return !initiator.equals(sender);
  }
}
