package com.partisiablockchain.governance.mpctoken;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;

import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.secata.stream.SafeDataInputStream;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

final class AccountPluginRpcTest {

  private static final BlockchainAddress ADDRESS_A =
      BlockchainAddress.fromString("000000000000000000000000000000000000000001");
  private static final BlockchainAddress ADDRESS_B =
      BlockchainAddress.fromString("000000000000000000000000000000000000000002");

  @Test
  void stakeTokens() {
    long amount = 1234;

    byte[] rpc = AccountPluginRpc.stakeTokensOnBehalfOf(ADDRESS_B, amount);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.STAKE_TOKENS);
    assertThat(stream.readOptional(BlockchainAddress::read)).isEqualTo(ADDRESS_B);
    assertThat(stream.readLong()).isEqualTo(amount);
  }

  /** Set staking goal, reads the expected inputted bytes. */
  @Test
  void setStakingGoal() {
    long amount = 1234;
    byte[] rpc = AccountPluginRpc.setStakingGoal(amount);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.SET_STAKING_GOAL);
    assertThat(stream.readOptional(BlockchainAddress::read)).isNull();
    assertThat(stream.readLong()).isEqualTo(amount);
  }

  @Test
  @DisplayName("Set expiration timestamp for delegation, reads the expected inputted bytes.")
  void setExpirationForDelegation() {
    Long expirationTimestamp = 1234L;
    byte[] rpc = AccountPluginRpc.setExpirationTimestamp(ADDRESS_A, expirationTimestamp);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte())
        .isEqualTo(AccountPluginRpc.SET_EXPIRATION_TIMESTAMP_FOR_DELEGATED_STAKES);
    assertThat(stream.readOptional(BlockchainAddress::read)).isNull();
    assertThat(BlockchainAddress.read(stream)).isEqualTo(ADDRESS_A);
    Long readExpiration = stream.readOptional(SafeDataInputStream::readLong);
    assertThat(readExpiration).isEqualTo(expirationTimestamp);
  }

  @Test
  @DisplayName(
      "Set expiration timestamp for delegation on behalf of, reads the expected inputted bytes.")
  void setExpirationForDelegationOnBehalfOf() {
    Long expirationTimestamp = 1234L;
    byte[] rpc =
        AccountPluginRpc.setExpirationTimestampOnBehalfOf(
            ADDRESS_B, ADDRESS_A, expirationTimestamp);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte())
        .isEqualTo(AccountPluginRpc.SET_EXPIRATION_TIMESTAMP_FOR_DELEGATED_STAKES);
    assertThat(stream.readOptional(BlockchainAddress::read)).isEqualTo(ADDRESS_B);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(ADDRESS_A);
    Long readExpiration = stream.readOptional(SafeDataInputStream::readLong);
    assertThat(readExpiration).isEqualTo(expirationTimestamp);
  }

  @Test
  @DisplayName("Set staking goal on behalf of another account, reads the expected inputted bytes.")
  void setStakingGoalOnBehalfOf() {
    long amount = 1234;
    byte[] rpc = AccountPluginRpc.setStakingGoalOnBehalfOf(ADDRESS_B, amount);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.SET_STAKING_GOAL);
    assertThat(stream.readOptional(BlockchainAddress::read)).isEqualTo(ADDRESS_B);
    assertThat(stream.readLong()).isEqualTo(amount);
  }

  @Test
  void associateIceContract() {
    final BlockchainAddress address = ADDRESS_A;
    Hash transactionId = Hash.create(s -> s.writeString("id"));
    int amount = 100;
    byte[] rpc = AccountPluginRpc.initiateIceAssociateContract(transactionId, amount, address);
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte())
        .isEqualTo(AccountPluginRpc.INITIATE_ICE_ASSOCIATE_CONTRACT);
    assertThat(Hash.read(stream)).isEqualTo(transactionId);
    assertThat(stream.readLong()).isEqualTo(amount);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(address);
  }

  @Test
  void associateIce() {
    final BlockchainAddress address = ADDRESS_A;
    Hash transactionId = Hash.create(s -> s.writeString("id"));
    int amount = 100;
    byte[] rpc =
        AccountPluginRpc.initiateIceAssociateOnBehalfOf(ADDRESS_B, transactionId, amount, address);
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.INITIATE_ICE_ASSOCIATE);
    assertThat(stream.readOptional(BlockchainAddress::read)).isEqualTo(ADDRESS_B);
    assertThat(Hash.read(stream)).isEqualTo(transactionId);
    assertThat(stream.readLong()).isEqualTo(amount);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(address);
  }

  @Test
  void disassociateIce() {
    final BlockchainAddress address = ADDRESS_A;
    Hash transactionId = Hash.create(s -> s.writeString("id"));
    int amount = 100;
    byte[] rpc = AccountPluginRpc.initiateIceDisassociateContract(transactionId, amount, address);
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte())
        .isEqualTo(AccountPluginRpc.INITIATE_ICE_DISASSOCIATE_CONTRACT);
    assertThat(Hash.read(stream)).isEqualTo(transactionId);
    assertThat(stream.readLong()).isEqualTo(amount);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(address);
  }

  @Test
  void disassociateIceContract() {
    final BlockchainAddress address = ADDRESS_A;
    Hash transactionId = Hash.create(s -> s.writeString("id"));
    int amount = 100;
    byte[] rpc = AccountPluginRpc.initiateIceDisassociate(transactionId, amount, address);
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.INITIATE_ICE_DISASSOCIATE);
    assertThat(stream.readOptional(BlockchainAddress::read)).isEqualTo(null);
    assertThat(Hash.read(stream)).isEqualTo(transactionId);
    assertThat(stream.readLong()).isEqualTo(amount);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(address);
  }

  @Test
  void abortIce() {
    Hash transactionId = Hash.create(s -> s.writeString("id"));
    byte[] rpc = AccountPluginRpc.abortIceAssociate(transactionId);
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.ABORT_ICE);
    assertThat(Hash.read(stream)).isEqualTo(transactionId);
  }

  @Test
  void commitIce() {
    Hash transactionId = Hash.create(s -> s.writeString("id"));
    byte[] rpc = AccountPluginRpc.commitIceAssociate(transactionId);
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.COMMIT_ICE);
    assertThat(Hash.read(stream)).isEqualTo(transactionId);
  }

  @Test
  void unstakeTokens() {
    long amount = 1234;
    byte[] rpc = AccountPluginRpc.unstakeTokens(amount);
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.UNSTAKE_TOKENS);
    assertThat(stream.readOptional(BlockchainAddress::read)).isEqualTo(null);
    assertThat(stream.readLong()).isEqualTo(amount);
  }

  @Test
  void checkPendingUnstakes() {
    byte[] rpc = AccountPluginRpc.checkPendingUnstakesOnBehalfOf(ADDRESS_B);
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.CHECK_PENDING_UNSTAKES);
    assertThat(stream.readOptional(BlockchainAddress::read)).isEqualTo(ADDRESS_B);
  }

  @Test
  void checkVestedTokens() {
    byte[] rpc = AccountPluginRpc.checkVestedTokens();
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.CHECK_VESTED);
    assertThat(stream.readOptional(BlockchainAddress::read)).isEqualTo(null);
  }

  @Test
  void disassociateAllTokensForContract() {
    BlockchainAddress contractAddress =
        BlockchainAddress.fromString("000040000000000000000000000000000000000001");

    byte[] rpc = AccountPluginRpc.disassociateAllTokensForContract(contractAddress);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte())
        .isEqualTo(AccountPluginRpc.DISASSOCIATE_ALL_TOKENS_FOR_CONTRACT_BYTE);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(contractAddress);
  }

  @Test
  void initiateWithdraw() {
    Hash transactionHash = Hash.create(h -> h.writeInt(123));
    long amount = 1234;
    byte[] rpc = AccountPluginRpc.initiateWithdrawOnBehalfOf(ADDRESS_B, transactionHash, amount);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.INIT_WITHDRAW);
    assertThat(stream.readOptional(BlockchainAddress::read)).isEqualTo(ADDRESS_B);
    assertThat(Hash.read(stream)).isEqualTo(transactionHash);
    assertThat(stream.readLong()).isEqualTo(amount);
  }

  @Test
  void initiateDeposit() {
    Hash transactionHash = Hash.create(h -> h.writeInt(123));
    long amount = 1234;
    byte[] rpc = AccountPluginRpc.initiateDeposit(transactionHash, amount);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.INIT_DEPOSIT);
    assertThat(Hash.read(stream)).isEqualTo(transactionHash);
    assertThat(stream.readLong()).isEqualTo(amount);
  }

  @Test
  void acceptDelegatedStakes() {
    BlockchainAddress sender = ADDRESS_A;
    long amount = 1234;
    byte[] rpc = AccountPluginRpc.acceptDelegatedStakesOnBehalfOf(ADDRESS_B, sender, amount);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.ACCEPT_DELEGATED_STAKES);
    assertThat(stream.readOptional(BlockchainAddress::read)).isEqualTo(ADDRESS_B);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(sender);
    assertThat(stream.readLong()).isEqualTo(amount);
  }

  @Test
  void reduceDelegatedStakes() {
    BlockchainAddress sender = ADDRESS_A;
    long amount = 1234;
    byte[] rpc = AccountPluginRpc.reduceDelegatedStakes(sender, amount);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.REDUCE_DELEGATED_STAKES);
    assertThat(stream.readOptional(BlockchainAddress::read)).isEqualTo(null);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(sender);
    assertThat(stream.readLong()).isEqualTo(amount);
  }

  @Test
  void initiateDelegateStakes() {
    Hash transactionHash = Hash.create(h -> h.writeInt(123));
    long amount = 1234;
    BlockchainAddress counterPart = ADDRESS_A;
    byte[] rpc =
        AccountPluginRpc.initiateDelegateStakesOnBehalfOf(
            ADDRESS_B, transactionHash, amount, counterPart);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.INIT_DELEGATE_STAKES);
    assertThat(stream.readOptional(BlockchainAddress::read)).isEqualTo(ADDRESS_B);
    assertThat(Hash.read(stream)).isEqualTo(transactionHash);
    assertThat(stream.readLong()).isEqualTo(amount);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(counterPart);
  }

  @Test
  void initiateReceiveDelegatedStakes() {
    Hash transactionHash = Hash.create(h -> h.writeInt(123));
    long amount = 1234;
    BlockchainAddress counterPart = ADDRESS_A;
    byte[] rpc =
        AccountPluginRpc.initiateReceiveDelegatedStakes(transactionHash, amount, counterPart);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.INIT_RECEIVE_DELEGATED_STAKES);
    assertThat(Hash.read(stream)).isEqualTo(transactionHash);
    assertThat(stream.readLong()).isEqualTo(amount);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(counterPart);
  }

  @Test
  @DisplayName(
      "Receiving delegated stakes with expiration timestamp, reads the expected inputted bytes.")
  void initiateReceiveDelegatedStakesWithExpirationTimestamp() {
    Hash transactionHash = Hash.create(h -> h.writeInt(123));
    long amount = 1234;
    long expirationTimestamp = 100L;
    BlockchainAddress counterPart = ADDRESS_A;
    byte[] rpc =
        AccountPluginRpc.initiateReceiveDelegatedStakesWithExpiration(
            transactionHash, amount, counterPart, expirationTimestamp);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte())
        .isEqualTo(AccountPluginRpc.INITIATE_RECEIVE_DELEGATED_STAKES_WITH_EXPIRATION);
    assertThat(Hash.read(stream)).isEqualTo(transactionHash);
    assertThat(stream.readLong()).isEqualTo(amount);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(counterPart);
    assertThat(stream.readLong()).isEqualTo(expirationTimestamp);
  }

  @Test
  void initiateRetractDelegatedStakes() {
    Hash transactionHash = Hash.create(h -> h.writeInt(123));
    long amount = 1234;
    BlockchainAddress counterPart = ADDRESS_A;
    byte[] rpc =
        AccountPluginRpc.initiateRetractDelegatedStakesOnBehalfOf(
            ADDRESS_B, transactionHash, amount, counterPart);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.INIT_RETRACT_DELEGATED_STAKES);
    assertThat(stream.readOptional(BlockchainAddress::read)).isEqualTo(ADDRESS_B);
    assertThat(Hash.read(stream)).isEqualTo(transactionHash);
    assertThat(stream.readLong()).isEqualTo(amount);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(counterPart);
  }

  @Test
  void initiateReturnDelegatedStakes() {
    Hash transactionHash = Hash.create(h -> h.writeInt(123));
    long amount = 1234;
    BlockchainAddress counterPart = ADDRESS_A;
    byte[] rpc =
        AccountPluginRpc.initiateReturnDelegatedStakes(transactionHash, amount, counterPart);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.INIT_RETURN_DELEGATED_STAKES);
    assertThat(Hash.read(stream)).isEqualTo(transactionHash);
    assertThat(stream.readLong()).isEqualTo(amount);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(counterPart);
  }

  @Test
  void initiateByocTransferSender() {
    Hash transactionHash = Hash.create(h -> h.writeInt(123));
    Unsigned256 amount = Unsigned256.create(1234);
    String symbol = "ETH";
    byte[] rpc =
        AccountPluginRpc.initByocTransferSenderOnBehalfOf(
            ADDRESS_B, transactionHash, amount, symbol);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.INIT_BYOC_TRANSFER_SENDER);
    assertThat(stream.readOptional(BlockchainAddress::read)).isEqualTo(ADDRESS_B);
    assertThat(Hash.read(stream)).isEqualTo(transactionHash);
    assertThat(Unsigned256.read(stream)).isEqualTo(amount);
    assertThat(stream.readString()).isEqualTo(symbol);
  }

  @Test
  void initiateByocTransferRecipient() {
    Hash transactionHash = Hash.create(h -> h.writeInt(123));
    Unsigned256 amount = Unsigned256.create(1234);
    String symbol = "ETH";
    byte[] rpc = AccountPluginRpc.initiateByocTransferRecipient(transactionHash, amount, symbol);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.INIT_BYOC_TRANSFER_RECIPIENT);
    assertThat(Hash.read(stream)).isEqualTo(transactionHash);
    assertThat(Unsigned256.read(stream)).isEqualTo(amount);
    assertThat(stream.readString()).isEqualTo(symbol);
  }

  @Test
  void abortTransfer() {
    Hash transactionHash = Hash.create(h -> h.writeInt(123));
    byte[] rpc = AccountPluginRpc.abortTransfer(transactionHash);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.ABORT_TRANSFER);
    assertThat(Hash.read(stream)).isEqualTo(transactionHash);
  }

  @Test
  void abortDelegatedStakes() {
    Hash transactionHash = Hash.create(h -> h.writeInt(123));
    byte[] rpc = AccountPluginRpc.abortDelegatedStakes(transactionHash);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.ABORT_DELEGATE_STAKES);
    assertThat(Hash.read(stream)).isEqualTo(transactionHash);
  }

  @Test
  void commitTransfer() {
    Hash transactionHash = Hash.create(h -> h.writeInt(123));
    byte[] rpc = AccountPluginRpc.commitTransfer(transactionHash);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.COMMIT_TRANSFER);
    assertThat(Hash.read(stream)).isEqualTo(transactionHash);
  }

  @Test
  void commitDelegatedStakes() {
    Hash transactionHash = Hash.create(h -> h.writeInt(123));
    byte[] rpc = AccountPluginRpc.commitDelegateStakes(transactionHash);

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.COMMIT_DELEGATE_STAKES);
    assertThat(Hash.read(stream)).isEqualTo(transactionHash);
  }

  @Test
  void appointCustodian() {
    byte[] rpc = AccountPluginRpc.appointCustodianOnBehalfOf(ADDRESS_A, ADDRESS_B);
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.APPOINT_CUSTODIAN);
    assertThat(stream.readOptional(BlockchainAddress::read)).isEqualTo(ADDRESS_A);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(ADDRESS_B);
  }

  @Test
  void acceptCustodianship() {
    byte[] rpc = AccountPluginRpc.acceptCustodianship(ADDRESS_A);
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.ACCEPT_CUSTODIANSHIP);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(ADDRESS_A);
  }

  @Test
  void cancelAppointedCustodian() {
    byte[] rpc = AccountPluginRpc.cancelAppointedCustodianOnBehalfOf(ADDRESS_A);
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.CANCEL_APPOINTED_CUSTODIAN);
    assertThat(stream.readOptional(BlockchainAddress::read)).isEqualTo(ADDRESS_A);
  }

  @Test
  void resetCustodian() {
    byte[] rpc = AccountPluginRpc.resetCustodianOnBehalfOf(ADDRESS_A);
    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    assertThat(stream.readUnsignedByte()).isEqualTo(AccountPluginRpc.RESET_CUSTODIAN);
    assertThat(BlockchainAddress.read(stream)).isEqualTo(ADDRESS_A);
  }
}
