package com.partisiablockchain.governance.mpctoken;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.google.errorprone.annotations.Immutable;
import com.partisiablockchain.BlockchainAddress;
import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.StateSerializableEquality;
import com.partisiablockchain.contract.SysContractContextTest;
import com.partisiablockchain.contract.SysContractSerialization;
import com.partisiablockchain.contract.sys.LocalPluginStateUpdate;
import com.partisiablockchain.crypto.Hash;
import com.partisiablockchain.math.Unsigned256;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateSerializable;
import com.partisiablockchain.tree.AvlTree;
import com.secata.stream.SafeDataInputStream;
import com.secata.stream.SafeDataOutputStream;
import com.secata.tools.coverage.FunctionUtility;
import com.secata.tools.immutable.FixedList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

/** Tests for {@link MpcTokenContract}. */
final class MpcTokenContractTest {

  private final BlockchainAddress accountA =
      BlockchainAddress.fromString("000000000000000000000000000000000000000001");
  private final BlockchainAddress accountB =
      BlockchainAddress.fromString("000000000000000000000000000000000000000002");
  private final BlockchainAddress accountC =
      BlockchainAddress.fromString("000000000000000000000000000000000000000003");

  private final BlockchainAddress publicContract =
      BlockchainAddress.fromString("020000000000000000000000000000000000000002");
  private static final Hash hashA = Hash.create(s -> s.writeLong(1));
  private static final Hash hashB = Hash.create(s -> s.writeLong(2));
  private final SysContractSerialization<MpcTokenContractState> serialization =
      new SysContractSerialization<>(MpcTokenContractInvoker.class, MpcTokenContractState.class);
  private SysContractContextTest context;
  private final MpcTokenContractState initial = MpcTokenContractState.initial(false);

  private static final String SYMBOL = "ETH";

  /** Sets up the test. */
  @BeforeEach
  void setUp() {
    context = new SysContractContextTest(1, 100, accountA);
  }

  @Test
  void onCreate() {
    MpcTokenContractState state = serialization.create(context, s -> s.writeBoolean(false));
    Assertions.assertThat(state).isNotNull();
  }

  @Test
  void onUpgrade() {
    MpcTokenContractState state =
        MpcTokenContractState.initial(false)
            .addTransfer(
                hashA,
                TransferInformation.create(
                    accountA, accountB, Unsigned256.create(1_000L), SYMBOL, null))
            .addTransfer(
                Hash.create(s -> s.writeInt(123)),
                TransferInformation.create(
                    accountA, accountB, Unsigned256.create(5_000L), SYMBOL, null))
            .addStakeDelegation(
                hashA,
                StakeDelegation.create(
                    accountA,
                    accountB,
                    550,
                    StakeDelegation.DelegationType.DELEGATE_STAKES,
                    accountA,
                    null))
            .addStakeDelegation(
                Hash.create(s -> s.writeInt(321)),
                StakeDelegation.create(
                    accountB,
                    accountA,
                    7640,
                    StakeDelegation.DelegationType.DELEGATE_STAKES,
                    accountC,
                    100L))
            .addStakeDelegation(
                hashA,
                StakeDelegation.create(
                    accountA,
                    accountB,
                    550,
                    StakeDelegation.DelegationType.RETRACT_DELEGATED_STAKES,
                    accountC,
                    null))
            .addIcedTokens(
                Hash.create(s -> s.writeInt(123)),
                new IceStaking(accountA, publicContract, 100, accountA))
            .addIcedTokens(
                Hash.create(s -> s.writeInt(321)),
                new IceStaking(
                    accountB,
                    BlockchainAddress.fromString("020000000000000000000000000000000000000003"),
                    200,
                    accountB));

    MpcTokenContractState afterUpgrade =
        new MpcTokenContractInvoker()
            .onUpgrade(
                StateAccessor.create(state), SafeDataInputStream.createFromBytes(new byte[0]));
    StateSerializableEquality.assertStatesEqual(afterUpgrade, state);
  }

  @Test
  void stakeTokens() {
    final long amount = 10;
    final BlockchainAddress sender = accountA;

    from(sender);
    assertUnmodifiedState(
        initial,
        state ->
            serialization.invoke(
                context,
                state,
                rpc -> {
                  rpc.writeByte(MpcTokenContract.Invocations.STAKE_TOKENS);
                  rpc.writeLong(amount);
                }));

    LocalPluginStateUpdate stakeTokensUpdate = context.getUpdateLocalAccountPluginStates().get(0);
    assertAccountPluginUpdate(stakeTokensUpdate, sender, AccountPluginRpc.stakeTokens(amount));
  }

  /** Setting a positive staking goal for an account, sends rpc with inputted goal and account. */
  @Test
  void setStakingGoal() {
    final long amount = 10;
    final BlockchainAddress sender = accountA;

    from(sender);
    assertUnmodifiedState(
        initial,
        state ->
            serialization.invoke(
                context,
                state,
                rpc -> {
                  rpc.writeByte(MpcTokenContract.Invocations.SET_STAKING_GOAL);
                  rpc.writeLong(amount);
                }));

    LocalPluginStateUpdate stakingGoalUpdate = context.getUpdateLocalAccountPluginStates().get(0);
    assertAccountPluginUpdate(stakingGoalUpdate, sender, AccountPluginRpc.setStakingGoal(amount));
  }

  /** Setting a staking goal of zero for an account, sends rpc with inputted goal and account. */
  @Test
  void setZeroStakingGoal() {
    final long amount = 0;
    final BlockchainAddress sender = accountA;

    from(sender);
    assertUnmodifiedState(
        initial,
        state ->
            serialization.invoke(
                context,
                state,
                rpc -> {
                  rpc.writeByte(MpcTokenContract.Invocations.SET_STAKING_GOAL);
                  rpc.writeLong(amount);
                }));

    LocalPluginStateUpdate stakingGoalUpdate = context.getUpdateLocalAccountPluginStates().get(0);
    assertAccountPluginUpdate(stakingGoalUpdate, sender, AccountPluginRpc.setStakingGoal(amount));
  }

  @ParameterizedTest
  @ValueSource(longs = {-1, 0})
  void stakeNonPositiveAmountOfTokensFails(long nonPositiveAmount) {
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    initial,
                    rpc -> {
                      rpc.writeByte(MpcTokenContract.Invocations.STAKE_TOKENS);
                      rpc.writeLong(nonPositiveAmount);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Amount must be positive");
  }

  /** Setting a negative staking goal for an account, throws exception. */
  @Test
  void setNegativeStakingGoalFails() {
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    initial,
                    rpc -> {
                      rpc.writeByte(MpcTokenContract.Invocations.SET_STAKING_GOAL);
                      rpc.writeLong(-1);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Staking goal cannot be negative");
  }

  @Test
  void stakeTokensOnBehalfOf() {
    final BlockchainAddress custodian = accountA;
    final BlockchainAddress target = accountB;
    final long amount = 10;

    from(custodian);
    assertUnmodifiedState(
        initial,
        state ->
            serialization.invoke(
                context,
                state,
                rpc -> {
                  rpc.writeByte(MpcTokenContract.Invocations.STAKE_TOKENS_ON_BEHALF_OF);
                  target.write(rpc);
                  rpc.writeLong(amount);
                }));
    LocalPluginStateUpdate stakeTokensUpdate = context.getUpdateLocalAccountPluginStates().get(0);
    assertAccountPluginUpdate(
        stakeTokensUpdate, target, AccountPluginRpc.stakeTokensOnBehalfOf(custodian, amount));
  }

  /**
   * Setting a positive staking goal on behalf of another account as its custodian, sends rpc with
   * inputted goal and account.
   */
  @Test
  void setStakingGoalOnBehalfOf() {
    final BlockchainAddress custodian = accountA;
    final BlockchainAddress target = accountB;
    final long amount = 10;

    from(custodian);
    assertUnmodifiedState(
        initial,
        state ->
            serialization.invoke(
                context,
                state,
                rpc -> {
                  rpc.writeByte(MpcTokenContract.Invocations.SET_STAKING_GOAL_ON_BEHALF_OF);
                  target.write(rpc);
                  rpc.writeLong(amount);
                }));
    LocalPluginStateUpdate stakingGoalUpdate = context.getUpdateLocalAccountPluginStates().get(0);
    assertAccountPluginUpdate(
        stakingGoalUpdate, target, AccountPluginRpc.setStakingGoalOnBehalfOf(custodian, amount));
  }

  @Nested
  @DisplayName("Setting the expiration timestamp for a delegation.")
  final class SetExpirationTimestamp {
    @Test
    @DisplayName("To a positive value, sends rpc with inputted account and expiration timestamp.")
    void setExpirationTimestamp() {
      final BlockchainAddress sender = accountA;
      final BlockchainAddress target = accountC;
      final long expirationTimestamp = 10L;

      from(sender);
      assertUnmodifiedState(
          initial,
          state ->
              serialization.invoke(
                  context,
                  state,
                  rpc -> {
                    rpc.writeByte(MpcTokenContract.Invocations.SET_EXPIRATION_FOR_DELEGATED_STAKES);
                    target.write(rpc);
                    rpc.writeOptional(
                        (value, stream) -> stream.writeLong(value), expirationTimestamp);
                  }));
      LocalPluginStateUpdate expirationTimestampUpdate =
          context.getUpdateLocalAccountPluginStates().get(0);
      assertAccountPluginUpdate(
          expirationTimestampUpdate,
          target,
          AccountPluginRpc.setExpirationTimestamp(sender, expirationTimestamp));
    }

    @Test
    @DisplayName("To never expire, sends rpc with inputted account and expiration timestamp.")
    void setExpirationTimestampToNeverExpire() {
      final BlockchainAddress sender = accountA;
      final BlockchainAddress target = accountC;
      final Long noExpiration = null;

      from(sender);
      assertUnmodifiedState(
          initial,
          state ->
              serialization.invoke(
                  context,
                  state,
                  rpc -> {
                    rpc.writeByte(MpcTokenContract.Invocations.SET_EXPIRATION_FOR_DELEGATED_STAKES);
                    target.write(rpc);
                    rpc.writeOptional((value, stream) -> stream.writeLong(value), noExpiration);
                  }));
      LocalPluginStateUpdate expirationTimestampUpdate =
          context.getUpdateLocalAccountPluginStates().get(0);
      assertAccountPluginUpdate(
          expirationTimestampUpdate,
          target,
          AccountPluginRpc.setExpirationTimestamp(sender, noExpiration));
    }

    @Test
    @DisplayName("To zero, sends rpc with inputted goal and account.")
    void setZeroExpirationTimestamp() {
      final BlockchainAddress sender = accountA;
      final BlockchainAddress target = accountC;
      final long expirationTimestamp = 0L;

      from(sender);
      assertUnmodifiedState(
          initial,
          state ->
              serialization.invoke(
                  context,
                  state,
                  rpc -> {
                    rpc.writeByte(MpcTokenContract.Invocations.SET_EXPIRATION_FOR_DELEGATED_STAKES);
                    target.write(rpc);
                    rpc.writeOptional(
                        (value, stream) -> stream.writeLong(value), expirationTimestamp);
                  }));
      LocalPluginStateUpdate expirationTimestampUpdate =
          context.getUpdateLocalAccountPluginStates().get(0);
      assertAccountPluginUpdate(
          expirationTimestampUpdate,
          target,
          AccountPluginRpc.setExpirationTimestamp(sender, expirationTimestamp));
    }

    @Test
    @DisplayName(
        "To a positive value on behalf of another account as its"
            + " custodian, sends rpc with inputted goal and account.")
    void setExpirationTimestampOnBehalfOf() {
      final BlockchainAddress custodian = accountA;
      final BlockchainAddress target = accountB;
      final BlockchainAddress delegator = accountC;
      final long expirationTimestamp = 10L;

      from(custodian);
      assertUnmodifiedState(
          initial,
          state ->
              serialization.invoke(
                  context,
                  state,
                  rpc -> {
                    rpc.writeByte(
                        MpcTokenContract.Invocations
                            .SET_EXPIRATION_TIMESTAMP_FOR_DELEGATED_STAKES_ON_BEHALF_OF);
                    delegator.write(rpc);
                    target.write(rpc);
                    rpc.writeOptional(
                        (value, stream) -> stream.writeLong(value), expirationTimestamp);
                  }));
      LocalPluginStateUpdate expirationTimestampUpdate =
          context.getUpdateLocalAccountPluginStates().get(0);
      assertAccountPluginUpdate(
          expirationTimestampUpdate,
          target,
          AccountPluginRpc.setExpirationTimestampOnBehalfOf(
              custodian, delegator, expirationTimestamp));
    }

    @Test
    @DisplayName(
        "To never expire on behalf of another account as its custodian, sends rpc with inputted"
            + " goal and account.")
    void setExpirationTimestampToNeverExpireOnBehalfOf() {
      final BlockchainAddress custodian = accountA;
      final BlockchainAddress target = accountB;
      final BlockchainAddress delegator = accountC;
      final Long noExpiration = null;

      from(custodian);
      assertUnmodifiedState(
          initial,
          state ->
              serialization.invoke(
                  context,
                  state,
                  rpc -> {
                    rpc.writeByte(
                        MpcTokenContract.Invocations
                            .SET_EXPIRATION_TIMESTAMP_FOR_DELEGATED_STAKES_ON_BEHALF_OF);
                    delegator.write(rpc);
                    target.write(rpc);
                    rpc.writeOptional((value, stream) -> stream.writeLong(value), noExpiration);
                  }));
      LocalPluginStateUpdate expirationTimestampUpdate =
          context.getUpdateLocalAccountPluginStates().get(0);
      assertAccountPluginUpdate(
          expirationTimestampUpdate,
          target,
          AccountPluginRpc.setExpirationTimestampOnBehalfOf(custodian, delegator, noExpiration));
    }

    @Test
    @DisplayName(
        "To zero for another account as its custodian, sends rpc with inputted goal and account.")
    void setZeroExpirationTimestampOnBehalfOf() {
      final BlockchainAddress custodian = accountA;
      final BlockchainAddress target = accountB;
      final BlockchainAddress delegator = accountC;
      final long zeroExpirationTimestamp = 0L;

      from(custodian);
      assertUnmodifiedState(
          initial,
          state ->
              serialization.invoke(
                  context,
                  state,
                  rpc -> {
                    rpc.writeByte(
                        MpcTokenContract.Invocations
                            .SET_EXPIRATION_TIMESTAMP_FOR_DELEGATED_STAKES_ON_BEHALF_OF);
                    delegator.write(rpc);
                    target.write(rpc);
                    rpc.writeOptional(
                        (value, stream) -> stream.writeLong(value), zeroExpirationTimestamp);
                  }));
      LocalPluginStateUpdate expirationTimestampUpdate =
          context.getUpdateLocalAccountPluginStates().get(0);
      assertAccountPluginUpdate(
          expirationTimestampUpdate,
          target,
          AccountPluginRpc.setExpirationTimestampOnBehalfOf(
              custodian, delegator, zeroExpirationTimestamp));
    }

    @Test
    @DisplayName("To a negative value, throws exception.")
    void setNegativeExpirationTimestampFails() {
      Assertions.assertThatThrownBy(
              () ->
                  serialization.invoke(
                      context,
                      initial,
                      rpc -> {
                        rpc.writeByte(
                            MpcTokenContract.Invocations.SET_EXPIRATION_FOR_DELEGATED_STAKES);
                        accountC.write(rpc);
                        rpc.writeOptional((value, stream) -> stream.writeLong(value), -1L);
                      }))
          .isInstanceOf(RuntimeException.class)
          .hasMessage("Expiration timestamp cannot be negative");
    }

    @Test
    @DisplayName("To a negative value for another account as its custodian, throws exception.")
    void setNegativeExpirationTimestampOnBehalfOfFails() {
      Assertions.assertThatThrownBy(
              () ->
                  serialization.invoke(
                      context,
                      initial,
                      rpc -> {
                        rpc.writeByte(
                            MpcTokenContract.Invocations
                                .SET_EXPIRATION_TIMESTAMP_FOR_DELEGATED_STAKES_ON_BEHALF_OF);
                        accountB.write(rpc);
                        accountC.write(rpc);
                        rpc.writeOptional((value, stream) -> stream.writeLong(value), -1L);
                      }))
          .isInstanceOf(RuntimeException.class)
          .hasMessage("Expiration timestamp cannot be negative");
    }
  }

  @DisplayName("Delegating stakes with an expiration timestamp.")
  @Nested
  final class DelegatingWithTimestamp {
    @Test
    @DisplayName(
        "With a positive value, successfully executes the two phase commit for delegating stakes"
            + " with an expiration timestamp.")
    void delegateStakesWithExpirationTwoPhaseSuccessful() {
      long amount = 1000;
      Long expirationTimestamp = 100L;
      MpcTokenContractState updatedState =
          stakeDelegation(
              initial,
              MpcTokenContract.Invocations.DELEGATE_STAKES_WITH_EXPIRATION,
              accountB,
              amount,
              expirationTimestamp);

      Assertions.assertThat(updatedState.getStakeDelegations().size()).isEqualTo(1);

      Assertions.assertThat(
              updatedState
                  .getStakeDelegations()
                  .getValue(context.getCurrentTransactionHash())
                  .initiator)
          .isEqualTo(accountA);
      Assertions.assertThat(
              updatedState
                  .getStakeDelegations()
                  .getValue(context.getCurrentTransactionHash())
                  .expirationTimestamp)
          .isEqualTo(expirationTimestamp);

      byte[] recipientRpcExpected =
          AccountPluginRpc.initiateReceiveDelegatedStakesWithExpiration(
              hashA, amount, accountA, expirationTimestamp);
      byte[] recipientRpcActual = context.getUpdateLocalAccountPluginStates().get(1).getRpc();
      Assertions.assertThat(recipientRpcActual).isEqualTo(recipientRpcExpected);

      byte[] senderRpcExpected = AccountPluginRpc.initiateDelegateStakes(hashA, amount, accountB);
      byte[] senderRpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
      Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);

      byte[] expectedCallbackRpc =
          SafeDataOutputStream.serialize(
              MpcTokenContract.Callbacks.stakeDelegation(hashA, accountA, accountB));
      byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
      Assertions.assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);

      CallbackContext callbackContext = createTwoPhaseCallbackContext(true, true);

      MpcTokenContractState stateAfterCallback =
          serialization.callback(
              context,
              callbackContext,
              updatedState,
              rpc -> {
                rpc.writeByte(MpcTokenContract.Callbacks.STAKE_DELEGATION_CALLBACK);
                hashA.write(rpc);
                accountA.write(rpc);
                accountB.write(rpc);
              });

      Assertions.assertThat(stateAfterCallback.getStakeDelegations().size()).isEqualTo(0);

      senderRpcActual = context.getUpdateLocalAccountPluginStates().get(2).getRpc();
      senderRpcExpected = AccountPluginRpc.commitDelegateStakes(hashA);
      recipientRpcActual = context.getUpdateLocalAccountPluginStates().get(3).getRpc();
      recipientRpcExpected = AccountPluginRpc.commitDelegateStakes(hashA);

      Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
      Assertions.assertThat(recipientRpcActual).isEqualTo(recipientRpcExpected);
    }

    @Test
    @DisplayName("With a positive value on behalf of another account creates the expected rpc.")
    void stakeDelegationWithExpirationOnBehalfOf() {
      long amount = 1000;
      long expirationTimestamp = 100;

      MpcTokenContractState updatedState =
          serialization.invoke(
              context,
              initial,
              stream -> {
                stream.writeByte(
                    MpcTokenContract.Invocations.DELEGATE_STAKES_WITH_EXPIRATION_ON_BEHALF_OF);
                accountC.write(stream);
                accountB.write(stream);
                stream.writeLong(amount);
                stream.writeLong(expirationTimestamp);
              });

      Assertions.assertThat(updatedState.getStakeDelegations().size()).isEqualTo(1);

      LocalPluginStateUpdate firstAccountPluginUpdate =
          context.getUpdateLocalAccountPluginStates().get(0);
      byte[] senderRpcExpected =
          AccountPluginRpc.initiateDelegateStakesOnBehalfOf(accountA, hashA, amount, accountB);
      Assertions.assertThat(firstAccountPluginUpdate.getRpc()).isEqualTo(senderRpcExpected);
      Assertions.assertThat(firstAccountPluginUpdate.getContext()).isEqualTo(accountC);

      LocalPluginStateUpdate secondAccountPluginUpdate =
          context.getUpdateLocalAccountPluginStates().get(1);
      Assertions.assertThat(secondAccountPluginUpdate.getContext()).isEqualTo(accountB);
      byte[] recipientRpcExpected =
          AccountPluginRpc.initiateReceiveDelegatedStakesWithExpiration(
              hashA, amount, accountC, expirationTimestamp);
      Assertions.assertThat(secondAccountPluginUpdate.getRpc()).isEqualTo(recipientRpcExpected);

      byte[] expectedCallbackRpc =
          SafeDataOutputStream.serialize(
              MpcTokenContract.Callbacks.stakeDelegation(hashA, accountC, accountB));
      byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
      Assertions.assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
    }

    @Test
    @DisplayName("To yourself, throws exception.")
    void twoPhaseCommitToYourselfStakingWithTimestamp() {
      long amount = 1234;
      Assertions.assertThatThrownBy(
              () ->
                  stakeDelegation(
                      initial,
                      MpcTokenContract.Invocations.DELEGATE_STAKES_WITH_EXPIRATION,
                      accountA,
                      amount,
                      10L))
          .isInstanceOf(IllegalArgumentException.class)
          .hasMessage("Cannot delegate stakes between the same account using this operation");
    }
  }

  @Test
  @DisplayName(
      "Setting a staking goal of zero for another account as its custodian, sends rpc with inputted"
          + " goal and account.")
  void setZeroStakingGoalOnBehalfOf() {
    final BlockchainAddress custodian = accountA;
    final BlockchainAddress target = accountB;
    final long amount = 0;

    from(custodian);
    assertUnmodifiedState(
        initial,
        state ->
            serialization.invoke(
                context,
                state,
                rpc -> {
                  rpc.writeByte(MpcTokenContract.Invocations.SET_STAKING_GOAL_ON_BEHALF_OF);
                  target.write(rpc);
                  rpc.writeLong(amount);
                }));
    LocalPluginStateUpdate stakingGoalUpdate = context.getUpdateLocalAccountPluginStates().get(0);
    assertAccountPluginUpdate(
        stakingGoalUpdate, target, AccountPluginRpc.setStakingGoalOnBehalfOf(custodian, amount));
  }

  @ParameterizedTest
  @ValueSource(longs = {-1, 0})
  void stakeNonPositiveAmountOfTokensOnBehalfOfFails(long nonPositiveAmount) {
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    initial,
                    rpc -> {
                      rpc.writeByte(MpcTokenContract.Invocations.STAKE_TOKENS_ON_BEHALF_OF);
                      accountB.write(rpc);
                      rpc.writeLong(nonPositiveAmount);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Amount must be positive");
  }

  @Test
  @DisplayName(
      "Setting a negative staking goal for another account as its custodian, throws exception.")
  void setNegativeStakingGoalOnBehalfOfFails() {
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    initial,
                    rpc -> {
                      rpc.writeByte(MpcTokenContract.Invocations.SET_STAKING_GOAL_ON_BEHALF_OF);
                      accountB.write(rpc);
                      rpc.writeLong(-1);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Staking goal cannot be negative");
  }

  @Test
  void unstakeTokens() {
    final long amount = 10;
    final BlockchainAddress sender = accountA;

    from(sender);
    assertUnmodifiedState(
        initial,
        state ->
            serialization.invoke(
                context,
                state,
                rpc -> {
                  rpc.writeByte(MpcTokenContract.Invocations.UNSTAKE_TOKENS);
                  rpc.writeLong(amount);
                }));

    LocalPluginStateUpdate unstakeTokensUpdate = context.getUpdateLocalAccountPluginStates().get(0);
    assertAccountPluginUpdate(unstakeTokensUpdate, sender, AccountPluginRpc.unstakeTokens(amount));
  }

  @ParameterizedTest
  @ValueSource(longs = {-1, 0})
  void unstakeNonPositiveAmountOfTokensFails(long nonPositiveAmount) {
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    initial,
                    rpc -> {
                      rpc.writeByte(MpcTokenContract.Invocations.UNSTAKE_TOKENS);
                      accountB.write(rpc);
                      rpc.writeLong(nonPositiveAmount);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Amount must be positive");
  }

  @Test
  void unstakeTokensOnBehalfOf() {
    final BlockchainAddress custodian = accountA;
    final BlockchainAddress target = accountB;
    final long amount = 10;

    from(custodian);
    assertUnmodifiedState(
        initial,
        state ->
            serialization.invoke(
                context,
                state,
                rpc -> {
                  rpc.writeByte(MpcTokenContract.Invocations.UNSTAKE_TOKENS_ON_BEHALF_OF);
                  target.write(rpc);
                  rpc.writeLong(amount);
                }));

    LocalPluginStateUpdate unstakeTokensOnBehalfOfUpdate =
        context.getUpdateLocalAccountPluginStates().get(0);
    assertAccountPluginUpdate(
        unstakeTokensOnBehalfOfUpdate,
        target,
        AccountPluginRpc.unstakeTokensOnBehalfOf(custodian, amount));
  }

  @ParameterizedTest
  @ValueSource(longs = {-1, 0})
  void unstakeNonPositiveAmountOfTokensOnBehalfOfFails(long nonPositiveAmount) {
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    initial,
                    rpc -> {
                      rpc.writeByte(MpcTokenContract.Invocations.UNSTAKE_TOKENS_ON_BEHALF_OF);
                      accountB.write(rpc);
                      rpc.writeLong(nonPositiveAmount);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Amount must be positive");
  }

  @Test
  void disassociateTokensForRemovedContract() {
    final BlockchainAddress sender = accountA;
    final BlockchainAddress contract = publicContract;

    from(sender);
    assertUnmodifiedState(
        initial,
        state ->
            serialization.invoke(
                context,
                state,
                rpc -> {
                  rpc.writeByte(
                      MpcTokenContract.Invocations.DISASSOCIATE_TOKENS_FOR_REMOVED_CONTRACT);
                  contract.write(rpc);
                }));

    Assertions.assertThat(context.getExists()).isEqualTo(contract);

    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            MpcTokenContract.Callbacks.disassociateTokensForRemovedContract(contract, sender));
    Assertions.assertThat(context.getRemoteCalls().callbackRpc).isEqualTo(expectedCallbackRpc);
  }

  @Test
  void disassociateTokensForRemovedContractForOther() {
    final BlockchainAddress sender = accountB;
    final BlockchainAddress other = accountA;
    final BlockchainAddress contract = publicContract;

    from(sender);
    assertUnmodifiedState(
        initial,
        state ->
            serialization.invoke(
                context,
                state,
                rpc -> {
                  rpc.writeByte(
                      MpcTokenContract.Invocations
                          .DISASSOCIATE_TOKENS_FOR_REMOVED_CONTRACT_FOR_OTHER);
                  contract.write(rpc);
                  other.write(rpc);
                }));

    Assertions.assertThat(context.getExists()).isEqualTo(contract);

    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            MpcTokenContract.Callbacks.disassociateTokensForRemovedContract(contract, other));
    Assertions.assertThat(context.getRemoteCalls().callbackRpc).isEqualTo(expectedCallbackRpc);
  }

  @Test
  void tokensAreDisassociatedSinceContractDoesNotExist() {
    final BlockchainAddress sender = accountA;
    final BlockchainAddress contract = publicContract;

    assertUnmodifiedState(
        initial,
        state ->
            serialization.callback(
                context,
                createExistsCallbackContext(true, false),
                state,
                MpcTokenContract.Callbacks.disassociateTokensForRemovedContract(contract, sender)));

    LocalPluginStateUpdate disassociateTokensForRemovedContractUpdate =
        context.getUpdateLocalAccountPluginStates().get(0);
    assertAccountPluginUpdate(
        disassociateTokensForRemovedContractUpdate,
        sender,
        AccountPluginRpc.disassociateAllTokensForContract(contract));
  }

  @Test
  void tokensAreNotDisassociatedSinceContractExists() {
    assertUnmodifiedState(
        initial,
        state ->
            serialization.callback(
                context,
                createExistsCallbackContext(true, true),
                state,
                MpcTokenContract.Callbacks.disassociateTokensForRemovedContract(
                    publicContract, accountA)));

    Assertions.assertThat(context.getUpdateLocalAccountPluginStates()).hasSize(0);
  }

  @Test
  void tokensAreNotDisassociatedSinceCallbackFails() {
    assertUnmodifiedState(
        initial,
        state ->
            serialization.callback(
                context,
                createExistsCallbackContext(false, false),
                state,
                MpcTokenContract.Callbacks.disassociateTokensForRemovedContract(
                    publicContract, accountA)));

    Assertions.assertThat(context.getUpdateLocalAccountPluginStates()).hasSize(0);
  }

  @Test
  void transferByocOld() {
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            stream -> {
              stream.writeByte(MpcTokenContract.Invocations.BYOC_TRANSFER_OLD);
              accountB.write(stream);
              stream.writeLong(1L);
              stream.writeString(SYMBOL);
            });
    Assertions.assertThat(updatedState.getTransfers().size()).isEqualTo(1);
    Assertions.assertThat(updatedState.getTransfers().values().get(0).symbol).isEqualTo(SYMBOL);
  }

  @Test
  void transferByoc() {
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            stream -> {
              stream.writeByte(MpcTokenContract.Invocations.BYOC_TRANSFER);
              accountB.write(stream);
              Unsigned256.create(123).write(stream);
              stream.writeString(SYMBOL);
            });
    Assertions.assertThat(updatedState.getTransfers().size()).isEqualTo(1);
    Assertions.assertThat(updatedState.getTransfers().values().get(0).symbol).isEqualTo(SYMBOL);
  }

  @Test
  void transferSmallMemo() {
    Assertions.assertThatThrownBy(
            () ->
                transfer(
                    initial,
                    MpcTokenContract.Invocations.TRANSFER_SMALL_MEMO,
                    accountB,
                    0L,
                    s -> s.writeLong(1234)))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Amount must be positive");

    MpcTokenContractState updatedState =
        transfer(
            initial,
            MpcTokenContract.Invocations.TRANSFER_SMALL_MEMO,
            accountB,
            1L,
            s -> s.write(new byte[8]));
    Assertions.assertThat(updatedState.getTransfers().size()).isEqualTo(1);
  }

  @Test
  void transferLargeMemo() {
    Assertions.assertThatThrownBy(
            () ->
                transfer(
                    initial,
                    MpcTokenContract.Invocations.TRANSFER_LARGE_MEMO,
                    accountB,
                    0L,
                    s -> s.write(new byte[256])))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Amount must be positive");

    MpcTokenContractState updatedState =
        transfer(
            initial,
            MpcTokenContract.Invocations.TRANSFER_LARGE_MEMO,
            accountB,
            1L,
            s -> s.writeString("TestString"));
    Assertions.assertThat(updatedState.getTransfers().size()).isEqualTo(1);
  }

  @Test
  void invokeTransferNonPositiveGivesError() {
    Assertions.assertThatThrownBy(() -> transfer(initial, accountB, 0L))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Amount must be positive");
  }

  @Test
  void invokeDelegateStakesNonPositiveGivesError() {
    Assertions.assertThatThrownBy(
            () ->
                stakeDelegation(
                    initial, MpcTokenContract.Invocations.DELEGATE_STAKES, accountB, 0L))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Amount must be positive");
  }

  @Test
  void invokeRetractDelegatedStakesNonPositiveGivesError() {
    Assertions.assertThatThrownBy(
            () ->
                stakeDelegation(
                    initial, MpcTokenContract.Invocations.RETRACT_DELEGATED_STAKES, accountB, 0L))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Amount must be positive");

    Assertions.assertThatThrownBy(
            () ->
                stakeDelegation(
                    initial, MpcTokenContract.Invocations.RETRACT_DELEGATED_STAKES, accountB, -3L))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Amount must be positive");
  }

  private MpcTokenContractState transfer(
      MpcTokenContractState initialState, BlockchainAddress secondAddress, long amount) {
    return transfer(
        initialState,
        MpcTokenContract.Invocations.TRANSFER,
        secondAddress,
        amount,
        FunctionUtility.noOpConsumer());
  }

  private MpcTokenContractState transfer(
      MpcTokenContractState initialState,
      int transfer,
      BlockchainAddress secondAddress,
      long amount,
      Consumer<SafeDataOutputStream> additionalPayload) {

    return serialization.invoke(
        context,
        initialState,
        stream -> {
          stream.writeByte(transfer);
          secondAddress.write(stream);
          stream.writeLong(amount);
          additionalPayload.accept(stream);
        });
  }

  private MpcTokenContractState stakeDelegation(
      MpcTokenContractState initialState,
      int invocation,
      BlockchainAddress secondAddress,
      long amount) {

    return serialization.invoke(
        context,
        initialState,
        stream -> {
          stream.writeByte(invocation);
          secondAddress.write(stream);
          stream.writeLong(amount);
        });
  }

  private MpcTokenContractState stakeDelegation(
      MpcTokenContractState initialState,
      int invocation,
      BlockchainAddress secondAddress,
      long amount,
      long expirationTimestamp) {

    return serialization.invoke(
        context,
        initialState,
        stream -> {
          stream.writeByte(invocation);
          secondAddress.write(stream);
          stream.writeLong(amount);
          stream.writeLong(expirationTimestamp);
        });
  }

  private MpcTokenContractState iceInvocation(
      MpcTokenContractState initialState,
      int invocation,
      BlockchainAddress contractAddress,
      long amount) {

    return serialization.invoke(
        context,
        initialState,
        stream -> {
          stream.writeByte(invocation);
          contractAddress.write(stream);
          stream.writeLong(amount);
        });
  }

  @Test
  void invokeTransferLocked() {
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    MpcTokenContractState.initial(true),
                    stream -> {
                      stream.writeByte(MpcTokenContract.Invocations.TRANSFER);
                      accountB.write(stream);
                      stream.writeLong(10L);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Transfers are locked");

    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    MpcTokenContractState.initial(true),
                    stream -> {
                      stream.writeByte(MpcTokenContract.Invocations.TRANSFER_ON_BEHALF_OF);
                      accountA.write(stream);
                      accountB.write(stream);
                      stream.writeLong(10L);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Transfers are locked");

    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    MpcTokenContractState.initial(true),
                    stream -> {
                      stream.writeByte(MpcTokenContract.Invocations.TRANSFER_SMALL_MEMO);
                      accountB.write(stream);
                      stream.writeLong(10L);
                      stream.writeLong(112L);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Transfers are locked");

    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    MpcTokenContractState.initial(true),
                    stream -> {
                      stream.writeByte(
                          MpcTokenContract.Invocations.TRANSFER_SMALL_MEMO_ON_BEHALF_OF);
                      accountA.write(stream);
                      accountB.write(stream);
                      stream.writeLong(10L);
                      stream.writeLong(112L);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Transfers are locked");

    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    MpcTokenContractState.initial(true),
                    stream -> {
                      stream.writeByte(MpcTokenContract.Invocations.TRANSFER_LARGE_MEMO);
                      accountB.write(stream);
                      stream.writeLong(10L);
                      stream.writeString("Memo");
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Transfers are locked");

    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    MpcTokenContractState.initial(true),
                    stream -> {
                      stream.writeByte(
                          MpcTokenContract.Invocations.TRANSFER_LARGE_MEMO_ON_BEHALF_OF);
                      accountA.write(stream);
                      accountB.write(stream);
                      stream.writeLong(10L);
                      stream.writeString("Memo");
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Transfers are locked");
  }

  @Test
  void transferTwoPhaseSuccessful() {
    long amount = 1234;
    MpcTokenContractState updatedState = transfer(initial, accountB, amount);
    Assertions.assertThat(updatedState.getTransfers().size()).isEqualTo(1);
    Assertions.assertThat(
            updatedState.getTransfers().getValue(context.getCurrentTransactionHash()).initiator)
        .isEqualTo(accountA);

    byte[] senderRpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] senderRpcExpected = AccountPluginRpc.initiateWithdraw(hashA, amount);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
    byte[] recipientRpcActual = context.getUpdateLocalAccountPluginStates().get(1).getRpc();
    byte[] recipientRpcExpected = AccountPluginRpc.initiateDeposit(hashA, amount);
    Assertions.assertThat(recipientRpcActual).isEqualTo(recipientRpcExpected);

    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            MpcTokenContract.Callbacks.tokenTransfer(hashA, accountA, accountB));
    Assertions.assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
    CallbackContext callbackContext = createTwoPhaseCallbackContext(true, true);

    MpcTokenContractState callbackState =
        serialization.callback(
            context,
            callbackContext,
            updatedState,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Callbacks.TRANSFER_CALLBACK);
              hashA.write(rpc);
              accountA.write(rpc);
              accountB.write(rpc);
            });
    Assertions.assertThat(callbackState.getTransfers().size()).isEqualTo(0);

    senderRpcActual = context.getUpdateLocalAccountPluginStates().get(2).getRpc();
    senderRpcExpected = AccountPluginRpc.commitTransfer(hashA);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
    recipientRpcActual = context.getUpdateLocalAccountPluginStates().get(3).getRpc();
    recipientRpcExpected = AccountPluginRpc.commitTransfer(hashA);
    Assertions.assertThat(recipientRpcActual).isEqualTo(recipientRpcExpected);
  }

  /**
   * If a transfer has been aborted through {@link MpcTokenContract.Invocations#ABORT} prior to the
   * callback being executed then it has been removed from the state of the contract and no further
   * actions should be taken.
   */
  @Test
  void transferCallbackFailIfNoLongerPresentInState() {
    CallbackContext callbackContext = createTwoPhaseCallbackContext(true, true);
    Assertions.assertThatThrownBy(
            () ->
                serialization.callback(
                    context,
                    callbackContext,
                    initial,
                    rpc -> {
                      rpc.writeByte(MpcTokenContract.Callbacks.TRANSFER_CALLBACK);
                      hashA.write(rpc);
                      accountA.write(rpc);
                      accountB.write(rpc);
                    }))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Two phase already handled");
  }

  @Test
  void transferOnBehalfOf() {
    long amount = 1234;
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Invocations.TRANSFER_ON_BEHALF_OF);
              accountB.write(rpc);
              accountC.write(rpc);
              rpc.writeLong(amount);
            });
    Assertions.assertThat(updatedState.getTransfers().size()).isEqualTo(1);
    Assertions.assertThat(updatedState.getTransfers().getValue(hashA).initiator)
        .isEqualTo(accountA);

    byte[] senderRpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] senderRpcExpected = AccountPluginRpc.initiateWithdrawOnBehalfOf(accountA, hashA, amount);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
    byte[] recipientRpcActual = context.getUpdateLocalAccountPluginStates().get(1).getRpc();
    byte[] recipientRpcExpected = AccountPluginRpc.initiateDeposit(hashA, amount);
    Assertions.assertThat(recipientRpcActual).isEqualTo(recipientRpcExpected);

    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            MpcTokenContract.Callbacks.tokenTransfer(hashA, accountB, accountC));
    Assertions.assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
  }

  @Test
  void transferSmallMemoOnBehalfOf() {
    long amount = 1234;
    long memo = 123;
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Invocations.TRANSFER_SMALL_MEMO_ON_BEHALF_OF);
              accountB.write(rpc);
              accountC.write(rpc);
              rpc.writeLong(amount);
              rpc.writeLong(memo);
            });
    Assertions.assertThat(updatedState.getTransfers().size()).isEqualTo(1);

    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            MpcTokenContract.Callbacks.tokenTransfer(hashA, accountB, accountC));
    Assertions.assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
    byte[] recipientRpcActual = context.getUpdateLocalAccountPluginStates().get(1).getRpc();
    byte[] recipientRpcExpected = AccountPluginRpc.initiateDeposit(hashA, amount);
    Assertions.assertThat(recipientRpcActual).isEqualTo(recipientRpcExpected);
    byte[] senderRpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] senderRpcExpected = AccountPluginRpc.initiateWithdrawOnBehalfOf(accountA, hashA, amount);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
  }

  @Test
  void transferLargeMemoOnBehalfOf() {
    long amount = 1234;
    String memo = "this is a memo";
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Invocations.TRANSFER_LARGE_MEMO_ON_BEHALF_OF);
              accountB.write(rpc);
              accountC.write(rpc);
              rpc.writeLong(amount);
              rpc.writeString(memo);
            });
    Assertions.assertThat(updatedState.getTransfers().size()).isEqualTo(1);

    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            MpcTokenContract.Callbacks.tokenTransfer(hashA, accountB, accountC));

    byte[] recipientRpcActual = context.getUpdateLocalAccountPluginStates().get(1).getRpc();
    byte[] recipientRpcExpected = AccountPluginRpc.initiateDeposit(hashA, amount);
    byte[] senderRpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] senderRpcExpected = AccountPluginRpc.initiateWithdrawOnBehalfOf(accountA, hashA, amount);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
    Assertions.assertThat(recipientRpcActual).isEqualTo(recipientRpcExpected);
    Assertions.assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
  }

  @Test
  void transferByocOnBehalfOf() {
    long amount = 1234;
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Invocations.BYOC_TRANSFER_ON_BEHALF_OF);
              accountB.write(rpc);
              accountC.write(rpc);
              Unsigned256.create(amount).write(rpc);
              rpc.writeString(SYMBOL);
            });
    Assertions.assertThat(updatedState.getTransfers().size()).isEqualTo(1);

    byte[] senderRpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] senderRpcExpected =
        AccountPluginRpc.initByocTransferSenderOnBehalfOf(
            accountA, hashA, Unsigned256.create(amount), SYMBOL);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
    byte[] recipientRpcActual = context.getUpdateLocalAccountPluginStates().get(1).getRpc();
    byte[] recipientRpcExpected =
        AccountPluginRpc.initiateByocTransferRecipient(hashA, Unsigned256.create(amount), SYMBOL);
    Assertions.assertThat(recipientRpcActual).isEqualTo(recipientRpcExpected);

    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            MpcTokenContract.Callbacks.tokenTransfer(hashA, accountB, accountC));
    Assertions.assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
  }

  @Test
  void testTwoPhaseSuccessfulByoc() {
    long amount = 1234;
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            stream -> {
              stream.writeByte(MpcTokenContract.Invocations.BYOC_TRANSFER_OLD);
              accountB.write(stream);
              stream.writeLong(amount);
              stream.writeString(SYMBOL);
            });
    Assertions.assertThat(updatedState.getTransfers().size()).isEqualTo(1);

    byte[] senderRpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] senderRpcExpected =
        AccountPluginRpc.initiateByocTransferSender(hashA, Unsigned256.create(amount), SYMBOL);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
    byte[] rpcActual = context.getUpdateLocalAccountPluginStates().get(1).getRpc();
    byte[] rpcExpected =
        AccountPluginRpc.initiateByocTransferRecipient(hashA, Unsigned256.create(amount), SYMBOL);
    Assertions.assertThat(rpcActual).isEqualTo(rpcExpected);

    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            MpcTokenContract.Callbacks.tokenTransfer(hashA, accountA, accountB));
    Assertions.assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
  }

  @Test
  void twoPhaseCommitToYourself() {
    long amount = 1234;
    Assertions.assertThatThrownBy(() -> transfer(initial, accountA, amount))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot transfer between same account");
  }

  @Test
  void twoPhaseCommitFails() {
    long amount = 5;
    MpcTokenContractState updatedState = transfer(initial, accountB, amount);
    Assertions.assertThat(updatedState.getStakeDelegations().size()).isEqualTo(0);

    byte[] senderRpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] senderRpcExpected = AccountPluginRpc.initiateWithdraw(hashA, amount);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
    byte[] recipientRpcActual = context.getUpdateLocalAccountPluginStates().get(1).getRpc();
    byte[] recipientRpcExpected = AccountPluginRpc.initiateDeposit(hashA, amount);
    Assertions.assertThat(recipientRpcActual).isEqualTo(recipientRpcExpected);

    CallbackContext callbackContext = createTwoPhaseCallbackContext(true, false);

    MpcTokenContractState callbackState =
        serialization.callback(
            context,
            callbackContext,
            updatedState,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Callbacks.TRANSFER_CALLBACK);
              hashA.write(rpc);
              accountA.write(rpc);
              accountB.write(rpc);
            });

    Assertions.assertThat(callbackState.getTransfers().size()).isEqualTo(0);
    senderRpcActual = context.getUpdateLocalAccountPluginStates().get(2).getRpc();
    senderRpcExpected = AccountPluginRpc.abortTransfer(hashA);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
    recipientRpcActual = context.getUpdateLocalAccountPluginStates().get(3).getRpc();
    recipientRpcExpected = AccountPluginRpc.abortTransfer(hashA);
    Assertions.assertThat(recipientRpcActual).isEqualTo(recipientRpcExpected);

    callbackContext = createTwoPhaseCallbackContext(false, true);
    MpcTokenContractState callbackState2 =
        serialization.callback(
            context,
            callbackContext,
            updatedState,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Callbacks.TRANSFER_CALLBACK);
              hashA.write(rpc);
              accountA.write(rpc);
              accountB.write(rpc);
            });
    Assertions.assertThat(callbackState2.getTransfers().size()).isEqualTo(0);
    senderRpcActual = context.getUpdateLocalAccountPluginStates().get(4).getRpc();
    senderRpcExpected = AccountPluginRpc.abortTransfer(hashA);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
    recipientRpcActual = context.getUpdateLocalAccountPluginStates().get(5).getRpc();
    recipientRpcExpected = AccountPluginRpc.abortTransfer(hashA);
    Assertions.assertThat(recipientRpcActual).isEqualTo(recipientRpcExpected);

    callbackContext = createTwoPhaseCallbackContext(false, false);
    MpcTokenContractState callbackState3 =
        serialization.callback(
            context,
            callbackContext,
            updatedState,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Callbacks.TRANSFER_CALLBACK);
              hashA.write(rpc);
              accountA.write(rpc);
              accountB.write(rpc);
            });
    Assertions.assertThat(callbackState3.getTransfers().size()).isEqualTo(0);
    senderRpcActual = context.getUpdateLocalAccountPluginStates().get(6).getRpc();
    senderRpcExpected = AccountPluginRpc.abortTransfer(hashA);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
    recipientRpcActual = context.getUpdateLocalAccountPluginStates().get(7).getRpc();
    recipientRpcExpected = AccountPluginRpc.abortTransfer(hashA);
    Assertions.assertThat(recipientRpcActual).isEqualTo(recipientRpcExpected);
  }

  @Test
  void twoPhaseCommitFailsOnBehalfOf() {
    long amount = 5;
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Invocations.TRANSFER_ON_BEHALF_OF);
              accountB.write(rpc);
              accountC.write(rpc);
              rpc.writeLong(amount);
            });

    Assertions.assertThat(updatedState.getStakeDelegations().size()).isEqualTo(0);

    byte[] senderRpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] senderRpcExpected = AccountPluginRpc.initiateWithdrawOnBehalfOf(accountA, hashA, amount);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
    byte[] recipientRpcActual = context.getUpdateLocalAccountPluginStates().get(1).getRpc();
    byte[] recipientRpcExpected = AccountPluginRpc.initiateDeposit(hashA, amount);
    Assertions.assertThat(recipientRpcActual).isEqualTo(recipientRpcExpected);

    CallbackContext callbackContext = createTwoPhaseCallbackContext(false, false);
    MpcTokenContractState callbackState3 =
        serialization.callback(
            context,
            callbackContext,
            updatedState,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Callbacks.TRANSFER_CALLBACK);
              hashA.write(rpc);
              accountA.write(rpc);
              accountB.write(rpc);
            });
    Assertions.assertThat(callbackState3.getTransfers().size()).isEqualTo(0);
    senderRpcActual = context.getUpdateLocalAccountPluginStates().get(2).getRpc();
    senderRpcExpected = AccountPluginRpc.abortTransfer(hashA);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
    recipientRpcActual = context.getUpdateLocalAccountPluginStates().get(3).getRpc();
    recipientRpcExpected = AccountPluginRpc.abortTransfer(hashA);
    Assertions.assertThat(recipientRpcActual).isEqualTo(recipientRpcExpected);
  }

  @Test
  void invokeAbort() {
    long amount = 10;
    TransferInformation transferInformation =
        TransferInformation.create(accountA, accountB, Unsigned256.create(amount), null, accountA);

    MpcTokenContractState populatedState =
        MpcTokenContractState.initial(false).addTransfer(hashA, transferInformation);

    Assertions.assertThat(populatedState.getTransfers().size()).isEqualTo(1);

    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            populatedState,
            stream -> {
              stream.writeByte(MpcTokenContract.Invocations.ABORT);
              hashA.write(stream);
            });
    Assertions.assertThat(updatedState.getTransfers().size()).isEqualTo(0);
    SafeDataInputStream safeDataInputStream =
        SafeDataInputStream.createFromBytes(
            context.getUpdateLocalAccountPluginStates().get(0).getRpc());
    int invocationByte = safeDataInputStream.readUnsignedByte();
    Assertions.assertThat(invocationByte).isEqualTo(AccountPluginRpc.ABORT_TRANSFER);
  }

  @Test
  void invokeAbortOnBehalfOf() {
    long amount = 10;
    TransferInformation custodianTransfer =
        TransferInformation.create(accountC, accountB, Unsigned256.create(amount), null, accountA);

    TransferInformation regularTransfer =
        TransferInformation.create(accountA, accountB, Unsigned256.create(amount), null, accountA);

    MpcTokenContractState populatedState =
        MpcTokenContractState.initial(false)
            .addTransfer(hashA, custodianTransfer)
            .addTransfer(hashB, regularTransfer);

    Assertions.assertThat(populatedState.getTransfers().size()).isEqualTo(2);

    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            populatedState,
            stream -> {
              stream.writeByte(MpcTokenContract.Invocations.ABORT);
              hashA.write(stream);
            });

    byte[] rpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] rpcExpected = AccountPluginRpc.abortTransfer(hashA);
    Assertions.assertThat(rpcActual).isEqualTo(rpcExpected);
    Assertions.assertThat(updatedState).isNotNull();
  }

  @Test
  void invokeAbort_invalid() {
    MpcTokenContractState emptyState = MpcTokenContractState.initial(false);
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    emptyState,
                    stream -> {
                      stream.writeByte(MpcTokenContract.Invocations.ABORT);
                      hashA.write(stream);
                    }))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot abort a new transfer");

    MpcTokenContractState nonEmptyState =
        MpcTokenContractState.initial(false)
            .addTransfer(
                hashA,
                TransferInformation.create(
                    accountB, accountA, Unsigned256.create(1000), SYMBOL, null))
            .addTransfer(
                hashB,
                TransferInformation.create(
                    accountA, accountA, Unsigned256.create(1000), SYMBOL, accountB));

    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    nonEmptyState,
                    stream -> {
                      stream.writeByte(MpcTokenContract.Invocations.ABORT);
                      hashA.write(stream);
                    }))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot abort a pending transfer that you are not the original sender of");

    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    nonEmptyState,
                    stream -> {
                      stream.writeByte(MpcTokenContract.Invocations.ABORT);
                      hashB.write(stream);
                    }))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot abort a pending transfer that you are not the original sender of");
  }

  @Test
  void invalidIceInvocations() {
    Assertions.assertThatThrownBy(
            () -> iceInvocation(initial, MpcTokenContract.Invocations.ASSOCIATE_ICE, accountB, 100))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Can only ice stake against contracts");
    Assertions.assertThatThrownBy(
            () ->
                iceInvocation(
                    initial, MpcTokenContract.Invocations.DISASSOCIATE_ICE, accountB, 100))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Can only disassociate ice stakes from contracts");
    Assertions.assertThatThrownBy(
            () ->
                iceInvocation(
                    initial, MpcTokenContract.Invocations.ASSOCIATE_ICE, publicContract, 0))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Amount must be positive");
    Assertions.assertThatThrownBy(
            () ->
                iceInvocation(
                    initial, MpcTokenContract.Invocations.DISASSOCIATE_ICE, publicContract, 0))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Can only disassociate a positive amount");
    context = new SysContractContextTest(1, 100, publicContract);
    Assertions.assertThatThrownBy(
            () ->
                iceInvocation(
                    initial, MpcTokenContract.Invocations.ASSOCIATE_ICE, publicContract, 100))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Can only ice stake from accounts");
    Assertions.assertThatThrownBy(
            () ->
                iceInvocation(
                    initial, MpcTokenContract.Invocations.DISASSOCIATE_ICE, publicContract, 100))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only accounts can disassociate ice stakes");

    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    initial,
                    s -> {
                      s.writeByte(MpcTokenContract.Invocations.ASSOCIATE_ICE_ON_BEHALF_OF);
                      accountB.write(s);
                      accountA.write(s);
                      s.writeLong(100);
                    }))
        .hasMessage("Can only ice stake against contracts");

    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    initial,
                    s -> {
                      s.writeByte(MpcTokenContract.Invocations.ASSOCIATE_ICE_ON_BEHALF_OF);
                      accountB.write(s);
                      publicContract.write(s);
                      s.writeLong(0);
                    }))
        .hasMessage("Amount must be positive");

    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    initial,
                    s -> {
                      s.writeByte(MpcTokenContract.Invocations.ASSOCIATE_ICE_ON_BEHALF_OF);
                      publicContract.write(s);
                      publicContract.write(s);
                      s.writeLong(100);
                    }))
        .hasMessage("Can only ice stake from accounts");

    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    initial,
                    s -> {
                      s.writeByte(MpcTokenContract.Invocations.DISASSOCIATE_ICE_ON_BEHALF_OF);
                      accountB.write(s);
                      accountA.write(s);
                      s.writeLong(100);
                    }))
        .hasMessage("Can only disassociate ice stakes from contracts");
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    initial,
                    s -> {
                      s.writeByte(MpcTokenContract.Invocations.DISASSOCIATE_ICE_ON_BEHALF_OF);
                      accountB.write(s);
                      publicContract.write(s);
                      s.writeLong(0);
                    }))
        .hasMessage("Can only disassociate a positive amount");
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    initial,
                    s -> {
                      s.writeByte(MpcTokenContract.Invocations.DISASSOCIATE_ICE_ON_BEHALF_OF);
                      publicContract.write(s);
                      publicContract.write(s);
                      s.writeLong(100);
                    }))
        .hasMessage("Only accounts can disassociate ice stakes");
  }

  @Test
  void successfulIceAssociation() {
    long amount = 100;
    MpcTokenContractState updatedState =
        iceInvocation(initial, MpcTokenContract.Invocations.ASSOCIATE_ICE, publicContract, amount);

    Assertions.assertThat(updatedState.getIcedStakings().size()).isEqualTo(1);
    byte[] senderRpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] senderRpcExpected =
        AccountPluginRpc.initiateIceAssociateOnBehalfOf(null, hashA, amount, publicContract);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);

    byte[] contractRpcActual = context.getUpdateLocalAccountPluginStates().get(1).getRpc();
    byte[] contractRpcExpected =
        AccountPluginRpc.initiateIceAssociateContract(hashA, amount, accountA);
    Assertions.assertThat(contractRpcActual).isEqualTo(contractRpcExpected);

    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            MpcTokenContract.Callbacks.iceTokensStaking(hashA, accountA, publicContract));
    Assertions.assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
    CallbackContext callbackContext = createTwoPhaseCallbackContext(true, true);
    MpcTokenContractState callbackState =
        serialization.callback(
            context, callbackContext, updatedState, s -> s.write(actualCallbackRpc));
    Assertions.assertThat(callbackState.getIcedStakings().size()).isEqualTo(0);

    senderRpcActual = context.getUpdateLocalAccountPluginStates().get(2).getRpc();
    contractRpcExpected = AccountPluginRpc.commitIceAssociate(hashA);
    Assertions.assertThat(senderRpcActual).isEqualTo(contractRpcExpected);
  }

  /**
   * If a ice association has been aborted through {@link
   * MpcTokenContract.Invocations#ABORT_ICE_ASSOCIATE} prior to the callback being executed then it
   * has been removed from the state of the contract and no further actions should be taken.
   */
  @Test
  void iceAssociationCallbackFailIfNoLongerPresentInState() {
    CallbackContext callbackContext = createTwoPhaseCallbackContext(true, true);
    Assertions.assertThatThrownBy(
            () ->
                serialization.callback(
                    context,
                    callbackContext,
                    initial,
                    MpcTokenContract.Callbacks.iceTokensStaking(hashA, accountA, publicContract)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Two phase already handled");
  }

  @Test
  void successfulIceAssociationOnBehalfOf() {
    long amount = 100;
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            stream -> {
              stream.writeByte(MpcTokenContract.Invocations.ASSOCIATE_ICE_ON_BEHALF_OF);
              accountB.write(stream);
              publicContract.write(stream);
              stream.writeLong(amount);
            });
    Assertions.assertThat(updatedState.getIcedStakings().size()).isEqualTo(1);

    byte[] senderRpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] senderRpcExpected =
        AccountPluginRpc.initiateIceAssociateOnBehalfOf(accountA, hashA, amount, publicContract);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);

    byte[] contractRpcActual = context.getUpdateLocalAccountPluginStates().get(1).getRpc();
    byte[] contractRpcExpected =
        AccountPluginRpc.initiateIceAssociateContract(hashA, amount, accountB);
    Assertions.assertThat(contractRpcActual).isEqualTo(contractRpcExpected);

    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            MpcTokenContract.Callbacks.iceTokensStaking(hashA, accountB, publicContract));
    Assertions.assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);

    CallbackContext callbackContext = createTwoPhaseCallbackContext(true, true);
    MpcTokenContractState callbackState =
        serialization.callback(
            context, callbackContext, updatedState, s -> s.write(actualCallbackRpc));

    Assertions.assertThat(callbackState.getIcedStakings().size()).isEqualTo(0);

    senderRpcActual = context.getUpdateLocalAccountPluginStates().get(2).getRpc();
    senderRpcExpected = AccountPluginRpc.commitIceAssociate(hashA);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);

    contractRpcActual = context.getUpdateLocalAccountPluginStates().get(3).getRpc();
    contractRpcExpected = AccountPluginRpc.commitIceAssociate(hashA);
    Assertions.assertThat(contractRpcActual).isEqualTo(contractRpcExpected);
  }

  @Test
  void callbackFailsIceAssociation() {
    long amount = 100;
    MpcTokenContractState updatedState =
        iceInvocation(initial, MpcTokenContract.Invocations.ASSOCIATE_ICE, publicContract, amount);
    Assertions.assertThat(updatedState.getIcedStakings().size()).isEqualTo(1);
    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            MpcTokenContract.Callbacks.iceTokensStaking(hashA, accountA, publicContract));
    Assertions.assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
    CallbackContext callbackContext = createTwoPhaseCallbackContext(true, false);
    MpcTokenContractState callbackState =
        serialization.callback(
            context, callbackContext, updatedState, s -> s.write(actualCallbackRpc));
    Assertions.assertThat(callbackState.getIcedStakings().size()).isEqualTo(0);

    byte[] senderRpcActual = context.getUpdateLocalAccountPluginStates().get(2).getRpc();
    byte[] senderRpcExpected = AccountPluginRpc.abortIceAssociate(hashA);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
  }

  @Test
  void disassociateIce() {
    long amount = 100;
    MpcTokenContractState updatedState =
        iceInvocation(
            initial, MpcTokenContract.Invocations.DISASSOCIATE_ICE, publicContract, amount);
    Assertions.assertThat(updatedState.getIcedStakings().size()).isEqualTo(1);
    byte[] senderRpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] senderRpcExpected =
        AccountPluginRpc.initiateIceDisassociate(hashA, amount, publicContract);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);

    byte[] contractRpcActual = context.getUpdateLocalAccountPluginStates().get(1).getRpc();
    byte[] contractRpcExpected =
        AccountPluginRpc.initiateIceDisassociateContract(hashA, amount, accountA);
    Assertions.assertThat(contractRpcActual).isEqualTo(contractRpcExpected);
    CallbackContext callbackContext = createTwoPhaseCallbackContext(false, true);
    MpcTokenContractState callbackState =
        serialization.callback(
            context,
            callbackContext,
            updatedState,
            s -> s.write(context.getRemoteCalls().callbackRpc));
    Assertions.assertThat(callbackState.getIcedStakings().size()).isEqualTo(0);

    senderRpcActual = context.getUpdateLocalAccountPluginStates().get(2).getRpc();
    senderRpcExpected = AccountPluginRpc.abortIceAssociate(hashA);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);

    contractRpcActual = context.getUpdateLocalAccountPluginStates().get(3).getRpc();
    contractRpcExpected = AccountPluginRpc.abortIceAssociate(hashA);
    Assertions.assertThat(contractRpcActual).isEqualTo(contractRpcExpected);

    CallbackContext callbackContextCommit = createTwoPhaseCallbackContext(true, true);
    MpcTokenContractState callbackStateCommit =
        serialization.callback(
            context,
            callbackContextCommit,
            updatedState,
            s -> s.write(context.getRemoteCalls().callbackRpc));
    Assertions.assertThat(callbackStateCommit.getIcedStakings().size()).isEqualTo(0);

    byte[] senderRpcActualCommit = context.getUpdateLocalAccountPluginStates().get(4).getRpc();
    byte[] senderRpcExpectedCommit = AccountPluginRpc.commitIceAssociate(hashA);
    Assertions.assertThat(senderRpcActualCommit).isEqualTo(senderRpcExpectedCommit);
  }

  @Test
  void disassociateIceOnBehalfOf() {
    long amount = 100;

    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            s -> {
              s.writeByte(MpcTokenContract.Invocations.DISASSOCIATE_ICE_ON_BEHALF_OF);
              accountB.write(s);
              publicContract.write(s);
              s.writeLong(amount);
            });
    Assertions.assertThat(updatedState.getIcedStakings().size()).isEqualTo(1);

    CallbackContext callbackContextCommit = createTwoPhaseCallbackContext(true, true);
    MpcTokenContractState callbackStateCommit =
        serialization.callback(
            context,
            callbackContextCommit,
            updatedState,
            s -> s.write(context.getRemoteCalls().callbackRpc));
    Assertions.assertThat(callbackStateCommit.getIcedStakings().size()).isEqualTo(0);

    byte[] senderRpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] senderRpcExpected =
        AccountPluginRpc.initiateIceDisassociateOnBehalfOf(accountA, hashA, amount, publicContract);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
    byte[] contractRpcActual = context.getUpdateLocalAccountPluginStates().get(1).getRpc();
    byte[] contractRpcExpected =
        AccountPluginRpc.initiateIceDisassociateContract(hashA, amount, accountB);
    Assertions.assertThat(contractRpcActual).isEqualTo(contractRpcExpected);
  }

  @Test
  void delegateStakesTwoPhaseSuccessful() {
    long amount = 1234;

    MpcTokenContractState updatedState =
        stakeDelegation(initial, MpcTokenContract.Invocations.DELEGATE_STAKES, accountB, amount);

    Assertions.assertThat(updatedState.getStakeDelegations().size()).isEqualTo(1);
    Assertions.assertThat(
            updatedState
                .getStakeDelegations()
                .getValue(context.getCurrentTransactionHash())
                .initiator)
        .isEqualTo(accountA);

    byte[] senderRpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] senderRpcExpected = AccountPluginRpc.initiateDelegateStakes(hashA, amount, accountB);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
    byte[] recipientRpcActual = context.getUpdateLocalAccountPluginStates().get(1).getRpc();
    byte[] recipientRpcExpected =
        AccountPluginRpc.initiateReceiveDelegatedStakes(hashA, amount, accountA);
    Assertions.assertThat(recipientRpcActual).isEqualTo(recipientRpcExpected);

    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            MpcTokenContract.Callbacks.stakeDelegation(hashA, accountA, accountB));
    Assertions.assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);

    CallbackContext callbackContext = createTwoPhaseCallbackContext(true, true);

    MpcTokenContractState callbackState =
        serialization.callback(
            context,
            callbackContext,
            updatedState,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Callbacks.STAKE_DELEGATION_CALLBACK);
              hashA.write(rpc);
              accountA.write(rpc);
              accountB.write(rpc);
            });

    Assertions.assertThat(callbackState.getStakeDelegations().size()).isEqualTo(0);

    senderRpcActual = context.getUpdateLocalAccountPluginStates().get(2).getRpc();
    senderRpcExpected = AccountPluginRpc.commitDelegateStakes(hashA);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
    recipientRpcActual = context.getUpdateLocalAccountPluginStates().get(3).getRpc();
    recipientRpcExpected = AccountPluginRpc.commitDelegateStakes(hashA);
    Assertions.assertThat(recipientRpcActual).isEqualTo(recipientRpcExpected);
  }

  @Test
  void stakeDelegationOnBehalfOf() {
    long amount = 1234;

    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            stream -> {
              stream.writeByte(MpcTokenContract.Invocations.DELEGATE_STAKES_ON_BEHALF_OF);
              accountC.write(stream);
              accountB.write(stream);
              stream.writeLong(amount);
            });

    Assertions.assertThat(updatedState.getStakeDelegations().size()).isEqualTo(1);

    LocalPluginStateUpdate accountPluginUpdate0 =
        context.getUpdateLocalAccountPluginStates().get(0);
    Assertions.assertThat(accountPluginUpdate0.getContext()).isEqualTo(accountC);
    byte[] senderRpcExpected =
        AccountPluginRpc.initiateDelegateStakesOnBehalfOf(accountA, hashA, amount, accountB);
    Assertions.assertThat(accountPluginUpdate0.getRpc()).isEqualTo(senderRpcExpected);
    LocalPluginStateUpdate accountPluginUpdate1 =
        context.getUpdateLocalAccountPluginStates().get(1);
    Assertions.assertThat(accountPluginUpdate1.getContext()).isEqualTo(accountB);
    byte[] recipientRpcExpected =
        AccountPluginRpc.initiateReceiveDelegatedStakes(hashA, amount, accountC);
    Assertions.assertThat(accountPluginUpdate1.getRpc()).isEqualTo(recipientRpcExpected);

    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            MpcTokenContract.Callbacks.stakeDelegation(hashA, accountC, accountB));
    Assertions.assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
  }

  @Test
  void twoPhaseCommitToYourself_stake() {
    long amount = 1234;
    Assertions.assertThatThrownBy(
            () ->
                stakeDelegation(
                    initial, MpcTokenContract.Invocations.DELEGATE_STAKES, accountA, amount))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot delegate stakes between the same account using this operation");
  }

  @Test
  void twoPhaseCommitFails_stake() {
    long amount = 5;
    MpcTokenContractState updatedState =
        stakeDelegation(initial, MpcTokenContract.Invocations.DELEGATE_STAKES, accountB, amount);

    byte[] senderRpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] senderRpcExpected = AccountPluginRpc.initiateDelegateStakes(hashA, amount, accountB);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
    byte[] recipientRpcActual = context.getUpdateLocalAccountPluginStates().get(1).getRpc();
    byte[] recipientRpcExpected =
        AccountPluginRpc.initiateReceiveDelegatedStakes(hashA, amount, accountA);
    Assertions.assertThat(recipientRpcActual).isEqualTo(recipientRpcExpected);

    CallbackContext callbackContext = createTwoPhaseCallbackContext(true, false);
    MpcTokenContractState callbackState =
        serialization.callback(
            context,
            callbackContext,
            updatedState,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Callbacks.STAKE_DELEGATION_CALLBACK);
              hashA.write(rpc);
              accountA.write(rpc);
              accountB.write(rpc);
            });

    Assertions.assertThat(callbackState.getStakeDelegations().size()).isEqualTo(0);
    senderRpcActual = context.getUpdateLocalAccountPluginStates().get(2).getRpc();
    senderRpcExpected = AccountPluginRpc.abortDelegatedStakes(hashA);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
    recipientRpcActual = context.getUpdateLocalAccountPluginStates().get(3).getRpc();
    recipientRpcExpected = AccountPluginRpc.abortDelegatedStakes(hashA);
    Assertions.assertThat(recipientRpcActual).isEqualTo(recipientRpcExpected);

    callbackContext = createTwoPhaseCallbackContext(false, true);
    MpcTokenContractState callbackState2 =
        serialization.callback(
            context,
            callbackContext,
            updatedState,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Callbacks.STAKE_DELEGATION_CALLBACK);
              hashA.write(rpc);
              accountA.write(rpc);
              accountB.write(rpc);
            });

    Assertions.assertThat(callbackState2.getStakeDelegations().size()).isEqualTo(0);
    senderRpcActual = context.getUpdateLocalAccountPluginStates().get(4).getRpc();
    senderRpcExpected = AccountPluginRpc.abortDelegatedStakes(hashA);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
    recipientRpcActual = context.getUpdateLocalAccountPluginStates().get(5).getRpc();
    recipientRpcExpected = AccountPluginRpc.abortDelegatedStakes(hashA);
    Assertions.assertThat(recipientRpcActual).isEqualTo(recipientRpcExpected);

    callbackContext = createTwoPhaseCallbackContext(false, false);
    MpcTokenContractState callbackState3 =
        serialization.callback(
            context,
            callbackContext,
            updatedState,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Callbacks.STAKE_DELEGATION_CALLBACK);
              hashA.write(rpc);
              accountA.write(rpc);
              accountB.write(rpc);
            });

    Assertions.assertThat(callbackState3.getStakeDelegations().size()).isEqualTo(0);
    senderRpcActual = context.getUpdateLocalAccountPluginStates().get(6).getRpc();
    senderRpcExpected = AccountPluginRpc.abortDelegatedStakes(hashA);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
    recipientRpcActual = context.getUpdateLocalAccountPluginStates().get(7).getRpc();
    recipientRpcExpected = AccountPluginRpc.abortDelegatedStakes(hashA);
    Assertions.assertThat(recipientRpcActual).isEqualTo(recipientRpcExpected);
  }

  @Test
  void invokeAbort_delegateStakes() {
    long amount = 10;
    StakeDelegation stakeDelegation =
        StakeDelegation.create(
            accountA,
            accountB,
            amount,
            StakeDelegation.DelegationType.DELEGATE_STAKES,
            accountA,
            null);
    MpcTokenContractState populatedState =
        MpcTokenContractState.initial(false).addStakeDelegation(hashA, stakeDelegation);
    Assertions.assertThat(populatedState.getStakeDelegations().size()).isEqualTo(1);

    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            populatedState,
            stream -> {
              stream.writeByte(MpcTokenContract.Invocations.ABORT_STAKE_DELEGATION_EVENT);
              hashA.write(stream);
            });

    Assertions.assertThat(updatedState.getStakeDelegations().size()).isEqualTo(0);
    byte[] senderRpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] senderRpcExpected = AccountPluginRpc.abortDelegatedStakes(hashA);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
    byte[] recipientRpcActual = context.getUpdateLocalAccountPluginStates().get(1).getRpc();
    byte[] recipientRpcExpected = AccountPluginRpc.abortDelegatedStakes(hashA);
    Assertions.assertThat(recipientRpcActual).isEqualTo(recipientRpcExpected);
  }

  @Test
  void invokeAbort_iceAssociation() {

    IceStaking iceStaking = new IceStaking(accountA, accountB, 100, accountA);

    MpcTokenContractState populatedState =
        MpcTokenContractState.initial(false).addIcedTokens(hashA, iceStaking);
    Assertions.assertThat(populatedState.getIcedStakings().size()).isEqualTo(1);

    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            populatedState,
            stream -> {
              stream.writeByte(MpcTokenContract.Invocations.ABORT_ICE_ASSOCIATE);
              hashA.write(stream);
            });

    Assertions.assertThat(updatedState.getIcedStakings().size()).isEqualTo(0);
    byte[] senderRpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] senderRpcExpected = AccountPluginRpc.abortIceAssociate(hashA);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
    byte[] recipientRpcActual = context.getUpdateLocalAccountPluginStates().get(1).getRpc();
    byte[] recipientRpcExpected = AccountPluginRpc.abortIceAssociate(hashA);
    Assertions.assertThat(recipientRpcActual).isEqualTo(recipientRpcExpected);
  }

  @Test
  void invokeAbort_invalidIceAssociation() {
    MpcTokenContractState emptyState = MpcTokenContractState.initial(false);
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    emptyState,
                    stream -> {
                      stream.writeByte(MpcTokenContract.Invocations.ABORT_ICE_ASSOCIATE);
                      hashA.write(stream);
                      stream.writeEnum(StakeDelegation.DelegationType.DELEGATE_STAKES);
                    }))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot abort action for id that does not exist");

    MpcTokenContractState nonEmptyState =
        MpcTokenContractState.initial(false)
            .addIcedTokens(hashA, new IceStaking(accountB, accountA, 100, accountB));

    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    nonEmptyState,
                    stream -> {
                      stream.writeByte(MpcTokenContract.Invocations.ABORT_ICE_ASSOCIATE);
                      hashA.write(stream);
                    }))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(
            "Cannot abort an ice association event that you are not the original sender of");
  }

  @Test
  void invokeAbort_invalidStake() {
    MpcTokenContractState emptyState = MpcTokenContractState.initial(false);
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    emptyState,
                    stream -> {
                      stream.writeByte(MpcTokenContract.Invocations.ABORT_STAKE_DELEGATION_EVENT);
                      hashA.write(stream);
                      stream.writeEnum(StakeDelegation.DelegationType.DELEGATE_STAKES);
                    }))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Cannot abort action for id that does not exist");

    MpcTokenContractState nonEmptyState =
        MpcTokenContractState.initial(false)
            .addStakeDelegation(
                hashA,
                StakeDelegation.create(
                    accountB,
                    accountA,
                    1_000L,
                    StakeDelegation.DelegationType.RETRACT_DELEGATED_STAKES,
                    accountB,
                    null));

    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    nonEmptyState,
                    stream -> {
                      stream.writeByte(MpcTokenContract.Invocations.ABORT_STAKE_DELEGATION_EVENT);
                      hashA.write(stream);
                    }))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(
            "Cannot abort a stake delegation event that you are not the original sender of");
  }

  @Test
  void retractDelegatedStakesTwoPhaseSuccessful() {
    long amount = 1234;
    MpcTokenContractState updatedState =
        stakeDelegation(
            initial, MpcTokenContract.Invocations.RETRACT_DELEGATED_STAKES, accountB, amount);
    Assertions.assertThat(updatedState.getStakeDelegations().size()).isEqualTo(1);

    byte[] senderRpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] senderRpcExpected =
        AccountPluginRpc.initiateRetractDelegatedStakes(hashA, amount, accountB);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
    byte[] recipientRpcActual = context.getUpdateLocalAccountPluginStates().get(1).getRpc();
    byte[] recipientRpcExpected =
        AccountPluginRpc.initiateReturnDelegatedStakes(hashA, amount, accountA);
    Assertions.assertThat(recipientRpcActual).isEqualTo(recipientRpcExpected);

    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            MpcTokenContract.Callbacks.stakeDelegation(hashA, accountA, accountB));
    Assertions.assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
  }

  @Test
  void testTwoPhaseSuccessful_retractDelegatedStakesOnBehalfOf() {
    long amount = 1234;
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            stream -> {
              stream.writeByte(MpcTokenContract.Invocations.RETRACT_DELEGATED_STAKES_ON_BEHALF_OF);
              accountB.write(stream);
              accountC.write(stream);
              stream.writeLong(amount);
            });
    Assertions.assertThat(updatedState.getStakeDelegations().size()).isEqualTo(1);

    byte[] senderRpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] senderRpcExpected =
        AccountPluginRpc.initiateRetractDelegatedStakesOnBehalfOf(
            accountA, hashA, amount, accountC);
    Assertions.assertThat(senderRpcActual).isEqualTo(senderRpcExpected);
    byte[] recipientRpcActual = context.getUpdateLocalAccountPluginStates().get(1).getRpc();
    byte[] recipientRpcExpected =
        AccountPluginRpc.initiateReturnDelegatedStakes(hashA, amount, accountB);
    Assertions.assertThat(recipientRpcActual).isEqualTo(recipientRpcExpected);

    byte[] actualCallbackRpc = context.getRemoteCalls().callbackRpc;
    byte[] expectedCallbackRpc =
        SafeDataOutputStream.serialize(
            MpcTokenContract.Callbacks.stakeDelegation(hashA, accountB, accountC));
    Assertions.assertThat(actualCallbackRpc).isEqualTo(expectedCallbackRpc);
  }

  @Test
  void twoPhaseCommitToYourself_retractDelegatedStakes() {
    long amount = 1234;
    Assertions.assertThatThrownBy(
            () ->
                stakeDelegation(
                    initial,
                    MpcTokenContract.Invocations.RETRACT_DELEGATED_STAKES,
                    accountA,
                    amount))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage(
            "Cannot retract delegated stakes between the same account using this operation");
  }

  @Test
  void invokeTransferNonAccountRecipient() {
    BlockchainAddress recipient = createContractAddress();
    Assertions.assertThatThrownBy(() -> transfer(initial, recipient, 1))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Only blockchain addresses of type ACCOUNT can receive tokens or coins");
  }

  @Test
  void invokeDelegateStakesNonAccountRecipient() {
    BlockchainAddress recipient = createContractAddress();
    Assertions.assertThatThrownBy(
            () -> stakeDelegation(null, MpcTokenContract.Invocations.DELEGATE_STAKES, recipient, 1))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Only blockchain addresses of type ACCOUNT can receive delegated stakes "
                + "from another account");
  }

  @Test
  void invokeRetractDelegatedStakesNonAccountRecipient() {
    BlockchainAddress recipient = createContractAddress();
    Assertions.assertThatThrownBy(
            () ->
                stakeDelegation(
                    null, MpcTokenContract.Invocations.RETRACT_DELEGATED_STAKES, recipient, 1))
        .isInstanceOf(RuntimeException.class)
        .hasMessage(
            "Only blockchain addresses of type ACCOUNT can get their delegated stakes retracted "
                + "from another account");
  }

  @Test
  void invokeAcceptDelegatedStakes() {
    long amount = 10;
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Invocations.ACCEPT_DELEGATED_STAKES);
              accountB.write(rpc);
              rpc.writeLong(amount);
            });
    byte[] rpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] rpcExpected = AccountPluginRpc.acceptDelegatedStakes(accountB, amount);
    Assertions.assertThat(rpcActual).isEqualTo(rpcExpected);
    Assertions.assertThat(updatedState).isNotNull();
  }

  @Test
  void invokeAcceptDelegatedStakesOnBehalfOf() {
    long amount = 10;
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Invocations.ACCEPT_DELEGATED_STAKES_ON_BEHALF_OF);
              accountB.write(rpc);
              accountB.write(rpc);
              rpc.writeLong(amount);
            });
    byte[] rpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] rpcExpected =
        AccountPluginRpc.acceptDelegatedStakesOnBehalfOf(accountA, accountB, amount);
    Assertions.assertThat(rpcActual).isEqualTo(rpcExpected);
    Assertions.assertThat(updatedState).isNotNull();
  }

  @Test
  void invokeAcceptDelegatedStakesInvalidAmount() {
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    initial,
                    rpc -> {
                      rpc.writeByte(MpcTokenContract.Invocations.ACCEPT_DELEGATED_STAKES);
                      accountB.write(rpc);
                      rpc.writeLong(0);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Amount must be positive");

    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    null,
                    rpc -> {
                      rpc.writeByte(MpcTokenContract.Invocations.ACCEPT_DELEGATED_STAKES);
                      accountB.write(rpc);
                      rpc.writeLong(0);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Amount must be positive");

    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    null,
                    rpc -> {
                      rpc.writeByte(
                          MpcTokenContract.Invocations.ACCEPT_DELEGATED_STAKES_ON_BEHALF_OF);
                      accountB.write(rpc);
                      accountA.write(rpc);
                      rpc.writeLong(0);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Amount must be positive");
  }

  @Test
  void invokeReduceDelegatedStakes() {
    long amount = 10;
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Invocations.REDUCE_DELEGATED_STAKES);
              accountB.write(rpc);
              rpc.writeLong(amount);
            });
    byte[] rpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] rpcExpected = AccountPluginRpc.reduceDelegatedStakes(accountB, amount);
    Assertions.assertThat(rpcActual).isEqualTo(rpcExpected);
    Assertions.assertThat(updatedState).isNotNull();
  }

  @Test
  void invokeReduceDelegatedStakesOnBehalfOf() {
    long amount = 10;
    BlockchainAddress custodian = accountA;
    BlockchainAddress target = accountB;
    BlockchainAddress delegator = accountC;
    Assertions.assertThat(context.getFrom()).isEqualTo(custodian);
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Invocations.REDUCE_DELEGATED_STAKES_ON_BEHALF_OF);
              target.write(rpc);
              delegator.write(rpc);
              rpc.writeLong(amount);
            });
    LocalPluginStateUpdate reduceDelegatedStakesUpdate =
        context.getUpdateLocalAccountPluginStates().get(0);
    Assertions.assertThat(reduceDelegatedStakesUpdate.getContext()).isEqualTo(target);
    byte[] rpcActual = reduceDelegatedStakesUpdate.getRpc();
    byte[] rpcExpected =
        AccountPluginRpc.reduceDelegatedStakesOnBehalfOf(custodian, delegator, amount);
    Assertions.assertThat(rpcActual).isEqualTo(rpcExpected);
    Assertions.assertThat(updatedState).isNotNull();
  }

  @Test
  void invokeReduceDelegatedStakesInvalidAmount() {
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    initial,
                    rpc -> {
                      rpc.writeByte(MpcTokenContract.Invocations.REDUCE_DELEGATED_STAKES);
                      accountB.write(rpc);
                      rpc.writeLong(0);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Amount must be positive");

    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    null,
                    rpc -> {
                      rpc.writeByte(MpcTokenContract.Invocations.REDUCE_DELEGATED_STAKES);
                      accountB.write(rpc);
                      rpc.writeLong(0);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Amount must be positive");

    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    null,
                    rpc -> {
                      rpc.writeByte(
                          MpcTokenContract.Invocations.REDUCE_DELEGATED_STAKES_ON_BEHALF_OF);
                      accountB.write(rpc);
                      accountA.write(rpc);
                      rpc.writeLong(0);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Amount must be positive");
  }

  @Test
  void invokeCheckPendingUnstakes() {
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            rpc -> rpc.writeByte(MpcTokenContract.Invocations.CHECK_PENDING_UNSTAKES));
    byte[] rpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] rpcExpected = AccountPluginRpc.checkPendingUnstakes();
    Assertions.assertThat(rpcActual).isEqualTo(rpcExpected);
    Assertions.assertThat(updatedState).isNotNull();
  }

  @Test
  void invokeCheckPendingUnstakesOnBehalfOf() {
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Invocations.CHECK_PENDING_UNSTAKES_ON_BEHALF_OF);
              accountB.write(rpc);
            });
    byte[] rpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] rpcExpected = AccountPluginRpc.checkPendingUnstakesOnBehalfOf(accountA);
    Assertions.assertThat(rpcActual).isEqualTo(rpcExpected);
    Assertions.assertThat(updatedState).isNotNull();
  }

  @Test
  void invokeCheckVested() {
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            rpc -> rpc.writeByte(MpcTokenContract.Invocations.CHECK_VESTED_TOKENS));

    Assertions.assertThat(updatedState).isNotNull();
    byte[] rpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] rpcExpected = AccountPluginRpc.checkVestedTokens();
    Assertions.assertThat(rpcActual).isEqualTo(rpcExpected);
  }

  @Test
  void invokeCheckVestedForOther() {
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Invocations.CHECK_VESTED_TOKENS_FOR_OTHER);
              accountC.write(rpc);
            });

    Assertions.assertThat(updatedState).isNotNull();
    byte[] rpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] rpcExpected = AccountPluginRpc.checkVestedTokens();
    Assertions.assertThat(rpcActual).isEqualTo(rpcExpected);
    Assertions.assertThat(context.getUpdateLocalAccountPluginStates().get(0).getContext())
        .isEqualTo(accountC);
  }

  @Test
  void invokeCheckVestedOnBehalfOf() {
    BlockchainAddress target = accountB;
    BlockchainAddress custodian = accountA;
    Assertions.assertThat(context.getFrom()).isEqualTo(custodian);
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Invocations.CHECK_VESTED_TOKENS_ON_BEHALF_OF);
              target.write(rpc);
            });

    Assertions.assertThat(updatedState).isNotNull();
    LocalPluginStateUpdate checkVestedTokensUpdate =
        context.getUpdateLocalAccountPluginStates().get(0);
    Assertions.assertThat(checkVestedTokensUpdate.getContext()).isEqualTo(target);
    byte[] rpcActual = checkVestedTokensUpdate.getRpc();
    byte[] rpcExpected = AccountPluginRpc.checkVestedTokensOnBehalfOf(custodian);
    Assertions.assertThat(rpcActual).isEqualTo(rpcExpected);
  }

  @Test
  void invokeAppointInitialCustodian() {
    final BlockchainAddress appointedCustodian = accountB;
    final BlockchainAddress target = accountA;

    Assertions.assertThat(context.getFrom()).isEqualTo(target);
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Invocations.APPOINT_CUSTODIAN);
              target.write(rpc);
              appointedCustodian.write(rpc);
            });

    Assertions.assertThat(updatedState).isNotNull();
    byte[] rpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] rpcExpected = AccountPluginRpc.appointCustodian(appointedCustodian);
    Assertions.assertThat(rpcActual).isEqualTo(rpcExpected);
  }

  @Test
  void invokeAppointCustodian() {
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Invocations.APPOINT_CUSTODIAN);
              accountB.write(rpc);
              accountA.write(rpc);
            });

    Assertions.assertThat(updatedState).isNotNull();
    byte[] rpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] rpcExpected = AccountPluginRpc.appointCustodianOnBehalfOf(accountA, accountA);
    Assertions.assertThat(rpcActual).isEqualTo(rpcExpected);
  }

  @Test
  void invokeCancelInitialAppointedCustodian() {
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Invocations.CANCEL_APPOINTED_CUSTODIAN);
              accountA.write(rpc);
            });

    Assertions.assertThat(updatedState).isNotNull();
    byte[] rpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] rpcExpected = AccountPluginRpc.cancelAppointedCustodian();
    Assertions.assertThat(rpcActual).isEqualTo(rpcExpected);
  }

  @Test
  void invokeCancelAppointedCustodian() {
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Invocations.CANCEL_APPOINTED_CUSTODIAN);
              accountB.write(rpc);
            });

    Assertions.assertThat(updatedState).isNotNull();
    byte[] rpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] rpcExpected = AccountPluginRpc.cancelAppointedCustodianOnBehalfOf(accountA);
    Assertions.assertThat(rpcActual).isEqualTo(rpcExpected);
  }

  @Test
  void invokeResetCustodian() {
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Invocations.RESET_CUSTODIAN);
              accountB.write(rpc);
            });

    Assertions.assertThat(updatedState).isNotNull();
    byte[] rpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] rpcExpected = AccountPluginRpc.resetCustodianOnBehalfOf(accountA);
    Assertions.assertThat(rpcActual).isEqualTo(rpcExpected);
  }

  @Test
  void invokeAcceptCustodianship() {
    MpcTokenContractState updatedState =
        serialization.invoke(
            context,
            initial,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Invocations.ACCEPT_CUSTODIANSHIP);
              accountB.write(rpc);
            });

    Assertions.assertThat(updatedState).isNotNull();
    byte[] rpcActual = context.getUpdateLocalAccountPluginStates().get(0).getRpc();
    byte[] rpcExpected = AccountPluginRpc.acceptCustodianship(accountA);
    Assertions.assertThat(rpcActual).isEqualTo(rpcExpected);
  }

  @Test
  void tokenTransferCallBack() {
    Hash transactionHash = Hash.create(h -> h.writeInt(123));
    BlockchainAddress sender =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    BlockchainAddress recipient =
        BlockchainAddress.fromString("000000000000000000000000000000000000000002");

    byte[] rpc =
        SafeDataOutputStream.serialize(
            MpcTokenContract.Callbacks.tokenTransfer(transactionHash, sender, recipient));

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    Assertions.assertThat(stream.readUnsignedByte())
        .isEqualTo(MpcTokenContract.Callbacks.TRANSFER_CALLBACK);
    Assertions.assertThat(Hash.read(stream)).isEqualTo(transactionHash);
    Assertions.assertThat(BlockchainAddress.read(stream)).isEqualTo(sender);
    Assertions.assertThat(BlockchainAddress.read(stream)).isEqualTo(recipient);
  }

  @Test
  void stakeDelegationCallback() {
    Hash transactionHash = Hash.create(h -> h.writeInt(123));
    BlockchainAddress sender =
        BlockchainAddress.fromString("000000000000000000000000000000000000000001");
    BlockchainAddress recipient =
        BlockchainAddress.fromString("000000000000000000000000000000000000000002");

    byte[] rpc =
        SafeDataOutputStream.serialize(
            MpcTokenContract.Callbacks.stakeDelegation(transactionHash, sender, recipient));

    SafeDataInputStream stream = SafeDataInputStream.createFromBytes(rpc);
    Assertions.assertThat(stream.readUnsignedByte())
        .isEqualTo(MpcTokenContract.Callbacks.STAKE_DELEGATION_CALLBACK);
    Assertions.assertThat(Hash.read(stream)).isEqualTo(transactionHash);
    Assertions.assertThat(BlockchainAddress.read(stream)).isEqualTo(sender);
    Assertions.assertThat(BlockchainAddress.read(stream)).isEqualTo(recipient);
  }

  /**
   * If a stake delegation has been aborted through {@link
   * MpcTokenContract.Invocations#ABORT_STAKE_DELEGATION_EVENT} prior to the callback being executed
   * then it has been removed from the state of the contract and no further actions should be taken.
   */
  @Test
  void stakeDelegationCallbackFailIfNoLongerPresentInState() {
    CallbackContext callbackContext = createTwoPhaseCallbackContext(true, true);
    Assertions.assertThatThrownBy(
            () ->
                serialization.callback(
                    context,
                    callbackContext,
                    initial,
                    MpcTokenContract.Callbacks.stakeDelegation(hashA, accountA, accountB)))
        .isInstanceOf(IllegalArgumentException.class)
        .hasMessage("Two phase already handled");
  }

  @Test
  void lockMpcTransfers() {
    MpcTokenContractState unlockedMpcTransfers = MpcTokenContractState.initial(false);

    from(MpcTokenContract.SYSTEM_UPDATE_CONTRACT_ADDRESS);
    MpcTokenContractState lockedMpcTransfers =
        serialization.invoke(
            context,
            unlockedMpcTransfers,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Invocations.LOCK_MPC_TRANSFERS);
              rpc.writeBoolean(true);
            });

    Assertions.assertThat(lockedMpcTransfers.isLocked()).isTrue();
  }

  @Test
  void unlockMpcTransfers() {
    MpcTokenContractState lockedMpcTransfers = MpcTokenContractState.initial(true);

    from(MpcTokenContract.SYSTEM_UPDATE_CONTRACT_ADDRESS);
    MpcTokenContractState unlockedMpcTransfers =
        serialization.invoke(
            context,
            lockedMpcTransfers,
            rpc -> {
              rpc.writeByte(MpcTokenContract.Invocations.LOCK_MPC_TRANSFERS);
              rpc.writeBoolean(false);
            });

    Assertions.assertThat(unlockedMpcTransfers.isLocked()).isFalse();
  }

  @Test
  void lockMpcTransferNotThroughSystemUpdateFails() {
    Assertions.assertThatThrownBy(
            () ->
                serialization.invoke(
                    context,
                    initial,
                    rpc -> {
                      rpc.writeByte(MpcTokenContract.Invocations.LOCK_MPC_TRANSFERS);
                      rpc.writeBoolean(true);
                    }))
        .isInstanceOf(RuntimeException.class)
        .hasMessage("Locking or unlocking MPC transfers can only be done through a system update");
  }

  private void from(BlockchainAddress from) {
    context =
        new SysContractContextTest(context.getBlockProductionTime(), context.getBlockTime(), from);
  }

  private void assertUnmodifiedState(
      MpcTokenContractState before, Function<MpcTokenContractState, MpcTokenContractState> invoke) {
    MpcTokenContractState after = invoke.apply(before);
    StateSerializableEquality.assertStatesEqual(after, before);
  }

  private void assertAccountPluginUpdate(
      LocalPluginStateUpdate update, BlockchainAddress context, byte[] rpc) {
    Assertions.assertThat(update.getContext()).isEqualTo(context);
    Assertions.assertThat(update.getRpc()).isEqualTo(rpc);
  }

  private CallbackContext createTwoPhaseCallbackContext(boolean success1, boolean success2) {
    return createCallbackContextWithResults(new byte[0], success1, success2);
  }

  private CallbackContext createExistsCallbackContext(boolean callbackSuccess, boolean exists) {
    SafeDataInputStream returnValue;
    if (callbackSuccess) {
      returnValue =
          SafeDataInputStream.createFromBytes(
              SafeDataOutputStream.serialize(stream -> stream.writeBoolean(exists)));
    } else {
      returnValue = SafeDataInputStream.createFromBytes(new byte[0]);
    }
    return CallbackContext.create(
        FixedList.create(
            List.of(CallbackContext.createResult(hashA, callbackSuccess, returnValue))));
  }

  private CallbackContext createCallbackContextWithResults(
      byte[] data, Boolean... successStatuses) {
    List<CallbackContext.ExecutionResult> results =
        Arrays.stream(successStatuses)
            .map(
                success ->
                    CallbackContext.createResult(
                        hashA, success, SafeDataInputStream.createFromBytes(data)))
            .toList();

    return CallbackContext.create(FixedList.create(results));
  }

  private BlockchainAddress createContractAddress() {
    return BlockchainAddress.fromHash(
        BlockchainAddress.Type.CONTRACT_SYSTEM, Hash.create(s -> s.writeInt(33)));
  }

  @Test
  @DisplayName(
      "Migrating from a state without expiration timestamp to a state with a expiration timestamp,"
          + " succeeds.")
  void migrateFromOldState() {
    Hash delegationHash = Hash.create(s -> s.writeInt(123));
    OldStakeDelegation oldStakeDelegation =
        new OldStakeDelegation(
            accountA, accountB, 1000, StakeDelegation.DelegationType.DELEGATE_STAKES, accountC);
    AvlTree<Hash, OldStakeDelegation> oldStakeDelegations = AvlTree.create();
    oldStakeDelegations = oldStakeDelegations.set(delegationHash, oldStakeDelegation);
    OldMpcTokenContractState oldMpcTokenContractState =
        new OldMpcTokenContractState(
            AvlTree.create(), oldStakeDelegations, AvlTree.create(), false);

    MpcTokenContractState afterUpgrade =
        new MpcTokenContractInvoker()
            .onUpgrade(
                StateAccessor.create(oldMpcTokenContractState),
                SafeDataInputStream.createFromBytes(new byte[0]));

    Assertions.assertThat(
            afterUpgrade.getStakeDelegations().getValue(delegationHash).expirationTimestamp)
        .isNull();
  }

  private record OldMpcTokenContractState(
      AvlTree<Hash, TransferInformation> transfers,
      AvlTree<Hash, OldStakeDelegation> stakeDelegations,
      AvlTree<Hash, IceStaking> icedStakings,
      boolean locked)
      implements StateSerializable {}

  @Immutable
  private record OldStakeDelegation(
      BlockchainAddress sender,
      BlockchainAddress recipient,
      long amount,
      StakeDelegation.DelegationType delegationType,
      BlockchainAddress initiator)
      implements StateSerializable {}
}
